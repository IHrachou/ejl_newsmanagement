package com.epam.javalab.newsmanag.command.constants;

import javax.servlet.http.HttpServletRequest;

/**
 * This class contains page mapping constants.
 *
 * @author Ilya Hrachou
 * @version 1.0
 */
public final class PageConstants {
    // prevent from creating instances
    private PageConstants() {
    }

    /**
     * Path to the error page.
     */
    public static final String PAGE_ERROR = "/jsp/error.jsp";
    /**
     * Path to the news-list.jsp page.
     */
    public static final String PAGE_NEWS_LIST = "/jsp/news-list.jsp";
    /**
     * Path to the view-news.jsp page.
     */
    public static final String PAGE_VIEW_NEWS = "/jsp/view-news.jsp";
    /**
     * Path to the controller action that will show default page of the news
     * list view.
     */
    public static final String CONTROLLER_NEWS_LIST = "ClientController?"
            + "command=showNewsList";

    /**
     * Generate the URL to the current page. This method is used to return to
     * the page that sent request.
     * <p>
     * For example, this method will return the path to the page which request
     * to change locale. When locale will be changed controller will forward to
     * this page.
     *
     * @param request
     *            HttpServletRequest instance
     * @return the URL of the requested page
     */
    public static String getPathToCurrentPage(HttpServletRequest request) {
        StringBuilder page = new StringBuilder(request.getServletPath());
        page = page.deleteCharAt(0);
        page.append("?");
        page.append(request.getSession().getAttribute(
                RequestParametersNames.PARAM_INPUT_QUERY));
        request.setAttribute(RequestParametersNames.PARAM_REDIRECT, "");
        return page.toString();
    }
}
