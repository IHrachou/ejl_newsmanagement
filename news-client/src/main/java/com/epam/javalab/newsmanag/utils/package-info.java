/**
 * This package contains different utility classes.
 *
 * @author Ilya Hrachou
 */
package com.epam.javalab.newsmanag.utils;
