package com.epam.javalab.newsmanag.tags;

import java.io.IOException;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import com.epam.javalab.newsmanag.command.constants.RequestParametersNames;
import com.epam.javalab.newsmanag.domain.Author;
import com.epam.javalab.newsmanag.domain.SearchCriteria;

/**
 * Write new &lt;option&gt; tag for Author instance and add "selected" attribute
 * if this author presents in SearchCriteria.
 *
 * @author Ilya Hrachou
 * @version 1.0
 */
public class AuthorsListTag extends TagSupport {
    private static final long serialVersionUID = 7044114082707318382L;
    // html output
    private static final String OPTION_SELECTED = "<option value=\"%d\" "
            + "selected=\"selected\">%s</option>";
    private static final String OPTION_UNSELECTED = "<option value=\"%d\">"
            + "%s</option>";

    /*
     * (non-Javadoc)
     * @see javax.servlet.jsp.tagext.TagSupport#doStartTag()
     */
    @Override
    public int doStartTag() throws JspException {
        HttpServletRequest request = (HttpServletRequest) pageContext
                .getRequest();
        SearchCriteria searchCriteria = (SearchCriteria) request.getSession()
                .getAttribute(RequestParametersNames.PARAM_SEARCH_CRITERIA);
        @SuppressWarnings("unchecked")
        List<Author> authors = (List<Author>) request.getAttribute(
                RequestParametersNames.PARAM_AUTHOR_LIST);
        JspWriter out = pageContext.getOut();
        try {
            if (searchCriteria == null) {
                for (Author author : authors) {
                    out.write(String.format(OPTION_UNSELECTED, author.getId(),
                            author.getName()));
                }
            } else {
                Set<Long> selectedAuthors = searchCriteria.getAuthorIds();
                for (Author author : authors) {
                    long authorId = author.getId();
                    String output = selectedAuthors.contains(authorId)
                            ? OPTION_SELECTED : OPTION_UNSELECTED;
                    out.write(String.format(output, author.getId(), author
                            .getName()));
                }
            }
        } catch (IOException e) {
            throw new JspException("Unable to write to the JspWriter");
        }
        return SKIP_BODY;
    }
}
