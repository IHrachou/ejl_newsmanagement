/**
 * This package contains classes to execute logic commands from the servlets
 * according to the MVC pattern. These classes represent model layer. They are
 * used for handling requests from the servlets and executing proper logic from
 * the backend module.
 *
 * @author Ilya Hrachou
 * @version 1.0
 */
package com.epam.javalab.newsmanag.command;