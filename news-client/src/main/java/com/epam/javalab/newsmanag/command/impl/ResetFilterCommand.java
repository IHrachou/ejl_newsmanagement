package com.epam.javalab.newsmanag.command.impl;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Component;

import com.epam.javalab.newsmanag.command.ICommand;
import com.epam.javalab.newsmanag.command.constants.PageConstants;
import com.epam.javalab.newsmanag.command.constants.RequestParametersNames;

/**
 * Reset the SearchCriteria from the session attributes and forward to show the
 * first page of non filtered news list.
 *
 * @author Ilya Hrachou
 * @version 1.0
 */
@Component(value = "resetFilter")
public class ResetFilterCommand implements ICommand {
    /*
     * (non-Javadoc)
     * @see
     * com.epam.javalab.newsmanag.command.ICommand#execute(javax.servlet.http.
     * HttpServletRequest)
     */
    @Override
    public String execute(HttpServletRequest request) {
        request.getSession().removeAttribute(
                RequestParametersNames.PARAM_SEARCH_CRITERIA);
        return PageConstants.CONTROLLER_NEWS_LIST;
    }
}
