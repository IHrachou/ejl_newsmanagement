package com.epam.javalab.newsmanag.command;

import javax.servlet.http.HttpServletRequest;

/**
 * This interface provides contract for command classes. It describe method
 * execute() which must return URL for the next page, that will be send to the
 * user as a result of this method.
 *
 * @author IHrachou
 * @version 1.0
 */
public interface ICommand {
    /**
     * This method return the URL of the next page, that will be send to the
     * user. This method must extract all required parameters from the request,
     * call proper business logic class and set the result variables to the
     * request.
     *
     * @param request
     *            an {@link javax.servlet.http.HttpServletRequest} object that
     *            contains the request the client has made of the servlet
     * @return the URL of the page with the results of proper business logic
     *         classes
     */
    String execute(HttpServletRequest request);
}
