package com.epam.javalab.newsmanag.command.impl;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.epam.javalab.newsmanag.command.ICommand;
import com.epam.javalab.newsmanag.command.constants.PageConstants;
import com.epam.javalab.newsmanag.command.constants.RequestParametersNames;
import com.epam.javalab.newsmanag.domain.News;
import com.epam.javalab.newsmanag.domain.SearchCriteria;
import com.epam.javalab.newsmanag.domain.dto.NewsDto;
import com.epam.javalab.newsmanag.exceptions.ServiceException;
import com.epam.javalab.newsmanag.services.INewsHandlingService;

/**
 * This command class is used for showing single news from the database with all
 * its comments, authors and tags. It is used for showing current, previous and
 * next news.
 *
 * @author Ilya Hrachou
 * @version 1.0
 */
@Component(value = "showNews")
public class ShowNewsCommand implements ICommand {
    private static final Logger logger = LogManager.getLogger(
            ShowNewsCommand.class);
    @Autowired
    private INewsHandlingService newsService;

    @Override
    public String execute(HttpServletRequest request) {
        String id = request.getParameter(RequestParametersNames.PARAM_NEWS_ID);
        long newsId;
        NewsDto newsDto;
        try {
            newsId = Integer.parseInt(id);
        } catch (Exception e) {
            logger.error("News ID is not a number");
            return PageConstants.PAGE_ERROR;
        }
        SearchCriteria searchCriteria = (SearchCriteria) request.getSession()
                .getAttribute(RequestParametersNames.PARAM_SEARCH_CRITERIA);
        try {
            if (request.getParameter(
                    RequestParametersNames.PARAM_PREVIOUS) != null) {
                newsDto = newsService.showPreviousNews(newsId, searchCriteria);
            } else if (request.getParameter(
                    RequestParametersNames.PARAM_NEXT) != null) {
                newsDto = newsService.showNextNews(newsId, searchCriteria);
            } else {
                newsDto = newsService.showNews(newsId);
            }
            if (newsDto == null) {
                request.setAttribute(RequestParametersNames.PARAM_REDIRECT, "");
                return PageConstants.CONTROLLER_NEWS_LIST;
            }
            formatNewsText(newsDto);
            request.setAttribute(RequestParametersNames.PARAM_NEWS, newsDto);
        } catch (ServiceException e) {
            logger.error("Cannot get news for id=" + newsId);
            return PageConstants.PAGE_ERROR;
        }
        request.getSession().setAttribute(
                RequestParametersNames.PARAM_INPUT_QUERY, request
                        .getQueryString());
        return PageConstants.PAGE_VIEW_NEWS;
    }

    private void formatNewsText(NewsDto newsDto) {
        News news = newsDto.getNews();
        String newsText = news.getFullText().replaceAll("\n", "<p>");
        news.setFullText(newsText);
    }
}
