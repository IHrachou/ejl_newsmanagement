/**
 * This package contains implementations of the {@link javax.servlet.Filter}
 * interface that performs filtering tasks on either the request to a resource
 * (a servlet or static content), or on the response from a resource, or both.
 *
 * @author Ilya Hrachou
 */
package com.epam.javalab.newsmanag.filters;
