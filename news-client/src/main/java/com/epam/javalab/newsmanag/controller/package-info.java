/**
 * This package contains controller classes (servlet implementation). This
 * classes provides the realization of Controller layer according to the MVC
 * pattern of the web application. Servlets processes requests from the JSP and
 * return the response.
 *
 * @author Ilya_Hrachou
 */
package com.epam.javalab.newsmanag.controller;