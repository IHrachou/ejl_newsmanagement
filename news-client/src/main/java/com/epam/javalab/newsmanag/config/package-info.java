/**
 * This package contains Java based Spring configuration.
 *
 * @author Ilya Hrachou
 * @version 1.0
 */
package com.epam.javalab.newsmanag.config;