package com.epam.javalab.newsmanag.tags;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import com.epam.javalab.newsmanag.command.constants.RequestParametersNames;
import com.epam.javalab.newsmanag.domain.Author;
import com.epam.javalab.newsmanag.domain.dto.NewsDto;

/**
 * Create list of authors for the current news ended with bracket ')'.
 *
 * @author Ilya Hrachou
 * @version 1.0
 */
public class NewsAuthorsListTag extends TagSupport {
    private static final long serialVersionUID = -395869038513580582L;

    /*
     * (non-Javadoc)
     * @see javax.servlet.jsp.tagext.TagSupport#doStartTag()
     */
    @Override
    public int doStartTag() throws JspException {
        HttpServletRequest request = (HttpServletRequest) pageContext
                .getRequest();
        NewsDto newsDto = (NewsDto) request.getAttribute(
                RequestParametersNames.PARAM_NEWS);
        List<Author> authors = newsDto.getAuthors();
        JspWriter out = pageContext.getOut();
        try {
            StringBuilder authorsList = new StringBuilder();
            if ((authors != null) && (!authors.isEmpty())) {
                for (Author author : authors) {
                    authorsList.append(author.getName()).append(", ");
                }
                int length = authorsList.length();
                authorsList = authorsList.replace(length - 2, length, ")");
                out.write(authorsList.toString());
            }
        } catch (IOException e) {
            throw new JspException("Unable to write to the JspWriter");
        }
        return SKIP_BODY;
    }
}
