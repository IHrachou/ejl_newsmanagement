/**
 * This package contains implementations of the {@code ICommand} interface.
 *
 * @author Ilya Hrachou
 * @version 1.0
 */
package com.epam.javalab.newsmanag.command.impl;