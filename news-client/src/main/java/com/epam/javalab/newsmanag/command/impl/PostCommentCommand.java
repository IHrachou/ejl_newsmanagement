package com.epam.javalab.newsmanag.command.impl;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.epam.javalab.newsmanag.command.ICommand;
import com.epam.javalab.newsmanag.command.constants.PageConstants;
import com.epam.javalab.newsmanag.command.constants.RequestParametersNames;
import com.epam.javalab.newsmanag.domain.Comment;
import com.epam.javalab.newsmanag.services.ICommentService;

/**
 * This command class is used to add new comment to the news.
 *
 * @author Ilya Hrachou
 * @version 1.0
 */
@Component(value = "postComment")
public class PostCommentCommand implements ICommand {
    private static final Logger logger = LogManager.getLogger(
            PostCommentCommand.class);
    private static final int MAX_COMMENT_LENGTH = 100;
    @Autowired
    private ICommentService commentService;

    @Override
    public String execute(HttpServletRequest request) {
        long newsId;
        try {
            newsId = Integer.parseInt(request.getParameter(
                    RequestParametersNames.PARAM_NEWS_ID));
        } catch (Exception e) {
            logger.error("News ID is not a number");
            return PageConstants.PAGE_ERROR;
        }
        String commentText = request.getParameter(
                RequestParametersNames.PARAM_COMMENT_TEXT).trim();
        if (checkCommentLength(commentText)) {
            Comment comment = new Comment();
            comment.setNewsId(newsId);
            comment.setText(commentText);
            try {
                commentService.add(comment);
            } catch (Exception e) {
                logger.error("Cannot add comment to the database");
            }
        } else {
            logger.error("Comment length is large than allowed");
        }
        return PageConstants.getPathToCurrentPage(request);
    }

    private boolean checkCommentLength(String comment) {
        if (comment.length() <= MAX_COMMENT_LENGTH) {
            return true;
        }
        return false;
    }
}
