package com.epam.javalab.newsmanag.utils;

import java.util.ResourceBundle;

/**
 * This class provides methods for parsing page number and amount of news per
 * page parameters from the request.
 *
 * @author Ilya Hrachou
 * @version 1.0
 */
public final class PaginationParametersParser {

    private PaginationParametersParser() {
    }

    // Default values
    private static int defaultPageNumber = 1;
    private static int defaultNewsPerPage = 10;
    // Initializing variables with default values from the config file
    static {
        try {
            ResourceBundle bundle = ResourceBundle.getBundle("config");
            try {
                defaultPageNumber = Integer.parseInt(bundle.getString(
                        "default.pageNumber"));
            } catch (NumberFormatException e) {
                // If config contains illegal value it will be skipped
            }
            try {
                defaultNewsPerPage = Integer.parseInt(bundle.getString(
                        "default.newsPerPage"));
            } catch (NumberFormatException e) {
                // If config contains illegal value it will be skipped
            }
        } catch (Exception e) {
            // If cannot load config default values will be remained
        }
    }

    /**
     * Return the page number from the input string or default value if this
     * string parameter cannot be parsed.
     *
     * @param pageNumberFromRequest
     *            string representation of the page number
     * @return the page number from the input string or default value if this
     *         string parameter cannot be parsed
     */
    public static int getPageNumber(String pageNumberFromRequest) {
        try {
            return Integer.parseInt(pageNumberFromRequest);
        } catch (NumberFormatException e) {
            // Nothing to do
        }
        return defaultPageNumber;
    }

    /**
     * Return the amount of news per page from the input string or default value
     * if this string parameter cannot be parsed.
     *
     * @param newsPerPageFromRequest
     *            string representation of the amount of news per page
     * @return the amount of news per page from the input string or default
     *         value if this string parameter cannot be parsed
     */
    public static int getNewsPerPage(String newsPerPageFromRequest) {
        try {
            return Integer.parseInt(newsPerPageFromRequest);
        } catch (NumberFormatException e) {
            // Nothing to do
        }
        return defaultNewsPerPage;
    }
}
