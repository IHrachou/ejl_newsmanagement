package com.epam.javalab.newsmanag.command.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.epam.javalab.newsmanag.command.ICommand;
import com.epam.javalab.newsmanag.command.constants.PageConstants;
import com.epam.javalab.newsmanag.command.constants.RequestParametersNames;
import com.epam.javalab.newsmanag.domain.Author;
import com.epam.javalab.newsmanag.domain.SearchCriteria;
import com.epam.javalab.newsmanag.domain.Tag;
import com.epam.javalab.newsmanag.domain.dto.NewsDto;
import com.epam.javalab.newsmanag.exceptions.ServiceException;
import com.epam.javalab.newsmanag.services.IAuthorService;
import com.epam.javalab.newsmanag.services.INewsHandlingService;
import com.epam.javalab.newsmanag.services.INewsService;
import com.epam.javalab.newsmanag.services.ITagService;
import com.epam.javalab.newsmanag.utils.PaginationParametersParser;

/**
 * This command class is used for showing list of news from the database for the
 * specified page. The amount of showed news is limited by the parameter from
 * the request. If the amount of news per page or the page number are not
 * specified in the request then default values will be used.
 *
 * @author Ilya Hrachou
 * @version 1.0
 */
@Component(value = "showNewsList")
public class ShowNewsListCommand implements ICommand {
    private static final Logger logger = LogManager.getLogger(
            ShowNewsListCommand.class);

    @Autowired
    private INewsHandlingService newsHandlingService;
    @Autowired
    private INewsService newsService;
    @Autowired
    private IAuthorService authorService;
    @Autowired
    private ITagService tagService;

    @Override
    public String execute(HttpServletRequest request) {
        int newsPerPage = PaginationParametersParser.getNewsPerPage(request
                .getParameter(RequestParametersNames.PARAM_NEWS_PER_PAGE));
        int pageNumber = PaginationParametersParser.getPageNumber(request
                .getParameter(RequestParametersNames.PARAM_PAGE));
        request.getSession().setAttribute(RequestParametersNames.PARAM_PAGE,
                pageNumber);
        SearchCriteria searchCriteria = (SearchCriteria) request.getSession()
                .getAttribute(RequestParametersNames.PARAM_SEARCH_CRITERIA);
        try {
            int startPos = ((pageNumber - 1) * newsPerPage) + 1;
            int endPos = pageNumber * newsPerPage;
            long newsAmount;
            List<NewsDto> newsList;
            if (searchCriteria == null) {
                newsList = newsHandlingService.showNewsListForRange(startPos,
                        endPos);
                newsAmount = newsService.countAllNews();
            } else {
                newsList = newsHandlingService.showNewsListForRange(startPos,
                        endPos, searchCriteria);
                newsAmount = newsService.countFilteredNews(searchCriteria);
            }
            List<Author> authors = authorService.getAll();
            List<Tag> tags = tagService.getAll();
            request.setAttribute(RequestParametersNames.PARAM_NEWS_LIST,
                    newsList);
            request.setAttribute(RequestParametersNames.PARAM_AUTHOR_LIST,
                    authors);
            request.setAttribute(RequestParametersNames.PARAM_TAG_LIST, tags);
            request.setAttribute(RequestParametersNames.PARAM_NEWS_AMOUNT,
                    newsAmount);
            request.getSession().setAttribute(
                    RequestParametersNames.PARAM_INPUT_QUERY, request
                            .getQueryString());
            return PageConstants.PAGE_NEWS_LIST;
        } catch (ServiceException e) {
            logger.error("Cannot get news list for page=" + pageNumber);
            return PageConstants.PAGE_ERROR;
        }
    }
}
