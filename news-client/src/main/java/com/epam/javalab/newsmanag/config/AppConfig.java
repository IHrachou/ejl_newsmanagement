package com.epam.javalab.newsmanag.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

/**
 * Spring configuration class.
 *
 * @author Ilya Hrachou
 * @version 1.0
 */
@Configuration
@ComponentScan(basePackages = { "com.epam.javalab.newsmanag.command",
        "com.epam.javalab.newsmanag.dao",
        "com.epam.javalab.newsmanag.services" })
@ImportResource("spring-config.xml")
public class AppConfig {

}
