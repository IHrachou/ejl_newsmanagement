package com.epam.javalab.newsmanag.command.constants;

/**
 * This utility class contains string constants with the names of the request
 * parameters.
 *
 * @author Ilya Hrachou
 * @version 1.0
 */
public final class RequestParametersNames {

    private RequestParametersNames() {
    }

    // Common
    public static final String PARAM_INPUT_QUERY = "query";
    public static final String PARAM_REDIRECT = "redirect";
    // Localization
    public static final String PARAM_LANG = "lang";
    public static final String PARAM_LANGUAGE = "language";
    // Database information
    public static final String PARAM_AUTHORS = "authors";
    public static final String PARAM_AUTHOR_LIST = "authorList";
    public static final String PARAM_TAGS = "tags";
    public static final String PARAM_TAG_LIST = "tagList";
    public static final String PARAM_NEWS_LIST = "newsList";
    public static final String PARAM_SEARCH_CRITERIA = "searchCriteria";
    public static final String PARAM_NEWS_AMOUNT = "newsAmount";
    public static final String PARAM_NEWS_ID = "id";
    public static final String PARAM_NEWS = "news";
    public static final String PARAM_COMMENT_TEXT = "commentText";
    // Pagination
    public static final String PARAM_PAGE = "page";
    public static final String PARAM_NEWS_PER_PAGE = "newsPerPage";
    public static final String PARAM_PREVIOUS = "prev";
    public static final String PARAM_NEXT = "next";
}
