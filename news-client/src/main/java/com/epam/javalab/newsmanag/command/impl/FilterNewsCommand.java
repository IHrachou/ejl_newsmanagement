package com.epam.javalab.newsmanag.command.impl;

import java.util.Arrays;
import java.util.HashSet;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.epam.javalab.newsmanag.command.ICommand;
import com.epam.javalab.newsmanag.command.constants.PageConstants;
import com.epam.javalab.newsmanag.command.constants.RequestParametersNames;
import com.epam.javalab.newsmanag.domain.SearchCriteria;

/**
 * Generate SearchCriteria from the request and add it to the session
 * attributes. If request contains no parameters for generating SearchCriteria
 * this command forwards to show the non filtered list of news. Otherwise it
 * forwards to show the list of news that is filtered according to the
 * SearchCriteria.
 *
 * @author Ilya Hrachou
 * @version 1.0
 */
@Component(value = "filterNewsList")
public class FilterNewsCommand implements ICommand {
    private static final Logger logger = LogManager.getLogger(
            FilterNewsCommand.class);

    /*
     * (non-Javadoc)
     * @see
     * com.epam.javalab.newsmanag.command.ICommand#execute(javax.servlet.http.
     * HttpServletRequest)
     */
    @Override
    public String execute(HttpServletRequest request) {
        Long[] authorIds = processLine(request.getParameterValues(
                RequestParametersNames.PARAM_AUTHORS));
        Long[] tagIds = processLine(request.getParameterValues(
                RequestParametersNames.PARAM_TAGS));
        if (!(authorIds.length == 0) || !(tagIds.length == 0)) {
            SearchCriteria searchCriteria = new SearchCriteria();
            searchCriteria.setAuthorIds(new HashSet<Long>(Arrays.asList(
                    authorIds)));
            searchCriteria.setTagIds(new HashSet<Long>(Arrays.asList(tagIds)));
            request.getSession().setAttribute(
                    RequestParametersNames.PARAM_SEARCH_CRITERIA,
                    searchCriteria);
        }
        return PageConstants.CONTROLLER_NEWS_LIST;
    }

    private Long[] processLine(String[] strings) {
        Long[] longArray;
        if (strings == null) {
            return new Long[0];
        }
        longArray = new Long[strings.length];
        int i = 0;
        try {
            for (String str : strings) {
                longArray[i] = Long.parseLong(str);
                i++;
            }
        } catch (NumberFormatException e) {
            logger.error("Input strings cannot be parsed to the ints");
        }
        return longArray;
    }
}
