package com.epam.javalab.newsmanag.tags;

import java.io.IOException;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import com.epam.javalab.newsmanag.command.constants.RequestParametersNames;
import com.epam.javalab.newsmanag.domain.SearchCriteria;
import com.epam.javalab.newsmanag.domain.Tag;

/**
 * Write new &lt;option&gt; tag for Tag instance and add "selected" attribute if
 * this tag presents in SearchCriteria.
 *
 * @author Ilya Hrachou
 * @version 1.0
 */
public class TagsListTag extends TagSupport {
    private static final long serialVersionUID = -6492829152109178413L;
    // html output
    private static final String OPTION_SELECTED = "<option value=\"%d\" "
            + "selected=\"selected\">%s</option>";
    private static final String OPTION_UNSELECTED = "<option value=\"%d\">"
            + "%s</option>";

    /*
     * (non-Javadoc)
     * @see javax.servlet.jsp.tagext.TagSupport#doStartTag()
     */
    @Override
    public int doStartTag() throws JspException {
        HttpServletRequest request = (HttpServletRequest) pageContext
                .getRequest();
        SearchCriteria searchCriteria = (SearchCriteria) request.getSession()
                .getAttribute(RequestParametersNames.PARAM_SEARCH_CRITERIA);
        @SuppressWarnings("unchecked")
        List<Tag> tags = (List<Tag>) request.getAttribute(
                RequestParametersNames.PARAM_TAG_LIST);
        JspWriter out = pageContext.getOut();
        try {
            if (searchCriteria == null) {
                for (Tag tag : tags) {
                    out.write(String.format(OPTION_UNSELECTED, tag.getId(), tag
                            .getName()));
                }
            } else {
                Set<Long> selectedTags = searchCriteria.getTagIds();
                for (Tag tag : tags) {
                    long tagId = tag.getId();
                    String output = selectedTags.contains(tagId)
                            ? OPTION_SELECTED : OPTION_UNSELECTED;
                    out.write(String.format(output, tag.getId(), tag
                            .getName()));
                }
            }
        } catch (IOException e) {
            throw new JspException("Unable to write to the JspWriter");
        }
        return SKIP_BODY;
    }
}
