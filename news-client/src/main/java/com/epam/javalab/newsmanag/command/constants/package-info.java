/**
 * Contains constants which are used in the commands implementations.
 *
 * @author Ilya Hrachou
 * @version 1.0
 */
package com.epam.javalab.newsmanag.command.constants;