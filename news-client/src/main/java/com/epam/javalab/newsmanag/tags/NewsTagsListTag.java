package com.epam.javalab.newsmanag.tags;

import java.io.IOException;
import java.util.List;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import com.epam.javalab.newsmanag.domain.Tag;

/**
 * Create list of tags for the news.
 *
 * @author Ilya Hrachou
 * @version 1.0
 */
public class NewsTagsListTag extends TagSupport {
    private static final long serialVersionUID = 4563691485075526746L;

    private List<Tag> tagList;

    /**
     * Set the list of tags.
     *
     * @param tagList
     *            the list of tags to set
     */
    public void setTagList(List<Tag> tagList) {
        this.tagList = tagList;
    }

    /*
     * (non-Javadoc)
     * @see javax.servlet.jsp.tagext.TagSupport#doStartTag()
     */
    @Override
    public int doStartTag() throws JspException {
        JspWriter out = pageContext.getOut();
        try {
            StringBuilder output = new StringBuilder();
            if (tagList != null) {
                for (Tag tag : tagList) {
                    output.append(tag.getName()).append(", ");
                }
                int length = output.length();
                output = output.delete(length - 2, length);
                out.write(output.toString());
            }
        } catch (IOException e) {
            throw new JspException("Unable to write to the JspWriter");
        }
        return SKIP_BODY;
    }
}
