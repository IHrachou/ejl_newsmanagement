package com.epam.javalab.newsmanag.command.impl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.jstl.core.Config;

import org.springframework.stereotype.Component;

import com.epam.javalab.newsmanag.command.ICommand;
import com.epam.javalab.newsmanag.command.constants.RequestParametersNames;

/**
 * Change the locale of the view layer.
 *
 * @author Ilya Hrachou
 * @version 1.0
 */
@Component(value = "changeLocale")
public class ChangeLocaleCommand implements ICommand {
    @Override
    public String execute(HttpServletRequest request) {
        // request.getSession().setAttribute(RequestParametersNames.PARAM_LANGUAGE,
        // request.getParameter(RequestParametersNames.PARAM_LANG));
        Config.set(request.getSession(), Config.FMT_LOCALE, request
                .getParameter(RequestParametersNames.PARAM_LANG));
        StringBuilder page = new StringBuilder(request.getServletPath());
        page = page.deleteCharAt(0);
        page.append("?");
        page.append(request.getSession().getAttribute(
                RequestParametersNames.PARAM_INPUT_QUERY));
        request.setAttribute(RequestParametersNames.PARAM_REDIRECT, "");
        return page.toString();
    }
}
