package com.epam.javalab.newsmanag.tags;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import com.epam.javalab.newsmanag.command.constants.RequestParametersNames;
import com.epam.javalab.newsmanag.utils.PaginationParametersParser;

/**
 * Adds pagination panel and provides links to the specified pages.
 *
 * @author Ilya Hrachou
 * @version 1.0
 */
public class PaginationTag extends TagSupport {
    private static final long serialVersionUID = 2759677052975207667L;
    // HTML output strings
    private static final String HTML_REFERENCE = "<a href=\"ClientController"
            + "?command=showNewsList&page=%d\" class=\"pagination\" %s>%s</a>";
    private static final String HTML_LT = "&lt;";
    private static final String HTML_GT = "&gt;";
    private static final String TITLE_LAST = "title=\"Go to the last page\"";
    private static final String TITLE_FIRST = "title=\"Go to the first page\"";

    @Override
    public int doStartTag() throws JspException {
        HttpServletRequest request = (HttpServletRequest) pageContext
                .getRequest();
        int newsPerPage = PaginationParametersParser.getNewsPerPage(request
                .getParameter(RequestParametersNames.PARAM_NEWS_PER_PAGE));
        int pageNumber = PaginationParametersParser.getPageNumber(request
                .getParameter(RequestParametersNames.PARAM_PAGE));
        long newsAmount;
        newsAmount = (Long) request.getAttribute(
                RequestParametersNames.PARAM_NEWS_AMOUNT);
        int pagesAmount = (int) ((newsAmount % newsPerPage) == 0 ? newsAmount
                / newsPerPage : (newsAmount / newsPerPage) + 1);
        JspWriter out = pageContext.getOut();
        try {
            if (pageNumber != 1) {

                int prevPage = pageNumber - 1;
                if (prevPage != 1) {
                    // reference to the first page
                    out.write(String.format(HTML_REFERENCE, 1, TITLE_FIRST,
                            HTML_LT));
                }
                // reference to the previous page if exist
                out.write(String.format(HTML_REFERENCE, prevPage, "",
                        prevPage));
            }
            // reference to the current page
            out.write(String.format(HTML_REFERENCE, pageNumber, "",
                    pageNumber));
            if (pageNumber != pagesAmount) {
                // reference to the next page if exist
                int nextPage = pageNumber + 1;
                out.write(String.format(HTML_REFERENCE, nextPage, "",
                        nextPage));
                if (nextPage != pagesAmount) {
                    // reference to the last page
                    out.write(String.format(HTML_REFERENCE, pagesAmount,
                            TITLE_LAST, HTML_GT));
                }
            }
        } catch (IOException e) {
            // Nothing to do
        }
        return super.doStartTag();
    }
}
