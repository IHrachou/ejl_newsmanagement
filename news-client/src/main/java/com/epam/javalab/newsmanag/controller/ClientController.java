package com.epam.javalab.newsmanag.controller;

import java.io.File;
import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.epam.javalab.newsmanag.command.ICommand;
import com.epam.javalab.newsmanag.config.AppConfig;

/**
 * Servlet implementation class ClientController.
 *
 * @author Ilya_Hrachou
 * @version 1.0
 * @see javax.servlet.http.HttpServlet
 */
@WebServlet("/ClientController")
public class ClientController extends HttpServlet {
    private static final long serialVersionUID = -2064736714565652078L;
    private static final Logger logger = Logger.getLogger(
            ClientController.class);
    private static final String PARAM_NAME_COMMAND = "command";
    private static final String LOG_CONF_FILE = File.separator + "log4j.xml";

    private ApplicationContext context;

    /**
     * Create instance of {@code ControllerServlet}.
     *
     * @see HttpServlet#HttpServlet()
     */
    public ClientController() {
        super();
        context = new AnnotationConfigApplicationContext(AppConfig.class);
    }

    /*
     * (non-Javadoc)
     * @see javax.servlet.GenericServlet#init()
     */
    @Override
    public void init() throws ServletException {
        super.init();
        new DOMConfigurator().doConfigure(this.getServletContext().getRealPath(
                "/WEB-INF") + LOG_CONF_FILE, LogManager.getLoggerRepository());
        logger.info(String.format("Servlet started: %s", this
                .getServletName()));
    }

    /*
     * (non-Javadoc)
     * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.
     * HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /*
     * (non-Javadoc)
     * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.
     * HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    private void processRequest(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
        ICommand command = (ICommand) context.getBean(request.getParameter(
                PARAM_NAME_COMMAND));
        String page = command.execute(request);
        if (request.getAttribute("redirect") != null) {
            response.sendRedirect(page);
        } else {
            RequestDispatcher requestDispatcher = request.getRequestDispatcher(
                    page);
            requestDispatcher.forward(request, response);
        }
    }

    /*
     * (non-Javadoc)
     * @see javax.servlet.GenericServlet#destroy()
     */
    @Override
    public void destroy() {
        logger.info(String.format("Servlet destroyed: %s", this
                .getServletName()));
        super.destroy();
    }
}
