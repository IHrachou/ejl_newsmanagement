<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>index</title>
<link rel="stylesheet" href="/css/newsmanag-style.css" />
</head>
<body>
	<jsp:forward page="ClientController?command=showNewsList"/>
</body>
</html>
