<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<div class="title">
  <fmt:message bundle="${sessionScope.userlang}"
    key="message.page.header.title" />
</div>
<div class="locale clearfix">
  <p class="lang">
    <a href="ClientController?command=changeLocale&lang=en"><fmt:message
        bundle="${sessionScope.userlang}"
        key="message.page.header.lang.en" /></a>
  </p>
  <p class="lang">
    <a href="ClientController?command=changeLocale&lang=ru"><fmt:message
        bundle="${sessionScope.userlang}"
        key="message.page.header.lang.ru" /></a>
  </p>
</div>
