<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<div align="center">
  <p>
    <fmt:message bundle="${sessionScope.userlang}"
      key="message.page.footer.copyright" />
  </p>
</div>
