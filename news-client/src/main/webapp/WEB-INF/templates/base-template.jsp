<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<header class="header">
  <tiles:insertAttribute name="header" />
</header>
<main> <tiles:insertAttribute name="content" /> </main>
<footer class="footer">
  <tiles:insertAttribute name="footer" />
</footer>
