<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ctg" uri="customtags"%>
<div class="news-list">
  <section class="search-bar">
    <form name="filter" method="get" action="ClientController">
      <select class="elem-list" name="authors" multiple="multiple">
        <option disabled="disabled">Chose authors</option>
        <ctg:authorsList />
      </select> <select class="elem-list" name="tags" multiple="multiple">
        <option disabled="disabled">Chose tags</option>
        <ctg:tagsList />
      </select>
      <button type="submit" name="command" value="filterNewsList">
        <fmt:message bundle="${sessionScope.userlang}"
          key="message.page.news.list.filter" />
      </button>
      <button type="submit" name="command" value="resetFilter">
        <fmt:message bundle="${sessionScope.userlang}"
          key="message.page.news.list.reset" />
      </button>
    </form>
  </section>
  <section class="news-list">
    <c:forEach items="${newsList}" var="newsDto">
      <article>
        <div>
          <a
            href="ClientController?command=showNews&id=${newsDto.news.id }"><span><c:out
                value="${newsDto.news.title}" /></span></a> <span>(by <c:forEach
              items="${newsDto.authors}" var="author">
              <c:out value="${author.name}, " />
            </c:forEach> )
          </span><span><fmt:formatDate type="date" dateStyle="short"
              value="${newsDto.news.modificationDate }" /></span>
        </div>
        <div>
          <c:out value="${newsDto.news.shortText }" />
        </div>
        <div>
          <span><ctg:newsTagsList tagList="${newsDto.tags }" /></span>
          <span><fmt:message bundle="${sessionScope.userlang}"
              key="message.page.news.list.comments" />(<c:out
              value="${newsDto.comments.size() }" />)</span> <span><fmt:message
              bundle="${sessionScope.userlang}"
              key="message.page.news.list.view" /></span>
        </div>
      </article>
    </c:forEach>
  </section>
  <section class="pagination">
    <ctg:newsListPagination />
  </section>
</div>
