<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ctg" uri="customtags"%>
<div class="news-view">
  <section class="backlink">
    <c:url var="back"
      value="ClientController?command=showNewsList&page=${sessionScope.page}" />
    <a href="${back}"> <fmt:message
        bundle="${sessionScope.userlang}"
        key="message.page.news.view.back" /></a>
  </section>
  <article class="news-view">
    <header>
      <h1>
        <c:out value="${news.news.title }" />
      </h1>
      <p>
        <fmt:message bundle="${sessionScope.userlang}"
          key="message.page.news.view.author" />
        <ctg:newsAuthorsList />
      </p>
      <p>
        <fmt:formatDate type="date" dateStyle="short"
          value="${news.news.modificationDate }" />
      </p>
    </header>
    <section class="news-content">${news.news.fullText }</section>
    <section class="comments">
      <c:forEach var="elem" items="${news.comments }">
        <article>
          <p class="date">
            <fmt:formatDate type="DATE" dateStyle="short"
              value="${elem.creationDate }" />
          </p>
          <p class="comment">
            <c:out value="${elem.text }" />
          </p>
        </article>
      </c:forEach>
      <form name="postComment" method="post" action="ClientController">
        <input type="hidden" name="command" value="postComment" /> <input
          type="hidden" name="id" value="${news.news.id }" />
        <textarea name="commentText" rows="4" cols="60"
          required="required" maxlength="100"></textarea>
        <fmt:message var="postCommentButton"
          bundle="${sessionScope.userlang}"
          key="message.page.news.view.postcomment" />
        <input type="submit" value="${postCommentButton }" />
      </form>
    </section>
  </article>
  <section class="navigation">
    <a href="ClientController?command=showNews&id=${news.news.id }&prev">
      <fmt:message bundle="${sessionScope.userlang}"
        key="message.page.news.view.previous" />
    </a> <a
      href="ClientController?command=showNews&id=${news.news.id }&next">
      <fmt:message bundle="${sessionScope.userlang}"
        key="message.page.news.view.next" />
    </a>
  </section>
</div>
