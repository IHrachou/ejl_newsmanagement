<%@ page language="java" contentType="text/html; charset=UTF-8"
  pageEncoding="UTF-8"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>

<fmt:setBundle basename="message" var="userlang" scope="session" />
<c:set var="language" value="${userlang.locale.language}" />

<html lang="${empty language ? 'en': language }">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title><fmt:message bundle="${sessionScope.userlang}"
    key="message.page.news.list.title" /></title>
<link rel="stylesheet" href="css/news-client.css" />
</head>
<body>
  <tiles:insertDefinition name="news-list" />
</body>
</html>