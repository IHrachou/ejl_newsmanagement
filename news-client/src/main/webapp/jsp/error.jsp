<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>

<c:if test="${empty sessionScope.locale}">
	<fmt:setLocale value="en_US" scope="session" />
</c:if>

<fmt:setBundle basename="message" var="userlang" scope="session" />

<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Error page</title>
<link rel="stylesheet" href="/css/newsmanag-style.css" />
</head>
<body>
	<h1>Error page</h1>
</body>
</html>