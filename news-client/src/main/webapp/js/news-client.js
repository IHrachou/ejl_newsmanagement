// Validate max length of the posted comment
function validateForm() {
    var x = document.forms["postComment"]["commentText"].value;
    if (x == null || x == "" || x.length > 100) {
        return false;
    }
}