# README #
EPAM Java Lab

Author > Ilya Hrachou

Date > 2016.05.13

News Management application
Version 1.0

**Requirements:**
Develop News Management application. It shoul includ backend and UI layers.

Application performs next operations:

- add news (go as one step in service layer);

- edit news;

- delete news;

- view the list of news sorted by the most commented news;

- view single news message;

- add news author;

- search according to the SearchCtiteria;

- add tag/tags for news message;

- add comment(s) for news message;

- delete comment(s);

- count all news.

Each news may have more than one author and more than 1 tag.

**Modules:**

- *news-common* - backend module which provides access to the database;

- *news-client* - frontend module which provides UI for client users;

- *news-admin* - frontend module which provides UI for admin users.