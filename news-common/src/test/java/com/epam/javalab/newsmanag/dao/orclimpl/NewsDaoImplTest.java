package com.epam.javalab.newsmanag.dao.orclimpl;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;

import com.epam.javalab.newsmanag.dao.AuthorDao;
import com.epam.javalab.newsmanag.dao.NewsDao;
import com.epam.javalab.newsmanag.dao.TagDao;
import com.epam.javalab.newsmanag.domain.News;
import com.epam.javalab.newsmanag.domain.SearchCriteria;
import com.epam.javalab.newsmanag.exceptions.DaoException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;

/**
 * Unit tests for {@link NewsDao}.
 *
 * @author Ilya Hrachou
 * @version 1.1.5
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:test-spring-config.xml")
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        DbUnitTestExecutionListener.class })
@DatabaseSetup(value = "classpath:news-data.xml")
@DatabaseTearDown(value = "classpath:news-data.xml",
        type = DatabaseOperation.DELETE_ALL)
public class NewsDaoImplTest {
    @Autowired
    private NewsDao newsDao;
    @Autowired
    private AuthorDao authorDao;
    @Autowired
    private TagDao tagDao;

    // Constants for the test
    private final Long incorrectId = -1L;
    private final Long expectedNewsId = 3L;
    private final Long expectedNewsIdForUpdate = 2L;
    private final Long expectedNewsIdForCriteria = 1L;
    private final String expectedTitle = "Новость 3";
    private final String expectedShortText = "Короткий текст для новости 3";
    private final String expectedFullText = "Полный текст для новости 3";
    private final Timestamp expectedCreationDate = Timestamp.valueOf(
            "2016-01-01 09:09:09.000");
    private Date expectedModificationDate;
    private final int expectedNewsAmount = 4;
    private final int expectedNewsAmountAfterDelete = 3;
    private final int expectedNewsAmountForRange = 2;
    private final long expectedStartPos = 1;
    private final long expectedEndPos = 2;
    private final long[] expectedOrders = { 2, 1, 3, 4 };
    private final Long expectedAuthorId = 2L;
    private final Long expectedTagId = 2L;
    {
        // Set the current date without time to the expected news
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            expectedModificationDate = new Date(sdf.parse(sdf.format(
                    new java.util.Date())).getTime());
        } catch (ParseException e) {
            // Nothing to do
        }
    }

    private static final News NEWS1 = new News();
    private static final News NEWS2 = new News();
    private static final News NEWS3 = new News();
    private static final News NEWS4 = new News();

    @BeforeClass
    public static void createExpectedNews() {
        // News 1
        NEWS1.setId(1L);
        NEWS1.setTitle("Новость 1");
        NEWS1.setShortText("Короткий текст для новости 1");
        NEWS1.setFullText("Полный текст для новости 1");
        NEWS1.setCreationDate(Timestamp.valueOf("2015-07-01 11:11:11.000"));
        NEWS1.setModificationDate(Date.valueOf("2015-07-01"));
        // News 2
        NEWS2.setId(2L);
        NEWS2.setTitle("Новость 2");
        NEWS2.setShortText("Короткий текст для новости 2");
        NEWS2.setFullText("Полный текст для новости 2");
        NEWS2.setCreationDate(Timestamp.valueOf("2016-01-01 09:09:09.000"));
        NEWS2.setModificationDate(Date.valueOf("2016-01-01"));
        // News 3
        NEWS3.setId(3L);
        NEWS3.setTitle("Новость 3");
        NEWS3.setShortText("Короткий текст для новости 3");
        NEWS3.setFullText("Полный текст для новости 3");
        NEWS3.setCreationDate(Timestamp.valueOf("2015-01-15 22:22:22.000"));
        NEWS3.setModificationDate(Date.valueOf("2015-01-15"));
        // News 3
        NEWS4.setId(4L);
        NEWS4.setTitle("Новость 4");
        NEWS4.setShortText("Короткий текст для новости 4");
        NEWS4.setFullText("Полный текст для новости 4");
        NEWS4.setCreationDate(Timestamp.valueOf("2014-02-17 10:10:10.000"));
        NEWS4.setModificationDate(Date.valueOf("2014-02-17"));
    }

    @Test
    public void createTest() throws DaoException {
        News expectedNews = new News();
        expectedNews.setTitle(expectedTitle);
        expectedNews.setShortText(expectedShortText);
        expectedNews.setFullText(expectedFullText);
        Long id = newsDao.create(expectedNews);
        assertNotEquals(incorrectId, id);
        News actualNews = newsDao.findByPk(id);
        expectedNews.setCreationDate(actualNews.getCreationDate());
        expectedNews.setModificationDate(actualNews.getModificationDate());
        assertEquals(expectedNews, actualNews);
    }

    @Test
    public void createNullInputTest() throws DaoException {
        try {
            newsDao.create(null);
        } catch (Exception e) {
            assertEquals("Unable to create null News in the database", e
                    .getMessage());
            return;
        }
        fail("Expected validation exception was not thrown");
    }

    @Test
    public void findByPkTest() throws DaoException {
        News newsExpected = NEWS2;
        assertEquals(newsExpected, newsDao.findByPk(newsExpected.getId()));
    }

    @Test
    public void findByPkNullInputTest() throws DaoException {
        try {
            newsDao.findByPk(null);
        } catch (Exception e) {
            assertEquals("Primary key cannot be null", e.getMessage());
            return;
        }
        fail("Expected validation exception was not thrown");
    }

    @Test
    public void findAllTest() throws DaoException {
        List<News> news = newsDao.findAll();
        assertEquals(expectedNewsAmount, news.size());
    }

    @Test
    public void findAllOrderTest() throws DaoException {
        List<News> news = newsDao.findAll();
        long[] actualOrder = new long[news.size()];
        int i = 0;
        for (News singleNews : news) {
            actualOrder[i++] = singleNews.getId();
        }
        assertArrayEquals(expectedOrders, actualOrder);
    }

    @Test
    public void findForRangeTest() throws DaoException {
        List<News> newsList = newsDao.findForRange(expectedStartPos,
                expectedEndPos);
        assertEquals(expectedNewsAmountForRange, newsList.size());
    }

    @Test
    public void findForRangeContainsNewsTest() throws DaoException {
        List<News> newsList = newsDao.findForRange(expectedStartPos,
                expectedEndPos);
        News expectedNews1 = newsDao.findByPk(expectedStartPos);
        News expectedNews2 = newsDao.findByPk(expectedEndPos);
        assertTrue(newsList.contains(expectedNews1));
        assertTrue(newsList.contains(expectedNews2));
    }

    @Test
    public void findForRangeWithCriteriaTest() throws DaoException {
        SearchCriteria searchCriteria = new SearchCriteria();
        searchCriteria.addAuthorId(1L);
        List<News> newsList = newsDao.findForRange(expectedStartPos,
                expectedEndPos, searchCriteria);
        assertEquals(1, newsList.size());
    }

    @Test
    public void findForRangeWithCriteriaNullCriteriaTest() throws DaoException {
        assertEquals(expectedNewsAmountForRange, newsDao.findForRange(
                expectedStartPos, expectedEndPos, null).size());
    }

    @Test
    public void updateTest() throws DaoException {
        News expectedNews = new News();
        expectedNews.setId(expectedNewsIdForUpdate);
        expectedNews.setTitle(expectedTitle);
        expectedNews.setShortText(expectedShortText);
        expectedNews.setFullText(expectedFullText);
        expectedNews.setCreationDate(expectedCreationDate);
        expectedNews.setModificationDate(expectedModificationDate);
        newsDao.update(expectedNews);
        assertEquals(expectedNews, newsDao.findByPk(expectedNewsIdForUpdate));
    }

    @Test
    public void updateNullInputTest() throws DaoException {
        try {
            newsDao.update(null);
        } catch (Exception e) {
            assertEquals("Null instance cannot be recorded to the database", e
                    .getMessage());
            return;
        }
        fail("Expected validation exception was not thrown");
    }

    @Test
    public void deleteTest() throws DaoException {
        newsDao.delete(expectedNewsId);
        assertEquals(expectedNewsAmountAfterDelete, newsDao.findAll().size());
        assertNull(newsDao.findByPk(expectedNewsId));
    }

    @Test
    public void deleteNullInputTest() throws DaoException {
        try {
            newsDao.delete(null);
        } catch (Exception e) {
            assertEquals("Primary key cannot be null", e.getMessage());
            return;
        }
        fail("Expected validation exception was not thrown");
    }

    @Test
    public void countNewsTest() throws DaoException {
        assertEquals(expectedNewsAmount, newsDao.countNews());
    }

    @Test
    public void countFilteredNewsTest() throws DaoException {
        SearchCriteria searchCriteria = new SearchCriteria();
        searchCriteria.addAuthorId(1L);
        assertEquals(1, newsDao.countFilteredNews(searchCriteria));
    }

    @Test
    public void countFilteredNewsNullInputTest() throws DaoException {
        assertEquals(expectedNewsAmount, newsDao.countFilteredNews(null));
    }

    @Test
    public void addAuthorTest() throws DaoException {
        List<Long> authorIds = new ArrayList<>();
        authorIds.add(expectedAuthorId);
        newsDao.addAuthors(expectedNewsId, authorIds);
        assertEquals(1, authorDao.findByNewsId(expectedNewsId).size());
    }

    @Test
    public void addAuthorNullNewsIdTest() throws DaoException {
        List<Long> authorIds = new ArrayList<>();
        authorIds.add(1L);
        try {
            newsDao.addAuthors(null, authorIds);
        } catch (Exception e) {
            assertEquals("Primary key cannot be null", e.getMessage());
            return;
        }
        fail("Expected validation exception was not thrown");
    }

    @Test
    public void addAuthorNullAuthorIdTest() throws DaoException {
        List<Long> authorIds = new ArrayList<>();
        authorIds.add(null);
        try {
            newsDao.addAuthors(1L, authorIds);
        } catch (Exception e) {
            assertEquals("Primary key cannot be null", e.getMessage());
            return;
        }
        fail("Expected validation exception was not thrown");
    }

    @Test
    public void addAuthorNullAuthorIdListTest() throws DaoException {
        try {
            newsDao.addAuthors(1L, null);
        } catch (Exception e) {
            assertEquals("Primary key cannot be null", e.getMessage());
            return;
        }
        fail("Expected validation exception was not thrown");
    }

    @Test
    public void deleteAuthorFromNewsTest() throws DaoException {
        newsDao.deleteAuthorFromNews(1L, 1L);
        assertTrue(authorDao.findByNewsId(1L).isEmpty());
    }

    @Test
    public void deleteAuthorFromNewsNullNewsIdTest() throws DaoException {
        try {
            newsDao.deleteAuthorFromNews(null, 1L);
        } catch (Exception e) {
            assertEquals("Primary key cannot be null", e.getMessage());
            return;
        }
        fail("Expected validation exception was not thrown");
    }

    @Test
    public void deleteAuthorFromNewsNullAuthorIdTest() throws DaoException {
        try {
            newsDao.deleteAuthorFromNews(1L, null);
        } catch (Exception e) {
            assertEquals("Primary key cannot be null", e.getMessage());
            return;
        }
        fail("Expected validation exception was not thrown");
    }

    @Test
    public void deleteSeveralAuthorsFromNewsTest() throws DaoException {
        List<Long> authorsIds = new ArrayList<>();
        authorsIds.add(1L);
        newsDao.deleteSeveralAuthorsFromNews(1L, authorsIds);
        assertTrue(authorDao.findByNewsId(1L).isEmpty());
    }

    @Test
    public void deleteSeveralAuthorsFromNewsNullNewsIdTest()
            throws DaoException {
        List<Long> authorsIds = new ArrayList<>();
        authorsIds.add(1L);
        try {
            newsDao.deleteSeveralAuthorsFromNews(null, authorsIds);
        } catch (Exception e) {
            assertEquals("Primary key cannot be null", e.getMessage());
            return;
        }
        fail("Expected validation exception was not thrown");
    }

    @Test
    public void deleteSeveralAuthorsFromNewsNullAuthorListTest()
            throws DaoException {
        try {
            newsDao.deleteSeveralAuthorsFromNews(1L, null);
        } catch (Exception e) {
            assertEquals("Primary key cannot be null", e.getMessage());
            return;
        }
        fail("Expected validation exception was not thrown");
    }

    @Test
    public void deleteSeveralAuthorsFromNewsNullAuthorIdTest()
            throws DaoException {
        List<Long> authorsIds = new ArrayList<>();
        authorsIds.add(null);
        try {
            newsDao.deleteSeveralAuthorsFromNews(1L, authorsIds);
        } catch (Exception e) {
            assertEquals("Primary key cannot be null", e.getMessage());
            return;
        }
        fail("Expected validation exception was not thrown");
    }

    @Test
    public void addTagTest() throws DaoException {
        List<Long> tagIds = new ArrayList<>();
        tagIds.add(expectedTagId);
        newsDao.addTags(expectedNewsId, tagIds);
        assertEquals(1, tagDao.findByNewsId(expectedNewsId).size());
    }

    @Test
    public void addTagNullNewsIdTest() throws DaoException {
        List<Long> tagIds = new ArrayList<>();
        tagIds.add(expectedTagId);
        try {
            newsDao.addTags(null, tagIds);
        } catch (Exception e) {
            assertEquals("Primary key cannot be null", e.getMessage());
            return;
        }
        fail("Expected validation exception was not thrown");
    }

    @Test
    public void addTagNullTagIdTest() throws DaoException {
        List<Long> tagIds = new ArrayList<>();
        tagIds.add(null);
        try {
            newsDao.addTags(1L, tagIds);
        } catch (Exception e) {
            assertEquals("Primary key cannot be null", e.getMessage());
            return;
        }
        fail("Expected validation exception was not thrown");
    }

    @Test
    public void addTagNullTagIdListTest() throws DaoException {
        try {
            newsDao.addTags(1L, null);
        } catch (Exception e) {
            assertEquals("Primary key cannot be null", e.getMessage());
            return;
        }
        fail("Expected validation exception was not thrown");
    }

    @Test
    public void deleteTagFromNewsTest() throws DaoException {
        newsDao.deleteTagFromNews(1L, 1L);
        assertTrue(tagDao.findByNewsId(1L).isEmpty());
    }

    @Test
    public void deleteTagFromNewsNullNewsIdTest() throws DaoException {
        try {
            newsDao.deleteTagFromNews(null, 1L);
        } catch (Exception e) {
            assertEquals("Primary key cannot be null", e.getMessage());
            return;
        }
        fail("Expected validation exception was not thrown");
    }

    @Test
    public void deleteTagFromNewsNullTagIdTest() throws DaoException {
        try {
            newsDao.deleteTagFromNews(1L, null);
        } catch (Exception e) {
            assertEquals("Primary key cannot be null", e.getMessage());
            return;
        }
        fail("Expected validation exception was not thrown");
    }

    @Test
    public void deleteSeveralTagsFromNewsTest() throws DaoException {
        List<Long> tagsIds = new ArrayList<>();
        tagsIds.add(1L);
        newsDao.deleteSeveralTagsFromNews(1L, tagsIds);
        assertTrue(tagDao.findByNewsId(1L).isEmpty());
    }

    @Test
    public void deleteSeveralTagsFromNewsNullNewsIdTest() throws DaoException {
        List<Long> tagsIds = new ArrayList<>();
        tagsIds.add(1L);
        try {
            newsDao.deleteSeveralTagsFromNews(null, tagsIds);
        } catch (Exception e) {
            assertEquals("Primary key cannot be null", e.getMessage());
            return;
        }
        fail("Expected validation exception was not thrown");
    }

    @Test
    public void deleteSeveralTagsFromNewsNullTagListTest() throws DaoException {
        try {
            newsDao.deleteSeveralTagsFromNews(1L, null);
        } catch (Exception e) {
            assertEquals("Primary key cannot be null", e.getMessage());
            return;
        }
        fail("Expected validation exception was not thrown");
    }

    @Test
    public void deleteSeveralTagsFromNewsNullTagIdTest() throws DaoException {
        List<Long> tagsIds = new ArrayList<>();
        tagsIds.add(null);
        try {
            newsDao.deleteSeveralTagsFromNews(1L, tagsIds);
        } catch (Exception e) {
            assertEquals("Primary key cannot be null", e.getMessage());
            return;
        }
        fail("Expected validation exception was not thrown");
    }

    @Test
    public void findByCriteriaTest() throws DaoException {
        SearchCriteria criteria = new SearchCriteria();
        criteria.addAuthorId(1L);
        criteria.addTagId(1L);
        List<News> actualNewsList = newsDao.findByCriteria(criteria);
        News expectedNews = newsDao.findByPk(expectedNewsIdForCriteria);
        assertTrue(actualNewsList.contains(expectedNews));
    }

    @Test
    public void findByNullCriteriaTest() throws DaoException {
        List<News> newsList = newsDao.findByCriteria(null);
        assertEquals(expectedNewsAmount, newsList.size());
    }

    @Test
    public void findByEmptyCriteriaTest() throws DaoException {
        List<News> newsList = newsDao.findByCriteria(new SearchCriteria());
        assertEquals(expectedNewsAmount, newsList.size());
    }

    @Test
    public void findPrevNewsTest() throws DaoException {
        News newsExpected = NEWS1;
        assertEquals(newsExpected, newsDao.findPreviousNews(expectedNewsId));
    }

    @Test
    public void findPrevNewsNullInputTest() throws DaoException {
        try {
            newsDao.findPreviousNews(null);
        } catch (Exception e) {
            assertEquals("Primary key cannot be null", e.getMessage());
            return;
        }
        fail("Expected validation exception was not thrown");
    }

    @Test
    public void findNextNewsTest() throws DaoException {
        News newsExpected = NEWS4;
        assertEquals(newsExpected, newsDao.findNextNews(expectedNewsId));
    }

    @Test
    public void findNextNewsNullInputTest() throws DaoException {
        try {
            newsDao.findNextNews(null);
        } catch (Exception e) {
            assertEquals("Primary key cannot be null", e.getMessage());
            return;
        }
        fail("Expected validation exception was not thrown");
    }

    @Test
    public void findNextNewsCurrentLastTest() throws DaoException {
        assertNull(newsDao.findNextNews(4L));
    }

    @Test
    public void findPrevNewsCurrentFirstTest() throws DaoException {
        assertNull(newsDao.findPreviousNews(2L));
    }

    @Test
    public void findPrevAndNextNewsIdTest() throws DaoException {
        Long[] actualIds = newsDao.findPrevAndNextNewsId(expectedNewsId, null);
        Long[] expectedIds = { 1L, 4L };
        assertArrayEquals(expectedIds, actualIds);
    }

    @Test
    public void findPrevAndNextNewsIdIllegalInputIdTest() throws DaoException {
        assertNull(newsDao.findPrevAndNextNewsId(Long.MAX_VALUE, null));
    }

    @Test
    public void findPrevAndNextNewsIdInputFirstNewsIdTest()
            throws DaoException {
        // First news has ID=2
        assertNull(newsDao.findPrevAndNextNewsId(2L, null)[0]);
    }

    @Test
    public void findPrevAndNextNewsIdInputLastNewsIdTest() throws DaoException {
        // Last news has ID=4
        assertNull(newsDao.findPrevAndNextNewsId(4L, null)[1]);
    }

    @Test
    public void deleteNewsListTest() throws DaoException {
        Long[] ids = { 1L, 2L, 3L, 4L };
        newsDao.deleteNewsList(ids);
        assertEquals(0, newsDao.countNews());
    }

    @Test
    public void deleteNewsListNullInputTest() throws DaoException {
        long before = newsDao.countNews();
        newsDao.deleteNewsList(null);
        long after = newsDao.countNews();
        assertEquals(before, after);
    }

    @Test(expected = DaoException.class)
    public void deleteNewsListNullIdTest() throws DaoException {
        Long[] ids = { 1L, null, 3L };
        newsDao.deleteNewsList(ids);
    }
}
