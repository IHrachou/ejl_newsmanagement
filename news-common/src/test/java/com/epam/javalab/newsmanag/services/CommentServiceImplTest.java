package com.epam.javalab.newsmanag.services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import com.epam.javalab.newsmanag.dao.CommentDao;
import com.epam.javalab.newsmanag.domain.Comment;
import com.epam.javalab.newsmanag.exceptions.DaoException;
import com.epam.javalab.newsmanag.exceptions.ServiceException;
import com.epam.javalab.newsmanag.services.impl.CommentServiceImpl;

/**
 * Unit tests for {@link ICommentService}.
 *
 * @author Ilya Hrachou
 * @version 1.1.5
 */
@RunWith(MockitoJUnitRunner.class)
public class CommentServiceImplTest {
    @Mock
    private CommentDao commentDao;
    @InjectMocks
    private CommentServiceImpl commentService;

    // Expected values for the test asserts
    private final Long expectedCommentId = 4L;
    private final int expectedCommentsAmount = 5;
    private final Long expectedNewsId = 2L;

    @Test
    public void addTest() throws ServiceException, DaoException {
        when(commentDao.create(any(Comment.class))).thenReturn(
                expectedCommentId);
        Comment comment = new Comment();
        Long id = commentService.add(comment);
        verify(commentDao).create(comment);
        assertEquals(expectedCommentId, id);
    }

    @Test(expected = ServiceException.class)
    public void addNullInputTest() throws ServiceException, DaoException {
        doThrow(new DaoException()).when(commentDao).create(null);
        commentService.add(null);
    }

    @Test(expected = ServiceException.class)
    public void addFailTest() throws DaoException, ServiceException {
        doThrow(new DaoException()).when(commentDao).create(any(Comment.class));
        Comment comment = new Comment();
        commentService.add(comment);
    }

    @Test
    public void getByPkTest() throws ServiceException, DaoException {
        when(commentDao.findByPk(anyLong())).thenAnswer(new Answer<Comment>() {
            @Override
            public Comment answer(InvocationOnMock invocation)
                    throws Throwable {
                Comment comment = new Comment();
                comment.setId((Long) invocation.getArguments()[0]);
                return comment;
            }
        });
        Comment actualComment = commentService.getByPk(expectedCommentId);
        assertNotNull(actualComment);
    }

    @Test(expected = ServiceException.class)
    public void getByPkNullInputTest() throws ServiceException, DaoException {
        doThrow(new DaoException()).when(commentDao).findByPk(null);
        commentService.getByPk(null);
    }

    @Test(expected = ServiceException.class)
    public void getByPkFailTest() throws DaoException, ServiceException {
        doThrow(new DaoException()).when(commentDao).findByPk(anyLong());
        commentService.getByPk(expectedCommentId);
    }

    @SuppressWarnings("serial")
    @Test
    public void getAllTest() throws ServiceException, DaoException {
        when(commentDao.findAll()).thenReturn(new ArrayList<Comment>() {
            {
                for (int i = 0; i < expectedCommentsAmount; i++) {
                    add(new Comment());
                }
            }
        });
        List<Comment> comments = commentService.getAll();
        assertEquals(expectedCommentsAmount, comments.size());
    }

    @Test(expected = ServiceException.class)
    public void getAllFailedTest() throws DaoException, ServiceException {
        doThrow(new DaoException()).when(commentDao).findAll();
        commentService.getAll();
    }

    @Test
    public void editTest() throws ServiceException, DaoException {
        doNothing().when(commentDao).update(any(Comment.class));
        commentService.edit(new Comment());
        verify(commentDao).update(any(Comment.class));
    }

    @Test(expected = ServiceException.class)
    public void editNullInputTest() throws DaoException, ServiceException {
        doThrow(new DaoException()).when(commentDao).update(null);
        commentService.edit(null);
    }

    @Test(expected = ServiceException.class)
    public void editFailedTest() throws DaoException, ServiceException {
        doThrow(new DaoException()).when(commentDao).update(any(Comment.class));
        commentService.edit(new Comment());
    }

    @Test
    public void deleteTest() throws ServiceException, DaoException {
        doNothing().when(commentDao).delete(anyLong());
        commentService.delete(expectedCommentId);
        verify(commentDao).delete(anyLong());
    }

    @Test(expected = ServiceException.class)
    public void deleteNullInputTest() throws DaoException, ServiceException {
        doThrow(new DaoException()).when(commentDao).delete(null);
        commentService.delete(null);
    }

    @Test(expected = ServiceException.class)
    public void deleteFailedTest() throws DaoException, ServiceException {
        doThrow(new DaoException()).when(commentDao).delete(anyLong());
        commentService.delete(anyLong());
    }

    @SuppressWarnings("serial")
    @Test
    public void getByNewsIdTest() throws ServiceException, DaoException {
        when(commentDao.findByNewsId(anyLong())).thenReturn(
                new ArrayList<Comment>() {
                    {
                        for (int i = 0; i < expectedCommentsAmount; i++) {
                            add(new Comment());
                        }
                    }
                });
        List<Comment> actualComments = commentService.getByNewsId(
                expectedNewsId);
        assertEquals(expectedCommentsAmount, actualComments.size());
    }

    @Test(expected = ServiceException.class)
    public void getByNewsIdNullInputTest() throws DaoException,
            ServiceException {
        doThrow(new DaoException()).when(commentDao).findByNewsId(null);
        commentService.getByNewsId(null);
    }

    @Test(expected = ServiceException.class)
    public void getByNewsIdFailedTest() throws DaoException, ServiceException {
        doThrow(new DaoException()).when(commentDao).findByNewsId(anyLong());
        commentService.getByNewsId(anyLong());
    }
}
