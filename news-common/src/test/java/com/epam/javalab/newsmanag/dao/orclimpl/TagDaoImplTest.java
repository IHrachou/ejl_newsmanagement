package com.epam.javalab.newsmanag.dao.orclimpl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;

import com.epam.javalab.newsmanag.dao.TagDao;
import com.epam.javalab.newsmanag.domain.Tag;
import com.epam.javalab.newsmanag.exceptions.DaoException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;

/**
 * Unit tests for {@link TagDao}.
 *
 * @author Ilya Hrachou
 * @version 1.1.5
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:test-spring-config.xml")
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        DbUnitTestExecutionListener.class })
@DatabaseSetup(value = "classpath:tag-data.xml")
@DatabaseTearDown(value = "classpath:tag-data.xml",
        type = DatabaseOperation.DELETE_ALL)
public class TagDaoImplTest {
    @Autowired
    private TagDao tagDao;

    // Constants for the test
    private final Long incorrectId = -1L;
    private final Long expectedTagId = 3L;
    private final Long expectedNewsId = 1L;
    private final String expectedName = "тэг 3";
    private final int expectedTagsAmount = 3;
    private final int expectedTagsAmountForNews = 2;
    private final int expectedTagsAmountAfterDelete = 2;
    private final Long[] expectedIds = { 2L, 3L };

    @Test
    public void createTest() throws DaoException {
        Tag expectedTag = new Tag();
        expectedTag.setName(expectedName);
        Long id = tagDao.create(expectedTag);
        assertNotEquals(incorrectId, id);
        assertEquals(expectedTag, tagDao.findByPk(id));
    }

    @Test
    public void createNullInputTest() throws DaoException {
        try {
            tagDao.create(null);
        } catch (Exception e) {
            assertEquals("Unable to create null Tag in the database", e
                    .getMessage());
            return;
        }
        fail("Expected validation exception was not thrown");
    }

    @Test
    public void findByPkTest() throws DaoException {
        Tag tagExpected = new Tag();
        tagExpected.setId(expectedTagId);
        tagExpected.setName(expectedName);
        Tag tagActual = tagDao.findByPk(expectedTagId);
        assertEquals(tagExpected, tagActual);
    }

    @Test
    public void findByPkNullInputTest() throws DaoException {
        try {
            tagDao.findByPk(null);
        } catch (Exception e) {
            assertEquals("Primary key cannot be null", e.getMessage());
            return;
        }
        fail("Expected validation exception was not thrown");
    }

    @Test
    public void findByNameTest() throws DaoException {
        Tag tagExpected = new Tag();
        tagExpected.setId(expectedTagId);
        tagExpected.setName(expectedName);
        Tag tagActual = tagDao.findByName(expectedName);
        assertEquals(tagExpected, tagActual);
    }

    @Test
    public void findByNameNullInputTest() throws DaoException {
        try {
            tagDao.findByName(null);
        } catch (Exception e) {
            assertEquals("Tag name cannot be null", e.getMessage());
            return;
        }
        fail("Expected validation exception was not thrown");
    }

    @Test
    public void findAllTest() throws DaoException {
        List<Tag> tags = tagDao.findAll();
        assertEquals(expectedTagsAmount, tags.size());
    }

    @Test
    public void updateTest() throws DaoException {
        Tag expectedTag = new Tag();
        expectedTag.setId(expectedTagId);
        expectedTag.setName(expectedName);
        tagDao.update(expectedTag);
        Tag actualTag = tagDao.findByPk(expectedTagId);
        assertEquals(expectedTag, actualTag);
    }

    @Test
    public void updateNullInputTest() throws DaoException {
        try {
            tagDao.update(null);
        } catch (Exception e) {
            assertEquals("Null instance cannot be recorded to the database", e
                    .getMessage());
            return;
        }
        fail("Expected validation exception was not thrown");
    }

    @Test(expected = DaoException.class)
    public void deleteTest() throws DaoException {
        tagDao.delete(expectedTagId);
        assertEquals(expectedTagsAmountAfterDelete, tagDao.findAll().size());
        tagDao.findByPk(expectedTagId);
    }

    @Test
    public void deleteNullInputTest() throws DaoException {
        try {
            tagDao.delete(null);
        } catch (Exception e) {
            assertEquals("Primary key cannot be null", e.getMessage());
            return;
        }
        fail("Expected validation exception was not thrown");
    }

    @Test
    public void findByNewsIdTest() throws DaoException {
        List<Tag> tags = tagDao.findByNewsId(expectedNewsId);
        assertEquals(expectedTagsAmountForNews, tags.size());
    }

    @Test
    public void findByNewsIdNullInputTest() throws DaoException {
        try {
            tagDao.findByNewsId(null);
        } catch (Exception e) {
            assertEquals("Primary key cannot be null", e.getMessage());
            return;
        }
        fail("Expected validation exception was not thrown");
    }

    @Test
    public void findListByIdsTest() throws DaoException {
        List<Tag> actualTags = tagDao.findListByIds(expectedIds);
        List<Tag> expectedTags = new ArrayList<>();
        for (Long id : expectedIds) {
            expectedTags.add(tagDao.findByPk(id));
        }
        assertEquals(expectedTags, actualTags);
    }

    @Test
    public void findListByIdsNullInputTest() throws DaoException {
        try {
            tagDao.findListByIds(null);
        } catch (Exception e) {
            assertEquals("Input array cannot be null", e.getMessage());
            return;
        }
        fail("Expected validation exception was not thrown");
    }
}
