package com.epam.javalab.newsmanag.services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import com.epam.javalab.newsmanag.dao.AuthorDao;
import com.epam.javalab.newsmanag.domain.Author;
import com.epam.javalab.newsmanag.exceptions.DaoException;
import com.epam.javalab.newsmanag.exceptions.ServiceException;
import com.epam.javalab.newsmanag.services.impl.AuthorServiceImpl;

/**
 * Unit tests for {@link IAuthorService}.
 *
 * @author Ilya Hrachou
 * @version 1.1.5
 */
@RunWith(MockitoJUnitRunner.class)
public class AuthorServiceImplTest {
    @Mock
    private AuthorDao authorDao;
    @InjectMocks
    private AuthorServiceImpl authorService;

    // Expected values for the test asserts
    private static final Long EXPECTED_AUTHOR_ID = 4L;
    private static final long EXPECTED_AUTHORS_AMOUNT = 5;
    private static final long EXPECTED_NEWS_ID = 2;
    private static final Long[] ID_LIST = { 2L, 4L, 6L };

    @Test
    public void addTest() throws ServiceException, DaoException {
        when(authorDao.create(any(Author.class))).thenReturn(
                EXPECTED_AUTHOR_ID);
        Author author = new Author();
        Long id = authorService.add(author);
        verify(authorDao).create(author);
        assertEquals(EXPECTED_AUTHOR_ID, id);
    }

    @Test(expected = ServiceException.class)
    public void addNullInputTest() throws ServiceException, DaoException {
        doThrow(new DaoException()).when(authorDao).create(null);
        authorService.add(null);
    }

    @Test(expected = ServiceException.class)
    public void addFailTest() throws DaoException, ServiceException {
        doThrow(new DaoException()).when(authorDao).create(any(Author.class));
        Author author = new Author();
        authorService.add(author);
    }

    @Test
    public void getByPkTest() throws ServiceException, DaoException {
        when(authorDao.findByPk(anyLong())).thenAnswer(new Answer<Author>() {
            @Override
            public Author answer(InvocationOnMock invocation) throws Throwable {
                Author author = new Author();
                author.setId((Long) invocation.getArguments()[0]);
                return author;
            }
        });
        Author actualAuthor = authorService.getByPk(EXPECTED_AUTHOR_ID);
        assertNotNull(actualAuthor);
    }

    @Test(expected = ServiceException.class)
    public void getByPkNullInputTest() throws ServiceException, DaoException {
        doThrow(new DaoException()).when(authorDao).findByPk(null);
        authorService.getByPk(null);
    }

    @Test(expected = ServiceException.class)
    public void getByPkFailTest() throws DaoException, ServiceException {
        doThrow(new DaoException()).when(authorDao).findByPk(anyLong());
        authorService.getByPk(EXPECTED_AUTHOR_ID);
    }

    @SuppressWarnings("serial")
    @Test
    public void getAllTest() throws ServiceException, DaoException {
        when(authorDao.findAll()).thenReturn(new ArrayList<Author>() {
            {
                for (int i = 0; i < EXPECTED_AUTHORS_AMOUNT; i++) {
                    add(new Author());
                }
            }
        });
        List<Author> authors = authorService.getAll();
        assertEquals(EXPECTED_AUTHORS_AMOUNT, authors.size());
    }

    @Test(expected = ServiceException.class)
    public void getAllFailedTest() throws DaoException, ServiceException {
        doThrow(new DaoException()).when(authorDao).findAll();
        authorService.getAll();
    }

    @SuppressWarnings("serial")
    @Test
    public void getNonExpiredTest() throws ServiceException, DaoException {
        when(authorDao.findNonExpired()).thenReturn(new ArrayList<Author>() {
            {
                for (int i = 0; i < EXPECTED_AUTHORS_AMOUNT; i++) {
                    add(new Author());
                }
            }
        });
        List<Author> authors = authorService.getNonExpired();
        assertEquals(EXPECTED_AUTHORS_AMOUNT, authors.size());
    }

    @Test(expected = ServiceException.class)
    public void getNonExpiredFailedTest() throws DaoException,
            ServiceException {
        doThrow(new DaoException()).when(authorDao).findNonExpired();
        authorService.getNonExpired();
    }

    @Test
    public void editTest() throws ServiceException, DaoException {
        doNothing().when(authorDao).update(any(Author.class));
        authorService.edit(new Author());
        verify(authorDao).update(any(Author.class));
    }

    @Test(expected = ServiceException.class)
    public void editNullInputTest() throws DaoException, ServiceException {
        doThrow(new DaoException()).when(authorDao).update(null);
        authorService.edit(null);
    }

    @Test(expected = ServiceException.class)
    public void editFailedTest() throws DaoException, ServiceException {
        doThrow(new DaoException()).when(authorDao).update(any(Author.class));
        authorService.edit(new Author());
    }

    @Test
    public void deleteTest() throws ServiceException, DaoException {
        doNothing().when(authorDao).delete(anyLong());
        authorService.delete(EXPECTED_AUTHOR_ID);
        verify(authorDao).delete(anyLong());
    }

    @Test(expected = ServiceException.class)
    public void deleteNullInputTest() throws DaoException, ServiceException {
        doThrow(new DaoException()).when(authorDao).delete(null);
        authorService.delete(null);
    }

    @Test(expected = ServiceException.class)
    public void deleteFailedTest() throws DaoException, ServiceException {
        doThrow(new DaoException()).when(authorDao).delete(anyLong());
        authorService.delete(anyLong());
    }

    @Test
    public void expireTest() throws ServiceException, DaoException {
        doNothing().when(authorDao).expire(anyLong());
        authorService.expire(EXPECTED_AUTHOR_ID);
        verify(authorDao).expire(EXPECTED_AUTHOR_ID);
    }

    @Test(expected = ServiceException.class)
    public void expireNullInputTest() throws DaoException, ServiceException {
        doThrow(new DaoException()).when(authorDao).expire(null);
        authorService.expire(null);
    }

    @Test(expected = ServiceException.class)
    public void expireFailedTest() throws DaoException, ServiceException {
        doThrow(new DaoException()).when(authorDao).expire(anyLong());
        authorService.expire(anyLong());
    }

    @SuppressWarnings("serial")
    @Test
    public void getByNewsIdTest() throws ServiceException, DaoException {
        when(authorDao.findByNewsId(anyLong())).thenReturn(
                new ArrayList<Author>() {
                    {
                        for (int i = 0; i < EXPECTED_AUTHORS_AMOUNT; i++) {
                            add(new Author());
                        }
                    }
                });
        List<Author> actualAuthors = authorService.getByNewsId(
                EXPECTED_NEWS_ID);
        assertEquals(EXPECTED_AUTHORS_AMOUNT, actualAuthors.size());
    }

    @Test(expected = ServiceException.class)
    public void getByNewsIdNullInputTest() throws DaoException,
            ServiceException {
        doThrow(new DaoException()).when(authorDao).findByNewsId(null);
        authorService.getByNewsId(null);
    }

    @Test(expected = ServiceException.class)
    public void getByNewsIdFailedTest() throws DaoException, ServiceException {
        doThrow(new DaoException()).when(authorDao).findByNewsId(anyLong());
        authorService.getByNewsId(anyLong());
    }

    @Test
    public void getListByIdsTest() throws ServiceException, DaoException {
        when(authorDao.findListByIds(any(Long[].class))).thenAnswer(
                new Answer<List<Author>>() {

                    @Override
                    public List<Author> answer(InvocationOnMock invocation)
                            throws Throwable {
                        List<Author> authors = new ArrayList<>();
                        for (Long long1 : (Long[]) invocation
                                .getArguments()[0]) {
                            Author author = new Author();
                            author.setId(long1);
                            authors.add(author);
                        }
                        return authors;
                    }
                });
        List<Author> actualAuthors = authorService.getListByIds(ID_LIST);
        assertEquals(ID_LIST.length, actualAuthors.size());
    }

    @Test(expected = ServiceException.class)
    public void getListByIdsNullInputTest() throws DaoException,
            ServiceException {
        doThrow(new DaoException()).when(authorDao).findListByIds(null);
        authorService.getListByIds(null);
    }

    @Test(expected = ServiceException.class)
    public void getListByIdsFailedTest() throws DaoException, ServiceException {
        doThrow(new DaoException()).when(authorDao).findListByIds(any(
                Long[].class));
        authorService.getListByIds(ID_LIST);
    }
}
