package com.epam.javalab.newsmanag.dao.orclimpl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.fail;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;

import com.epam.javalab.newsmanag.dao.UserDao;
import com.epam.javalab.newsmanag.domain.User;
import com.epam.javalab.newsmanag.exceptions.DaoException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;

/**
 * Unit tests for {@link UserDao}.
 *
 * @author Ilya Hrachou
 * @version 1.1.7
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:test-spring-config.xml")
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        DbUnitTestExecutionListener.class })
@DatabaseSetup(value = "classpath:user-data.xml")
@DatabaseTearDown(value = "classpath:user-data.xml",
        type = DatabaseOperation.DELETE_ALL)
public class UserDaoImplTest {
    @Autowired
    private UserDao userDao;

    // Constants for the test
    private final Long incorrectId = -1L;
    private final Long expectedUserId = 3L;
    private final Long expectedUserIdForUpdate = 2L;
    private final String expectedName = "пользователь 3";
    private final String expectedLogin = "login3";
    private final String expectedPass = "pass3";
    private final long expectedRoleId = 2L;
    private final int expectedUsersAmount = 3;
    private final int expectedUsersAmountAfterDelete = 2;

    @Test
    public void createTest() throws DaoException {
        User expectedUser = new User();
        expectedUser.setName(expectedName);
        expectedUser.setLogin(expectedLogin);
        expectedUser.setPassword(expectedPass);
        expectedUser.setRoleId(expectedRoleId);
        Long id = userDao.create(expectedUser);
        assertNotEquals(incorrectId, id);
        assertEquals(expectedUser, userDao.findByPk(id));
    }

    @Test
    public void createNullInputTest() throws DaoException {
        try {
            userDao.create(null);
        } catch (Exception e) {
            assertEquals("Unable to create null User in the database", e
                    .getMessage());
            return;
        }
        fail("Expected validation exception was not thrown");
    }

    @Test
    public void findByPkTest() throws DaoException {
        User expectedUser = new User();
        expectedUser.setId(expectedUserId);
        expectedUser.setName(expectedName);
        expectedUser.setLogin(expectedLogin);
        expectedUser.setPassword(expectedPass);
        expectedUser.setRoleId(expectedRoleId);
        assertEquals(expectedUser, userDao.findByPk(expectedUserId));
    }

    @Test
    public void findByPkNullInputTest() throws DaoException {
        try {
            userDao.findByPk(null);
        } catch (Exception e) {
            assertEquals("Primary key cannot be null", e.getMessage());
            return;
        }
        fail("Expected validation exception was not thrown");
    }

    @Test
    public void findByLoginTest() throws DaoException {
        User expectedUser = new User();
        expectedUser.setId(expectedUserId);
        expectedUser.setName(expectedName);
        expectedUser.setLogin(expectedLogin);
        expectedUser.setPassword(expectedPass);
        expectedUser.setRoleId(expectedRoleId);
        assertEquals(expectedUser, userDao.findByLogin(expectedLogin));
    }

    @Test
    public void findByLoginNullInputTest() throws DaoException {
        try {
            userDao.findByLogin(null);
        } catch (Exception e) {
            assertEquals("User login cannot be null or empty", e.getMessage());
            return;
        }
        fail("Expected validation exception was not thrown");
    }

    @Test
    public void findAllTest() throws DaoException {
        List<User> users = userDao.findAll();
        assertEquals(expectedUsersAmount, users.size());
    }

    @Test
    public void updateTest() throws DaoException {
        User expectedUser = new User();
        expectedUser.setId(expectedUserIdForUpdate);
        expectedUser.setName(expectedName);
        expectedUser.setLogin(expectedLogin);
        expectedUser.setPassword(expectedPass);
        expectedUser.setRoleId(expectedRoleId);
        userDao.update(expectedUser);
        assertEquals(expectedUser, userDao.findByPk(expectedUserIdForUpdate));
    }

    @Test
    public void updateNullInputTest() throws DaoException {
        try {
            userDao.update(null);
        } catch (Exception e) {
            assertEquals("Null instance cannot be recorded to the database", e
                    .getMessage());
            return;
        }
        fail("Expected validation exception was not thrown");
    }

    @Test(expected = DaoException.class)
    public void deleteTest() throws DaoException {
        userDao.delete(expectedUserId);
        assertEquals(expectedUsersAmountAfterDelete, userDao.findAll().size());
        userDao.findByPk(expectedUserId);
    }

    @Test
    public void deleteNullInputTest() throws DaoException {
        try {
            userDao.delete(null);
        } catch (Exception e) {
            assertEquals("Primary key cannot be null", e.getMessage());
            return;
        }
        fail("Expected validation exception was not thrown");
    }
}
