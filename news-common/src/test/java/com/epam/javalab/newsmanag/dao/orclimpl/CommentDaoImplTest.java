package com.epam.javalab.newsmanag.dao.orclimpl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.fail;

import java.sql.Timestamp;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;

import com.epam.javalab.newsmanag.dao.CommentDao;
import com.epam.javalab.newsmanag.domain.Comment;
import com.epam.javalab.newsmanag.exceptions.DaoException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;

/**
 * Unit tests for {@link CommentDao}.
 *
 * @author Ilya Hrachou
 * @version 1.1.5
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:test-spring-config.xml")
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        DbUnitTestExecutionListener.class })
@DatabaseSetup(value = "classpath:comment-data.xml")
@DatabaseTearDown(type = DatabaseOperation.DELETE_ALL,
        value = "classpath:comment-data.xml")
public class CommentDaoImplTest {
    @Autowired
    private CommentDao commentDao;

    // Constants for the test
    private final Long incorrectId = -1L;
    private final Long expectedCommentId = 2L;
    private final Long expectedCommentIdForUpdate = 3L;
    private final Long expectedNewsId = 2L;
    private final String expectedText = "Текст комментария 2";
    private final Timestamp expectedCreationDate = Timestamp.valueOf(
            "2016-03-10 11:11:11");
    private final int expectedCommentsAmount = 3;
    private final int expectedCommentsAmountAfterDelete = 2;
    private final int expectedCommentsAmountForNews = 1;

    @Test
    public void createTest() throws DaoException {
        Comment expectedComment = new Comment();
        expectedComment.setNewsId(expectedNewsId);
        expectedComment.setText(expectedText);
        Long id = commentDao.create(expectedComment);
        assertNotEquals(incorrectId, id);
        Comment actualComment = commentDao.findByPk(id);
        expectedComment.setCreationDate(actualComment.getCreationDate());
        assertEquals(expectedComment, actualComment);
    }

    @Test
    public void createNullInputTest() throws DaoException {
        try {
            commentDao.create(null);
        } catch (Exception e) {
            assertEquals("Unable to create null Comment in the database", e
                    .getMessage());
            return;
        }
        fail("Expected validation exception was not thrown");
    }

    @Test
    public void findByPkTest() throws DaoException {
        Comment commentExpected = new Comment();
        commentExpected.setId(expectedCommentId);
        commentExpected.setNewsId(expectedNewsId);
        commentExpected.setText(expectedText);
        commentExpected.setCreationDate(expectedCreationDate);
        assertEquals(commentExpected, commentDao.findByPk(expectedCommentId));
    }

    @Test
    public void findByPkNullInputTest() throws DaoException {
        try {
            commentDao.findByPk(null);
        } catch (Exception e) {
            assertEquals("Primary key cannot be null", e.getMessage());
            return;
        }
        fail("Expected validation exception was not thrown");
    }

    @Test
    public void findAllTest() throws DaoException {
        List<Comment> comments = commentDao.findAll();
        assertEquals(expectedCommentsAmount, comments.size());
    }

    @Test
    public void updateTest() throws DaoException {
        Comment expectedComment = new Comment();
        expectedComment.setId(expectedCommentIdForUpdate);
        expectedComment.setNewsId(expectedNewsId);
        expectedComment.setText(expectedText);
        expectedComment.setCreationDate(expectedCreationDate);
        commentDao.update(expectedComment);
        assertEquals(expectedComment, commentDao.findByPk(
                expectedCommentIdForUpdate));
    }

    @Test
    public void updateNullInputTest() throws DaoException {
        try {
            commentDao.update(null);
        } catch (Exception e) {
            assertEquals("Null instance cannot be recorded to the database", e
                    .getMessage());
            return;
        }
        fail("Expected validation exception was not thrown");
    }

    @Test(expected = DaoException.class)
    public void deleteTest() throws DaoException {
        commentDao.delete(expectedCommentId);
        assertEquals(expectedCommentsAmountAfterDelete, commentDao.findAll()
                .size());
        commentDao.findByPk(expectedCommentId);
    }

    @Test
    public void deleteNullInputTest() throws DaoException {
        try {
            commentDao.delete(null);
        } catch (Exception e) {
            assertEquals("Primary key cannot be null", e.getMessage());
            return;
        }
        fail("Expected validation exception was not thrown");
    }

    @Test
    public void findByNewsIdTest() throws DaoException {
        List<Comment> comments = commentDao.findByNewsId(expectedNewsId);
        assertEquals(expectedCommentsAmountForNews, comments.size());
    }

    @Test
    public void findByNewsIdNullInputTest() throws DaoException {
        try {
            commentDao.findByNewsId(null);
        } catch (Exception e) {
            assertEquals("Primary key cannot be null", e.getMessage());
            return;
        }
        fail("Expected validation exception was not thrown");
    }
}
