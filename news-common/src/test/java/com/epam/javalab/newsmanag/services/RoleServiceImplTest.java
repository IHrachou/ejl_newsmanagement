package com.epam.javalab.newsmanag.services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import com.epam.javalab.newsmanag.dao.RoleDao;
import com.epam.javalab.newsmanag.domain.Role;
import com.epam.javalab.newsmanag.exceptions.DaoException;
import com.epam.javalab.newsmanag.exceptions.ServiceException;
import com.epam.javalab.newsmanag.services.impl.RoleServiceImpl;

/**
 * Unit tests for {@link IRoleService}.
 *
 * @author Ilya Hrachou
 * @version 1.0
 */
@RunWith(MockitoJUnitRunner.class)
public class RoleServiceImplTest {
    @Mock
    private RoleDao roleDao;
    @InjectMocks
    private RoleServiceImpl roleService;

    // Expected values for the test asserts
    private Long expectedRoleId = 4L;
    private long expectedRolesAmount = 5;

    @Test
    public void addTest() throws ServiceException, DaoException {
        when(roleDao.create(any(Role.class))).thenReturn(expectedRoleId);
        Role role = new Role();
        Long id = roleService.add(role);
        verify(roleDao).create(role);
        assertEquals(expectedRoleId, id);
    }

    @Test(expected = ServiceException.class)
    public void addNullInputTest() throws ServiceException, DaoException {
        doThrow(new DaoException()).when(roleDao).create(null);
        roleService.add(null);
    }

    @Test(expected = ServiceException.class)
    public void addFailTest() throws DaoException, ServiceException {
        doThrow(new DaoException()).when(roleDao).create(any(Role.class));
        Role role = new Role();
        roleService.add(role);
    }

    @Test
    public void getByPkTest() throws ServiceException, DaoException {
        when(roleDao.findByPk(anyLong())).thenAnswer(new Answer<Role>() {
            @Override
            public Role answer(InvocationOnMock invocation) throws Throwable {
                Role role = new Role();
                role.setId((Long) invocation.getArguments()[0]);
                return role;
            }
        });
        Role actualRole = roleService.getByPk(expectedRoleId);
        assertNotNull(actualRole);
    }

    @Test(expected = ServiceException.class)
    public void getByPkNullInputTest() throws ServiceException, DaoException {
        doThrow(new DaoException()).when(roleDao).findByPk(null);
        roleService.getByPk(null);
    }

    @Test(expected = ServiceException.class)
    public void getByPkFailTest() throws DaoException, ServiceException {
        doThrow(new DaoException()).when(roleDao).findByPk(anyLong());
        roleService.getByPk(expectedRoleId);
    }

    @SuppressWarnings("serial")
    @Test
    public void getAllTest() throws ServiceException, DaoException {
        when(roleDao.findAll()).thenReturn(new ArrayList<Role>() {
            {
                for (int i = 0; i < expectedRolesAmount; i++) {
                    add(new Role());
                }
            }
        });
        List<Role> roles = roleService.getAll();
        assertEquals(expectedRolesAmount, roles.size());
    }

    @Test(expected = ServiceException.class)
    public void getAllFailedTest() throws DaoException, ServiceException {
        doThrow(new DaoException()).when(roleDao).findAll();
        roleService.getAll();
    }

    @Test
    public void editTest() throws ServiceException, DaoException {
        doNothing().when(roleDao).update(any(Role.class));
        roleService.edit(new Role());
        verify(roleDao).update(any(Role.class));
    }

    @Test(expected = ServiceException.class)
    public void editNullInputTest() throws DaoException, ServiceException {
        doThrow(new DaoException()).when(roleDao).update(null);
        roleService.edit(null);
    }

    @Test(expected = ServiceException.class)
    public void editFailedTest() throws DaoException, ServiceException {
        doThrow(new DaoException()).when(roleDao).update(any(Role.class));
        roleService.edit(new Role());
    }

    @Test
    public void deleteTest() throws ServiceException, DaoException {
        doNothing().when(roleDao).delete(anyLong());
        roleService.delete(expectedRoleId);
        verify(roleDao).delete(anyLong());
    }

    @Test(expected = ServiceException.class)
    public void deleteNullInputTest() throws DaoException, ServiceException {
        doThrow(new DaoException()).when(roleDao).delete(null);
        roleService.delete(null);
    }

    @Test(expected = ServiceException.class)
    public void deleteFailedTest() throws DaoException, ServiceException {
        doThrow(new DaoException()).when(roleDao).delete(anyLong());
        roleService.delete(anyLong());
    }
}
