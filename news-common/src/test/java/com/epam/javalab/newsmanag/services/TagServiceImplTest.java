package com.epam.javalab.newsmanag.services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import com.epam.javalab.newsmanag.dao.TagDao;
import com.epam.javalab.newsmanag.domain.Tag;
import com.epam.javalab.newsmanag.exceptions.DaoException;
import com.epam.javalab.newsmanag.exceptions.ServiceException;
import com.epam.javalab.newsmanag.services.impl.TagServiceImpl;

/**
 * Unit tests for {@link ITagService}.
 *
 * @author Ilya Hrachou
 * @version 1.1.5
 */
@RunWith(MockitoJUnitRunner.class)
public class TagServiceImplTest {
    @Mock
    private TagDao tagDao;
    @InjectMocks
    private TagServiceImpl tagService;

    // Expected values for the test asserts
    private final Long expectedTagId = 4L;
    private final long expectedTagsAmount = 5;
    private final long expectedNewsId = 2;
    private static final Long[] ID_LIST = { 2L, 4L, 6L };

    @Test
    public void addTest() throws ServiceException, DaoException {
        when(tagDao.create(any(Tag.class))).thenReturn(expectedTagId);
        Tag tag = new Tag();
        Long id = tagService.add(tag);
        verify(tagDao).create(tag);
        assertEquals(expectedTagId, id);
    }

    @Test(expected = ServiceException.class)
    public void addNullInputTest() throws ServiceException, DaoException {
        doThrow(new DaoException()).when(tagDao).create(null);
        tagService.add(null);
    }

    @Test(expected = ServiceException.class)
    public void addFailTest() throws DaoException, ServiceException {
        doThrow(new DaoException()).when(tagDao).create(any(Tag.class));
        Tag tag = new Tag();
        tagService.add(tag);
    }

    @Test
    public void getByPkTest() throws ServiceException, DaoException {
        when(tagDao.findByPk(anyLong())).thenAnswer(new Answer<Tag>() {
            @Override
            public Tag answer(InvocationOnMock invocation) throws Throwable {
                Tag tag = new Tag();
                tag.setId((Long) invocation.getArguments()[0]);
                return tag;
            }
        });
        Tag actualTag = tagService.getByPk(expectedTagId);
        assertNotNull(actualTag);
    }

    @Test(expected = ServiceException.class)
    public void getByPkNullInputTest() throws ServiceException, DaoException {
        doThrow(new DaoException()).when(tagDao).findByPk(null);
        tagService.getByPk(null);
    }

    @Test(expected = ServiceException.class)
    public void getByPkFailTest() throws DaoException, ServiceException {
        doThrow(new DaoException()).when(tagDao).findByPk(anyLong());
        tagService.getByPk(expectedTagId);
    }

    @SuppressWarnings("serial")
    @Test
    public void getAllTest() throws ServiceException, DaoException {
        when(tagDao.findAll()).thenReturn(new ArrayList<Tag>() {
            {
                for (int i = 0; i < expectedTagsAmount; i++) {
                    add(new Tag());
                }
            }
        });
        List<Tag> tags = tagService.getAll();
        assertEquals(expectedTagsAmount, tags.size());
    }

    @Test(expected = ServiceException.class)
    public void getAllFailedTest() throws DaoException, ServiceException {
        doThrow(new DaoException()).when(tagDao).findAll();
        tagService.getAll();
    }

    @Test
    public void editTest() throws ServiceException, DaoException {
        doNothing().when(tagDao).update(any(Tag.class));
        tagService.edit(new Tag());
        verify(tagDao).update(any(Tag.class));
    }

    @Test(expected = ServiceException.class)
    public void editNullInputTest() throws DaoException, ServiceException {
        doThrow(new DaoException()).when(tagDao).update(null);
        tagService.edit(null);
    }

    @Test(expected = ServiceException.class)
    public void editFailedTest() throws DaoException, ServiceException {
        doThrow(new DaoException()).when(tagDao).update(any(Tag.class));
        tagService.edit(new Tag());
    }

    @Test
    public void deleteTest() throws ServiceException, DaoException {
        doNothing().when(tagDao).delete(anyLong());
        tagService.delete(expectedTagId);
        verify(tagDao).delete(anyLong());
    }

    @Test(expected = ServiceException.class)
    public void deleteNullInputTest() throws DaoException, ServiceException {
        doThrow(new DaoException()).when(tagDao).delete(null);
        tagService.delete(null);
    }

    @Test(expected = ServiceException.class)
    public void deleteFailedTest() throws DaoException, ServiceException {
        doThrow(new DaoException()).when(tagDao).delete(anyLong());
        tagService.delete(anyLong());
    }

    @SuppressWarnings("serial")
    @Test
    public void getByNewsIdTest() throws ServiceException, DaoException {
        when(tagDao.findByNewsId(anyLong())).thenReturn(new ArrayList<Tag>() {
            {
                for (int i = 0; i < expectedTagsAmount; i++) {
                    add(new Tag());
                }
            }
        });
        List<Tag> actualTags = tagService.getByNewsId(expectedNewsId);
        assertEquals(expectedTagsAmount, actualTags.size());
    }

    @Test(expected = ServiceException.class)
    public void getByNewsIdNullInputTest() throws DaoException,
            ServiceException {
        doThrow(new DaoException()).when(tagDao).findByNewsId(null);
        tagService.getByNewsId(null);
    }

    @Test(expected = ServiceException.class)
    public void getByNewsIdFailedTest() throws DaoException, ServiceException {
        doThrow(new DaoException()).when(tagDao).findByNewsId(anyLong());
        tagService.getByNewsId(anyLong());
    }

    @Test
    public void getListByIdsTest() throws ServiceException, DaoException {
        when(tagDao.findListByIds(any(Long[].class))).thenAnswer(
                new Answer<List<Tag>>() {

                    @Override
                    public List<Tag> answer(InvocationOnMock invocation)
                            throws Throwable {
                        List<Tag> tags = new ArrayList<>();
                        for (Long long1 : (Long[]) invocation
                                .getArguments()[0]) {
                            Tag tag = new Tag();
                            tag.setId(long1);
                            tags.add(tag);
                        }
                        return tags;
                    }
                });
        List<Tag> actualTags = tagService.getListByIds(ID_LIST);
        assertEquals(ID_LIST.length, actualTags.size());
    }

    @Test(expected = ServiceException.class)
    public void getListByIdsNullInputTest() throws DaoException,
            ServiceException {
        doThrow(new DaoException()).when(tagDao).findListByIds(null);
        tagService.getListByIds(null);
    }

    @Test(expected = ServiceException.class)
    public void getListByIdsFailedTest() throws DaoException, ServiceException {
        doThrow(new DaoException()).when(tagDao).findListByIds(any(
                Long[].class));
        tagService.getListByIds(ID_LIST);
    }
}
