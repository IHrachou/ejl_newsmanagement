package com.epam.javalab.newsmanag.services;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.isNull;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import com.epam.javalab.newsmanag.dao.NewsDao;
import com.epam.javalab.newsmanag.domain.News;
import com.epam.javalab.newsmanag.domain.SearchCriteria;
import com.epam.javalab.newsmanag.exceptions.DaoException;
import com.epam.javalab.newsmanag.exceptions.ServiceException;
import com.epam.javalab.newsmanag.services.impl.NewsServiceImpl;

/**
 * Unit tests for {@link INewsService}.
 *
 * @author Ilya Hrachou
 * @version 1.1.4
 */
@RunWith(MockitoJUnitRunner.class)
public class NewsServiceImplTest {
    @Mock
    private NewsDao newsDao;
    @InjectMocks
    private NewsServiceImpl newsService;

    // Expected values for the test asserts
    private Long expectedNewsId = 4L;
    private long expectedNewsAmount = 5L;
    private long expectedStart = 3L;
    private long expectedEnd = 8L;
    private long illegalPos = -1L;

    // Tests for add(News news)
    @Test
    public void addTest() throws ServiceException, DaoException {
        when(newsDao.create(any(News.class))).thenReturn(expectedNewsId);
        News news = new News();
        Long id = newsService.add(news);
        verify(newsDao).create(news);
        assertEquals(expectedNewsId, id);
    }

    @Test(expected = ServiceException.class)
    public void addNullInputTest() throws ServiceException, DaoException {
        doThrow(new DaoException()).when(newsDao).create(null);
        newsService.add(null);
    }

    @Test(expected = ServiceException.class)
    public void addFailTest() throws DaoException, ServiceException {
        doThrow(new DaoException()).when(newsDao).create(any(News.class));
        News news = new News();
        newsService.add(news);
    }

    // Tests for getByPk(Long newsId)
    @Test
    public void getByPkTest() throws ServiceException, DaoException {
        when(newsDao.findByPk(anyLong())).thenAnswer(new Answer<News>() {
            @Override
            public News answer(InvocationOnMock invocation) throws Throwable {
                News news = new News();
                news.setId((Long) invocation.getArguments()[0]);
                return news;
            }
        });
        News actualNews = newsService.getByPk(expectedNewsId);
        assertNotNull(actualNews);
    }

    @Test(expected = ServiceException.class)
    public void getByPkNullInputTest() throws ServiceException, DaoException {
        doThrow(new DaoException()).when(newsDao).findByPk(null);
        newsService.getByPk(null);
    }

    @Test(expected = ServiceException.class)
    public void getByPkFailTest() throws DaoException, ServiceException {
        doThrow(new DaoException()).when(newsDao).findByPk(anyLong());
        newsService.getByPk(5L);
    }

    // Tests for getAll()
    @SuppressWarnings("serial")
    @Test
    public void getAllTest() throws ServiceException, DaoException {
        when(newsDao.findAll()).thenReturn(new ArrayList<News>() {
            {
                for (int i = 0; i < expectedNewsAmount; i++) {
                    add(new News());
                }
            }
        });
        List<News> newsList = newsService.getAll();
        assertEquals(expectedNewsAmount, newsList.size());
    }

    @Test(expected = ServiceException.class)
    public void getAllFailedTest() throws DaoException, ServiceException {
        doThrow(new DaoException()).when(newsDao).findAll();
        newsService.getAll();
    }

    // Tests for getAllForRange(long startPos, long endPos)
    @Test
    public void getAllForRangeTest() throws ServiceException, DaoException {
        when(newsDao.findForRange(anyLong(), anyLong())).thenAnswer(
                new Answer<List<News>>() {

                    @Override
                    public List<News> answer(InvocationOnMock invocation)
                            throws Throwable {
                        List<News> newsList = new ArrayList<>();
                        long start = (Long) invocation.getArguments()[0];
                        long end = (Long) invocation.getArguments()[1];
                        for (long i = start; i <= end; i++) {
                            News news = new News();
                            news.setId(i);
                            newsList.add(news);
                        }
                        return newsList;
                    }

                });
        List<News> newsList = newsService.getAllForRange(expectedStart,
                expectedEnd);
        assertEquals((expectedEnd - expectedStart) + 1, newsList.size());
    }

    @Test(expected = ServiceException.class)
    public void getAllForRangeIllegalStartPosTest() throws DaoException,
            ServiceException {
        // Negative start position
        newsService.getAllForRange(illegalPos, expectedEnd);
    }

    @Test(expected = ServiceException.class)
    public void getAllForRangeIllegalEndPosTest() throws DaoException,
            ServiceException {
        // Negative end position
        newsService.getAllForRange(expectedStart, illegalPos);
    }

    @Test(expected = ServiceException.class)
    public void getAllForRangeIllegalRangeTest() throws DaoException,
            ServiceException {
        // Start greater that end
        newsService.getAllForRange(expectedEnd, expectedStart);
    }

    @Test(expected = ServiceException.class)
    public void getAllForRangeFailedTest() throws DaoException,
            ServiceException {
        doThrow(new DaoException()).when(newsDao).findForRange(anyLong(),
                anyLong());
        newsService.getAllForRange(expectedStart, expectedEnd);
    }

    // Tests for getAllForRange(long start, long end, SearchCriteria search)
    @Test
    public void getAllForRangeWithCriteriaTest() throws ServiceException,
            DaoException {
        when(newsDao.findForRange(anyLong(), anyLong(), any(
                SearchCriteria.class))).thenAnswer(new Answer<List<News>>() {

                    @Override
                    public List<News> answer(InvocationOnMock invocation)
                            throws Throwable {
                        List<News> newsList = new ArrayList<>();
                        long start = (Long) invocation.getArguments()[0];
                        long end = (Long) invocation.getArguments()[1];
                        for (long i = start; i <= end; i++) {
                            News news = new News();
                            news.setId(i);
                            newsList.add(news);
                        }
                        return newsList;
                    }

                });
        List<News> newsList = newsService.getAllForRange(expectedStart,
                expectedEnd, new SearchCriteria());
        assertEquals((expectedEnd - expectedStart) + 1, newsList.size());
    }

    @Test(expected = ServiceException.class)
    public void getAllForRangeWithCriteriaIllegalStartPosTest()
            throws DaoException, ServiceException {
        // Negative start position
        newsService.getAllForRange(illegalPos, expectedEnd,
                new SearchCriteria());
    }

    @Test(expected = ServiceException.class)
    public void getAllForRangeWithCriteriaIllegalEndPosTest()
            throws DaoException, ServiceException {
        // Negative end position
        newsService.getAllForRange(expectedStart, illegalPos,
                new SearchCriteria());
    }

    @Test(expected = ServiceException.class)
    public void getAllForRangeWithCriteriaIllegalRangeTest()
            throws DaoException, ServiceException {
        // Start greater that end
        newsService.getAllForRange(expectedEnd, expectedStart,
                new SearchCriteria());
    }

    @Test(expected = ServiceException.class)
    public void getAllForRangeWithCriteriaNullCriteriaTest()
            throws DaoException, ServiceException {
        doThrow(new DaoException()).when(newsDao).findForRange(anyLong(),
                anyLong(), isNull(SearchCriteria.class));
        newsService.getAllForRange(expectedStart, expectedEnd, null);
    }

    @Test(expected = ServiceException.class)
    public void getAllForRangeWithCriteriaFailedTest() throws DaoException,
            ServiceException {
        doThrow(new DaoException()).when(newsDao).findForRange(anyLong(),
                anyLong(), any(SearchCriteria.class));
        newsService.getAllForRange(expectedStart, expectedEnd,
                new SearchCriteria());
    }

    // Tests for edit(News news)
    @Test
    public void editTest() throws ServiceException, DaoException {
        doNothing().when(newsDao).update(any(News.class));
        newsService.edit(new News());
        verify(newsDao).update(any(News.class));
    }

    @Test(expected = ServiceException.class)
    public void editNullInputTest() throws DaoException, ServiceException {
        doThrow(new DaoException()).when(newsDao).update(null);
        newsService.edit(null);
    }

    @Test(expected = ServiceException.class)
    public void editFailedTest() throws DaoException, ServiceException {
        doThrow(new DaoException()).when(newsDao).update(any(News.class));
        newsService.edit(new News());
    }

    // Tests for delete(Long newsId)
    @Test
    public void deleteTest() throws ServiceException, DaoException {
        doNothing().when(newsDao).delete(anyLong());
        newsService.delete(expectedNewsId);
        verify(newsDao).delete(anyLong());
    }

    @Test(expected = ServiceException.class)
    public void deleteNullInputTest() throws DaoException, ServiceException {
        doThrow(new DaoException()).when(newsDao).delete(null);
        newsService.delete(null);
    }

    @Test(expected = ServiceException.class)
    public void deleteFailedTest() throws DaoException, ServiceException {
        doThrow(new DaoException()).when(newsDao).delete(anyLong());
        newsService.delete(anyLong());
    }

    // Tests for deleteSeveral(Long... newsIds)
    @Test
    public void deleteSeveralTest() throws ServiceException, DaoException {
        doNothing().when(newsDao).deleteNewsList(any(Long[].class));
        newsService.deleteSeveral(expectedNewsId);
        verify(newsDao).deleteNewsList(any(Long[].class));
    }

    @Test(expected = ServiceException.class)
    public void deleteSeveralNullInputTest() throws DaoException,
            ServiceException {
        doThrow(new DaoException()).when(newsDao).deleteNewsList(null);
        Long[] nulArray = null;
        newsService.deleteSeveral(nulArray);
    }

    @Test(expected = ServiceException.class)
    public void deleteSeveralFailedTest() throws DaoException,
            ServiceException {
        doThrow(new DaoException()).when(newsDao).deleteNewsList(any(
                Long[].class));
        newsService.deleteSeveral(1L, 2L);
    }

    // Tests for searchNews(SearchCriteria searchCriteria)
    @Test
    public void searchNewsTest() throws ServiceException, DaoException {
        List<News> expectedNewsList = new ArrayList<>();
        News news1 = new News();
        News news2 = new News();
        expectedNewsList.add(news1);
        expectedNewsList.add(news2);
        when(newsDao.findByCriteria(any(SearchCriteria.class))).thenReturn(
                expectedNewsList);
        List<News> actualNewsList = newsService.searchNews(
                new SearchCriteria());
        assertEquals(expectedNewsList, actualNewsList);
    }

    @Test(expected = ServiceException.class)
    public void searchNewsNullInputTest() throws ServiceException,
            DaoException {
        doThrow(new DaoException()).when(newsDao).findByCriteria(null);
        newsService.searchNews(null);
    }

    @Test(expected = ServiceException.class)
    public void searchNewsFailedTest() throws ServiceException, DaoException {
        doThrow(new DaoException()).when(newsDao).findByCriteria(any(
                SearchCriteria.class));
        newsService.searchNews(new SearchCriteria());
    }

    // Test for countAllNews()
    @Test
    public void countAllNewsTest() throws ServiceException, DaoException {
        when(newsDao.countNews()).thenReturn(expectedNewsAmount);
        long actualNewsAmount = newsService.countAllNews();
        assertEquals(expectedNewsAmount, actualNewsAmount);
    }

    @Test(expected = ServiceException.class)
    public void countAllNewsFailedTest() throws ServiceException, DaoException {
        doThrow(new DaoException()).when(newsDao).countNews();
        newsService.countAllNews();
    }

    // Test for countFilteredNews()
    @Test
    public void countFilteredNewsTest() throws ServiceException, DaoException {
        when(newsDao.countFilteredNews(any(SearchCriteria.class))).thenReturn(
                expectedNewsAmount);
        long actualNewsAmount = newsService.countFilteredNews(
                new SearchCriteria());
        assertEquals(expectedNewsAmount, actualNewsAmount);
    }

    @Test(expected = ServiceException.class)
    public void countFilteredNewsFailedTest() throws ServiceException,
            DaoException {
        doThrow(new DaoException()).when(newsDao).countFilteredNews(any(
                SearchCriteria.class));
        newsService.countFilteredNews(new SearchCriteria());
    }

    // Tests for addAuthors, deleteAuthorFromNews, deleteSeveralAuthorsFromNews
    // Tests for addTags, deleteTagFromNews, deleteSeveralTagsFromNews

    // Tests for getPreviousNews(Long currentNewsId)
    @Test
    public void getPreviousNewsTest() throws ServiceException, DaoException {
        when(newsDao.findPreviousNews(anyLong())).thenAnswer(
                new Answer<News>() {
                    @Override
                    public News answer(InvocationOnMock invocation)
                            throws Throwable {
                        News news = new News();
                        news.setId((Long) invocation.getArguments()[0] - 1);
                        return news;
                    }
                });
        long previousId = expectedNewsId - 1;
        News actualNews = newsService.getPreviousNews(expectedNewsId);
        assertEquals(previousId, actualNews.getId().longValue());
    }

    @Test(expected = ServiceException.class)
    public void getPreviousNewsNullInputTest() throws ServiceException,
            DaoException {
        doThrow(new DaoException()).when(newsDao).findPreviousNews(null);
        newsService.getPreviousNews(null);
    }

    @Test(expected = ServiceException.class)
    public void getPreviousNewsFailTest() throws DaoException,
            ServiceException {
        doThrow(new DaoException()).when(newsDao).findPreviousNews(anyLong());
        newsService.getPreviousNews(expectedNewsId);
    }

    // Tests for getNextNews(Long currentNewsId)
    @Test
    public void getNextNewsTest() throws ServiceException, DaoException {
        when(newsDao.findNextNews(anyLong())).thenAnswer(new Answer<News>() {
            @Override
            public News answer(InvocationOnMock invocation) throws Throwable {
                News news = new News();
                news.setId((Long) invocation.getArguments()[0] + 1);
                return news;
            }
        });
        long nextId = expectedNewsId + 1;
        News actualNews = newsService.getNextNews(expectedNewsId);
        assertEquals(nextId, actualNews.getId().longValue());
    }

    @Test(expected = ServiceException.class)
    public void getNextNewsNullInputTest() throws ServiceException,
            DaoException {
        doThrow(new DaoException()).when(newsDao).findNextNews(null);
        newsService.getNextNews(null);
    }

    @Test(expected = ServiceException.class)
    public void getNextNewsFailTest() throws DaoException, ServiceException {
        doThrow(new DaoException()).when(newsDao).findNextNews(anyLong());
        newsService.getNextNews(expectedNewsId);
    }

    // Tests for getNextAndPrevNewsId(Long currentNewsId, SearchCriteria
    // searchCriteria)
    @Test
    public void getNextAndPrevNewsIdTest() throws ServiceException,
            DaoException {
        when(newsDao.findPrevAndNextNewsId(anyLong(), any(
                SearchCriteria.class))).thenAnswer(new Answer<Long[]>() {
                    @Override
                    public Long[] answer(InvocationOnMock invocation)
                            throws Throwable {
                        Long[] ids = new Long[2];
                        ids[0] = (Long) invocation.getArguments()[0] - 1;
                        ids[1] = (Long) invocation.getArguments()[0] + 1;
                        return ids;
                    }
                });
        Long[] expectedIds = { expectedNewsId - 1, expectedNewsId + 1 };
        Long[] actualIds = newsService.getNextAndPrevNewsId(expectedNewsId,
                new SearchCriteria());
        assertArrayEquals(expectedIds, actualIds);
    }

    @Test(expected = ServiceException.class)
    public void getNextAndPrevNewsIdNullNewsIdTest() throws ServiceException,
            DaoException {
        doThrow(new DaoException()).when(newsDao).findPrevAndNextNewsId(
                anyLong(), any(SearchCriteria.class));
        newsService.getNextAndPrevNewsId(null, new SearchCriteria());
    }

    @Test(expected = ServiceException.class)
    public void getNextAndPrevNewsIdFailTest() throws DaoException,
            ServiceException {
        doThrow(new DaoException()).when(newsDao).findPrevAndNextNewsId(
                anyLong(), any(SearchCriteria.class));
        newsService.getNextAndPrevNewsId(expectedNewsId, new SearchCriteria());
    }
}
