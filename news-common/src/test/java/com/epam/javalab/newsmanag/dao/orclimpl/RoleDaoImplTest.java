package com.epam.javalab.newsmanag.dao.orclimpl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.fail;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;

import com.epam.javalab.newsmanag.dao.RoleDao;
import com.epam.javalab.newsmanag.domain.Role;
import com.epam.javalab.newsmanag.exceptions.DaoException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;

/**
 * Unit tests for {@link RoleDao}.
 *
 * @author Ilya Hrachou
 * @version 1.0
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:test-spring-config.xml")
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        DbUnitTestExecutionListener.class })
@DatabaseSetup(value = "classpath:role-data.xml")
@DatabaseTearDown(value = "classpath:role-data.xml",
        type = DatabaseOperation.DELETE_ALL)
public class RoleDaoImplTest {
    @Autowired
    private RoleDao roleDao;

    // Constants for the test
    private final Long incorrectId = -1L;
    private final Long expectedRoleId = 3L;
    private final String expectedRoleName = "тест 3";
    private final int expectedRolesAmount = 3;
    private final int expectedRolesAmountAfterDelete = 2;

    @Test
    public void createTest() throws DaoException {
        Role expectedRole = new Role();
        expectedRole.setName(expectedRoleName);
        Long id = roleDao.create(expectedRole);
        assertNotEquals(incorrectId, id);
        assertEquals(expectedRole, roleDao.findByPk(id));
    }

    @Test
    public void createNullInputTest() throws DaoException {
        try {
            roleDao.create(null);
        } catch (Exception e) {
            assertEquals("Unable to create null Role in the database", e
                    .getMessage());
            return;
        }
        fail("Expected validation exception was not thrown");
    }

    @Test
    public void findByPkTest() throws DaoException {
        Role roleExpected = new Role();
        roleExpected.setId(expectedRoleId);
        roleExpected.setName(expectedRoleName);
        Role roleActual = roleDao.findByPk(expectedRoleId);
        assertEquals(roleExpected, roleActual);
    }

    @Test
    public void findByPkNullInputTest() throws DaoException {
        try {
            roleDao.findByPk(null);
        } catch (Exception e) {
            assertEquals("Primary key cannot be null", e.getMessage());
            return;
        }
        fail("Expected validation exception was not thrown");
    }

    @Test
    public void findByNameTest() throws DaoException {
        Role roleExpected = new Role();
        roleExpected.setId(expectedRoleId);
        roleExpected.setName(expectedRoleName);
        Role roleActual = roleDao.findByName(expectedRoleName).get(0);
        assertEquals(roleExpected, roleActual);
    }

    @Test
    public void findByNameNullInputTest() throws DaoException {
        try {
            roleDao.findByName(null);
        } catch (Exception e) {
            assertEquals("Role name cannot be null", e.getMessage());
            return;
        }
        fail("Expected validation exception was not thrown");
    }

    @Test
    public void findAllTest() throws DaoException {
        List<Role> roles = roleDao.findAll();
        assertEquals(expectedRolesAmount, roles.size());
    }

    @Test
    public void updateTest() throws DaoException {
        Role expectedRole = new Role();
        expectedRole.setId(expectedRoleId);
        expectedRole.setName(expectedRoleName);
        roleDao.update(expectedRole);
        Role actualRole = roleDao.findByPk(expectedRoleId);
        assertEquals(expectedRole, actualRole);
    }

    @Test
    public void updateNullInputTest() throws DaoException {
        try {
            roleDao.update(null);
        } catch (Exception e) {
            assertEquals("Null instance cannot be recorded to the database", e
                    .getMessage());
            return;
        }
        fail("Expected validation exception was not thrown");
    }

    @Test(expected = DaoException.class)
    public void deleteTest() throws DaoException {
        roleDao.delete(expectedRoleId);
        assertEquals(expectedRolesAmountAfterDelete, roleDao.findAll().size());
        roleDao.findByPk(expectedRoleId);
    }

    @Test
    public void deleteNullInputTest() throws DaoException {
        try {
            roleDao.delete(null);
        } catch (Exception e) {
            assertEquals("Primary key cannot be null", e.getMessage());
            return;
        }
        fail("Expected validation exception was not thrown");
    }
}
