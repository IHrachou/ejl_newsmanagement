package com.epam.javalab.newsmanag.dao.orclimpl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;

import com.epam.javalab.newsmanag.dao.AuthorDao;
import com.epam.javalab.newsmanag.domain.Author;
import com.epam.javalab.newsmanag.exceptions.DaoException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;

/**
 * Unit tests for {@link AuthorDao}.
 *
 * @author Ilya Hrachou
 * @version 1.1.5
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:test-spring-config.xml")
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        DbUnitTestExecutionListener.class })
@DatabaseSetup(value = "classpath:author-data.xml")
@DatabaseTearDown(value = "classpath:author-data.xml",
        type = DatabaseOperation.DELETE_ALL)
public class AuthorDaoImplTest {
    @Autowired
    private AuthorDao authorDao;

    // Constants for the test
    private final Long incorrectId = -1L;
    private final Long expectedAuthorId = 3L;
    private final Long expectedNewsId = 1L;
    private final String expectedName = "автор 3";
    private final int expectedAuthorsAmount = 3;
    private final int expectedNonExpiredAmount = 2;
    private final int expectedAuthorsAmountForNews = 2;
    private final Long[] expectedIds = { 2L, 3L };

    @Test
    public void createTest() throws DaoException {
        Author expectedAuthor = new Author();
        expectedAuthor.setName(expectedName);
        Long id = authorDao.create(expectedAuthor);
        assertNotEquals(incorrectId, id);
        assertEquals(expectedAuthor, authorDao.findByPk(id));
    }

    @Test
    public void createNullInputTest() throws DaoException {
        try {
            authorDao.create(null);
        } catch (Exception e) {
            assertEquals("Unable to create null Author in the database", e
                    .getMessage());
            return;
        }
        fail("Expected validation exception was not thrown");
    }

    @Test
    public void findByPkTest() throws DaoException {
        Author authorExpected = new Author();
        authorExpected.setId(expectedAuthorId);
        authorExpected.setName(expectedName);
        Author authorActual = authorDao.findByPk(expectedAuthorId);
        assertEquals(authorExpected, authorActual);
    }

    @Test
    public void findByPkNullInputTest() throws DaoException {
        try {
            authorDao.findByPk(null);
        } catch (Exception e) {
            assertEquals("Primary key cannot be null", e.getMessage());
            return;
        }
        fail("Expected validation exception was not thrown");
    }

    @Test
    public void findByNameTest() throws DaoException {
        Author authorExpected = new Author();
        authorExpected.setId(expectedAuthorId);
        authorExpected.setName(expectedName);
        Author authorActual = authorDao.findByName(expectedName);
        assertEquals(authorExpected, authorActual);
    }

    @Test
    public void findByNameNullInputTest() throws DaoException {
        try {
            authorDao.findByName(null);
        } catch (Exception e) {
            assertEquals("Author name cannot be null", e.getMessage());
            return;
        }
        fail("Expected validation exception was not thrown");
    }

    @Test
    public void findAllTest() throws DaoException {
        List<Author> authors = authorDao.findAll();
        assertEquals(expectedAuthorsAmount, authors.size());
    }

    @Test
    public void findNonExpiredTest() throws DaoException {
        List<Author> authors = authorDao.findNonExpired();
        assertEquals(expectedNonExpiredAmount, authors.size());
    }

    @Test
    public void updateTest() throws DaoException {
        Author expectedAuthor = new Author();
        expectedAuthor.setId(expectedAuthorId);
        expectedAuthor.setName(expectedName);
        authorDao.update(expectedAuthor);
        Author actualAuthor = authorDao.findByPk(expectedAuthorId);
        assertEquals(expectedAuthor, actualAuthor);
    }

    @Test
    public void updateNullInputTest() throws DaoException {
        try {
            authorDao.update(null);
        } catch (Exception e) {
            assertEquals("Null instance cannot be recorded to the database", e
                    .getMessage());
            return;
        }
        fail("Expected validation exception was not thrown");
    }

    @Test(expected = DaoException.class)
    public void deleteTest() throws DaoException {
        authorDao.delete(expectedAuthorId);
    }

    @Test
    public void deleteTestExceptionMessage() throws DaoException {
        try {
            authorDao.delete(expectedAuthorId);
        } catch (Exception e) {
            assertEquals("Unsupported operation. Author cannot be deleted", e
                    .getMessage());
            return;
        }
        fail("Expected validation exception was not thrown");
    }

    @Test
    public void expireTest() throws DaoException {
        authorDao.expire(expectedAuthorId);
        Author authorActual = authorDao.findByPk(expectedAuthorId);
        assertNotNull(authorActual.getExpired());
    }

    @Test
    public void expireNullInputTest() throws DaoException {
        try {
            authorDao.expire(null);
        } catch (Exception e) {
            assertEquals("Primary key cannot be null", e.getMessage());
            return;
        }
        fail("Expected validation exception was not thrown");
    }

    @Test
    public void findByNewsIdTest() throws DaoException {
        List<Author> authors = authorDao.findByNewsId(expectedNewsId);
        assertEquals(expectedAuthorsAmountForNews, authors.size());
    }

    @Test
    public void findByNewsIdNullInputTest() throws DaoException {
        try {
            authorDao.findByNewsId(null);
        } catch (Exception e) {
            assertEquals("Primary key cannot be null", e.getMessage());
            return;
        }
        fail("Expected validation exception was not thrown");
    }

    @Test
    public void findListByIdsTest() throws DaoException {
        List<Author> actualAuthors = authorDao.findListByIds(expectedIds);
        List<Author> expectedAuthors = new ArrayList<>();
        for (Long id : expectedIds) {
            expectedAuthors.add(authorDao.findByPk(id));
        }
        assertEquals(expectedAuthors, actualAuthors);
    }

    @Test
    public void findListByIdsNullInputTest() throws DaoException {
        try {
            authorDao.findListByIds(null);
        } catch (Exception e) {
            assertEquals("Input array cannot be null", e.getMessage());
            return;
        }
        fail("Expected validation exception was not thrown");
    }
}
