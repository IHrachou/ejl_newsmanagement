package com.epam.javalab.newsmanag.services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import com.epam.javalab.newsmanag.dao.UserDao;
import com.epam.javalab.newsmanag.domain.User;
import com.epam.javalab.newsmanag.exceptions.DaoException;
import com.epam.javalab.newsmanag.exceptions.ServiceException;
import com.epam.javalab.newsmanag.services.impl.UserServiceImpl;

/**
 * Unit tests for {@link IUserService}.
 *
 * @author Ilya Hrachou
 * @version 1.1.7
 */
@RunWith(MockitoJUnitRunner.class)
public class UserServiceImplTest {
    @Mock
    private UserDao userDao;
    @InjectMocks
    private UserServiceImpl userService;

    // Expected values for the test asserts
    private static final Long expectedUserId = 4L;
    private static final long expectedUsersAmount = 5;
    private static final String expectedUserLogin = "пользователь 1";

    @Test
    public void addTest() throws ServiceException, DaoException {
        when(userDao.create(any(User.class))).thenReturn(expectedUserId);
        User user = new User();
        Long id = userService.add(user);
        verify(userDao).create(user);
        assertEquals(expectedUserId, id);
    }

    @Test(expected = ServiceException.class)
    public void addNullInputTest() throws ServiceException, DaoException {
        doThrow(new DaoException()).when(userDao).create(null);
        userService.add(null);
    }

    @Test(expected = ServiceException.class)
    public void addFailTest() throws DaoException, ServiceException {
        doThrow(new DaoException()).when(userDao).create(any(User.class));
        User user = new User();
        userService.add(user);
    }

    @Test
    public void getByPkTest() throws ServiceException, DaoException {
        when(userDao.findByPk(anyLong())).thenAnswer(new Answer<User>() {
            @Override
            public User answer(InvocationOnMock invocation) throws Throwable {
                User user = new User();
                user.setId((Long) invocation.getArguments()[0]);
                return user;
            }
        });
        User actualUser = userService.getByPk(expectedUserId);
        assertNotNull(actualUser);
    }

    @Test(expected = ServiceException.class)
    public void getByPkNullInputTest() throws ServiceException, DaoException {
        doThrow(new DaoException()).when(userDao).findByPk(null);
        userService.getByPk(null);
    }

    @Test(expected = ServiceException.class)
    public void getByPkFailTest() throws DaoException, ServiceException {
        doThrow(new DaoException()).when(userDao).findByPk(anyLong());
        userService.getByPk(expectedUserId);
    }

    @Test
    public void getByLoginTest() throws ServiceException, DaoException {
        when(userDao.findByLogin(anyString())).thenAnswer(new Answer<User>() {
            @Override
            public User answer(InvocationOnMock invocation) throws Throwable {
                User user = new User();
                user.setLogin((String) invocation.getArguments()[0]);
                return user;
            }
        });
        User actualUser = userService.getByLogin(expectedUserLogin);
        assertNotNull(actualUser);
    }

    @Test(expected = ServiceException.class)
    public void getByLoginNullInputTest() throws ServiceException,
            DaoException {
        doThrow(new DaoException()).when(userDao).findByLogin(null);
        userService.getByLogin(null);
    }

    @Test(expected = ServiceException.class)
    public void getByLoginFailTest() throws DaoException, ServiceException {
        doThrow(new DaoException()).when(userDao).findByLogin(anyString());
        userService.getByLogin(expectedUserLogin);
    }

    @SuppressWarnings("serial")
    @Test
    public void getAllTest() throws ServiceException, DaoException {
        when(userDao.findAll()).thenReturn(new ArrayList<User>() {
            {
                for (int i = 0; i < expectedUsersAmount; i++) {
                    add(new User());
                }
            }
        });
        List<User> users = userService.getAll();
        assertEquals(expectedUsersAmount, users.size());
    }

    @Test(expected = ServiceException.class)
    public void getAllFailedTest() throws DaoException, ServiceException {
        doThrow(new DaoException()).when(userDao).findAll();
        userService.getAll();
    }

    @Test
    public void editTest() throws ServiceException, DaoException {
        doNothing().when(userDao).update(any(User.class));
        userService.edit(new User());
        verify(userDao).update(any(User.class));
    }

    @Test(expected = ServiceException.class)
    public void editNullInputTest() throws DaoException, ServiceException {
        doThrow(new DaoException()).when(userDao).update(null);
        userService.edit(null);
    }

    @Test(expected = ServiceException.class)
    public void editFailedTest() throws DaoException, ServiceException {
        doThrow(new DaoException()).when(userDao).update(any(User.class));
        userService.edit(new User());
    }

    @Test
    public void deleteTest() throws ServiceException, DaoException {
        doNothing().when(userDao).delete(anyLong());
        userService.delete(expectedUserId);
        verify(userDao).delete(anyLong());
    }

    @Test(expected = ServiceException.class)
    public void deleteNullInputTest() throws DaoException, ServiceException {
        doThrow(new DaoException()).when(userDao).delete(null);
        userService.delete(null);
    }

    @Test(expected = ServiceException.class)
    public void deleteFailedTest() throws DaoException, ServiceException {
        doThrow(new DaoException()).when(userDao).delete(anyLong());
        userService.delete(anyLong());
    }
}
