package com.epam.javalab.newsmanag.services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import com.epam.javalab.newsmanag.domain.Author;
import com.epam.javalab.newsmanag.domain.Comment;
import com.epam.javalab.newsmanag.domain.News;
import com.epam.javalab.newsmanag.domain.SearchCriteria;
import com.epam.javalab.newsmanag.domain.Tag;
import com.epam.javalab.newsmanag.domain.dto.NewsDto;
import com.epam.javalab.newsmanag.exceptions.DaoException;
import com.epam.javalab.newsmanag.exceptions.ServiceException;
import com.epam.javalab.newsmanag.services.impl.NewsHandlingService;

/**
 * Unit tests for {@link NewsHandlingService}.
 *
 * @author Ilya Hrachou
 * @version 1.1.6
 */
@RunWith(MockitoJUnitRunner.class)
public class NewsHandlingServiceImplTest {
    @Mock
    private INewsService newsService;
    @Mock
    private ITagService tagService;
    @Mock
    private IAuthorService authorService;
    @Mock
    private ICommentService commentService;
    @InjectMocks
    private NewsHandlingService newsHandlingService;

    // Expected fields values
    private Long expectedNewsId = 3L;
    private Long expectedAuthor1Id = 5L;
    private Long expectedAuthor2Id = 7L;
    private Long expectedTag1Id = 9L;
    private Long expectedTag2Id = 11L;
    private long expectedStartPos = 3L;
    private long expectedEndPos = 5L;

    // Tests for addNews(NewsDto newsDto)
    @Test
    public void addNewsTest() throws ServiceException, DaoException {
        when(newsService.add(any(News.class))).thenReturn(expectedNewsId);
        NewsDto newsDto = new NewsDto();
        newsDto.setNews(new News());
        List<Author> authors = createTestAuthorsList();
        List<Tag> tags = createTestTagsList();
        newsDto.setAuthors(authors);
        newsDto.setTags(tags);
        Long actualNewsId = newsHandlingService.addNews(newsDto);
        assertEquals(expectedNewsId, actualNewsId);
        verify(newsService).addAuthors(expectedNewsId, newsDto.getAuthorsIds());
        verify(newsService).addTags(expectedNewsId, newsDto.getTagsIds());
    }

    @Test
    public void addNewsNullInputTest() throws ServiceException, DaoException {
        try {
            newsHandlingService.addNews(null);
        } catch (Exception e) {
            assertEquals("News DTO object is null", e.getMessage());
            return;
        }
        fail("Expected validation exception was not thrown");
    }

    @Test
    public void addNewsNullAuthorTest() throws ServiceException, DaoException {
        NewsDto newsDto = new NewsDto();
        newsDto.setNews(new News());
        newsDto.setTags(new ArrayList<Tag>());
        try {
            newsHandlingService.addNews(newsDto);
        } catch (Exception e) {
            assertEquals("News DTO object doesn't contain authors", e
                    .getMessage());
            return;
        }
        fail("Expected validation exception was not thrown");
    }

    @Test(expected = ServiceException.class)
    public void addNewsFailTest() throws DaoException, ServiceException {
        doThrow(new ServiceException()).when(newsService).add(any(News.class));
        newsHandlingService.addNews(new NewsDto());
    }

    // Tests for deleteNews(NewsDto newsDto)
    @Test
    public void deleteNewsTest() throws ServiceException {
        List<Author> authors = createTestAuthorsList();
        when(authorService.getByNewsId(anyLong())).thenReturn(authors);
        List<Tag> tags = createTestTagsList();
        when(tagService.getByNewsId(anyLong())).thenReturn(tags);
        List<Comment> comments = createTestCommentsList();
        when(commentService.getByNewsId(anyLong())).thenReturn(comments);
        NewsDto newsDto = createTestNewsDto();
        newsHandlingService.deleteNews(newsDto);
        verify(newsService, times(2)).deleteAuthorFromNews(anyLong(),
                anyLong());
        verify(newsService, times(2)).deleteTagFromNews(anyLong(), anyLong());
        verify(commentService, times(2)).delete(anyLong());
        verify(newsService).delete(anyLong());
    }

    @Test
    public void deleteNewsNullInputTest() throws ServiceException,
            DaoException {
        try {
            newsHandlingService.deleteNews(null);
        } catch (Exception e) {
            assertEquals("News DTO object is null", e.getMessage());
            return;
        }
        fail("Expected validation exception was not thrown");
    }

    @Test(expected = ServiceException.class)
    public void deleteNewsFailTest() throws ServiceException {
        doThrow(new ServiceException()).when(newsService).delete(anyLong());
        newsHandlingService.deleteNews(createTestNewsDto());
    }

    // Tests for showNews(Long newsId)
    @Test
    public void showNewsTest() throws ServiceException {
        when(newsService.getByPk(anyLong())).thenReturn(new News());
        when(authorService.getByNewsId(anyLong())).thenReturn(
                createTestAuthorsList());
        when(tagService.getByNewsId(anyLong())).thenReturn(
                createTestTagsList());
        when(commentService.getByNewsId(anyLong())).thenReturn(
                createTestCommentsList());
        NewsDto newsDto = newsHandlingService.showNews(expectedNewsId);
        assertEquals(createTestNewsDto(), newsDto);
    }

    @Test
    public void showNewsNullNewsIdTest() throws ServiceException {
        try {
            newsHandlingService.showNews(null);
        } catch (Exception e) {
            assertEquals("Illegal news id", e.getMessage());
            return;
        }
        fail("Expected validation exception was not thrown");
    }

    // Tests for showNewsListForRange(long startPos, long endPos)
    @Test
    public void showNewsListForRangeTest() throws ServiceException {
        when(newsService.getAllForRange(anyLong(), anyLong())).thenAnswer(
                new Answer<List<News>>() {

                    @Override
                    public List<News> answer(InvocationOnMock invocation)
                            throws Throwable {
                        long start = (long) invocation.getArguments()[0];
                        long end = (long) invocation.getArguments()[1];
                        List<News> newsList = new ArrayList<>();
                        for (long i = start; i <= end; i++) {
                            newsList.add(new News());
                        }
                        return newsList;
                    }
                });
        newsHandlingService.showNewsListForRange(expectedStartPos,
                expectedEndPos);
        verify(newsService).getAllForRange(expectedStartPos, expectedEndPos);
        int expectedNewsAmount = (int) ((expectedEndPos - expectedStartPos)
                + 1);
        verify(authorService, times(expectedNewsAmount)).getByNewsId(anyLong());
        verify(commentService, times(expectedNewsAmount)).getByNewsId(
                anyLong());
        verify(tagService, times(expectedNewsAmount)).getByNewsId(anyLong());
    }

    @Test
    public void showNewsListForRangeWithCriteriaTest() throws ServiceException {
        when(newsService.getAllForRange(anyLong(), anyLong(), any(
                SearchCriteria.class))).thenAnswer(new Answer<List<News>>() {

                    @Override
                    public List<News> answer(InvocationOnMock invocation)
                            throws Throwable {
                        long start = (long) invocation.getArguments()[0];
                        long end = (long) invocation.getArguments()[1];
                        List<News> newsList = new ArrayList<>();
                        for (long i = start; i <= end; i++) {
                            newsList.add(new News());
                        }
                        return newsList;
                    }
                });
        SearchCriteria searchCriteria = new SearchCriteria();
        newsHandlingService.showNewsListForRange(expectedStartPos,
                expectedEndPos, searchCriteria);
        verify(newsService).getAllForRange(expectedStartPos, expectedEndPos,
                searchCriteria);
        int expectedNewsAmount = (int) ((expectedEndPos - expectedStartPos)
                + 1);
        verify(authorService, times(expectedNewsAmount)).getByNewsId(anyLong());
        verify(commentService, times(expectedNewsAmount)).getByNewsId(
                anyLong());
        verify(tagService, times(expectedNewsAmount)).getByNewsId(anyLong());
    }

    private List<Author> createTestAuthorsList() {
        List<Author> authors = new ArrayList<Author>();
        Author author = new Author();
        author.setId(expectedAuthor1Id);
        authors.add(author);
        author = new Author();
        author.setId(expectedAuthor2Id);
        authors.add(author);
        return authors;
    }

    private List<Tag> createTestTagsList() {
        List<Tag> tags = new ArrayList<Tag>();
        Tag tag = new Tag();
        tag.setId(expectedTag1Id);
        tags.add(tag);
        tag = new Tag();
        tag.setId(expectedTag2Id);
        tags.add(tag);
        return tags;
    }

    private List<Comment> createTestCommentsList() {
        List<Comment> comments = new ArrayList<>();
        comments.add(new Comment());
        comments.add(new Comment());
        return comments;
    }

    private NewsDto createTestNewsDto() {
        NewsDto newsDto = new NewsDto();
        newsDto.setNews(new News());
        newsDto.setAuthors(createTestAuthorsList());
        newsDto.setTags(createTestTagsList());
        newsDto.setComments(createTestCommentsList());
        return newsDto;
    }

    // Tests for showPreviousNews(Long currentNewsId)
    @Test
    public void showPreviousNewsTest() throws ServiceException {
        when(newsService.getPreviousNews(anyLong())).thenReturn(new News());
        when(authorService.getByNewsId(anyLong())).thenReturn(
                createTestAuthorsList());
        when(tagService.getByNewsId(anyLong())).thenReturn(
                createTestTagsList());
        when(commentService.getByNewsId(anyLong())).thenReturn(
                createTestCommentsList());
        NewsDto newsDto = newsHandlingService.showPreviousNews(expectedNewsId);
        assertEquals(createTestNewsDto(), newsDto);
    }

    @Test
    public void showPreviousNewsNullNewsIdTest() throws ServiceException {
        try {
            newsHandlingService.showPreviousNews(null);
        } catch (Exception e) {
            assertEquals("Illegal news id", e.getMessage());
            return;
        }
        fail("Expected validation exception was not thrown");
    }

    // Tests for showPreviousNews(Long currentNewsId)
    @Test
    public void showNextNewsTest() throws ServiceException {
        when(newsService.getNextNews(anyLong())).thenReturn(new News());
        when(authorService.getByNewsId(anyLong())).thenReturn(
                createTestAuthorsList());
        when(tagService.getByNewsId(anyLong())).thenReturn(
                createTestTagsList());
        when(commentService.getByNewsId(anyLong())).thenReturn(
                createTestCommentsList());
        NewsDto newsDto = newsHandlingService.showNextNews(expectedNewsId);
        assertEquals(createTestNewsDto(), newsDto);
    }

    @Test
    public void showNextNewsNullNewsIdTest() throws ServiceException {
        try {
            newsHandlingService.showNextNews(null);
        } catch (Exception e) {
            assertEquals("Illegal news id", e.getMessage());
            return;
        }
        fail("Expected validation exception was not thrown");
    }
}
