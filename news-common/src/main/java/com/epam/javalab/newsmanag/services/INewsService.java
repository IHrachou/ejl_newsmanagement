package com.epam.javalab.newsmanag.services;

import java.util.List;

import com.epam.javalab.newsmanag.domain.News;
import com.epam.javalab.newsmanag.domain.SearchCriteria;
import com.epam.javalab.newsmanag.exceptions.ServiceException;

/**
 * This interface is used in service layer. It extends {@code IGenericService}
 * interface and specifies additional methods for handling {@code News} domain.
 * It is used for mapping DAO logic to the service layer.
 *
 * @author Ilya Hrachou
 * @version 1.1.6
 */
public interface INewsService extends IGenericService<News, Long> {
    /**
     * Return the list of news for the specified range. News are sorted by the
     * amount of comments and the modification date in the descending order.
     * Range must be checked. <code>startPos</code> and <code>endPos</code> must
     * not be negative. <code>endPos</code> must not be lesser than
     * <code>startPos</code>.
     *
     * @param startPos
     *            the number of the first news inclusive; must not be negative
     * @param endPos
     *            the number of the last news inclusive; must not be lesser than
     *            <code>startPos</code>
     * @return the list of news for the specified range
     * @throws ServiceException
     *             when some errors occurs in the DAO layer; when input range is
     *             illegal
     * @since 1.0.2
     */
    List<News> getAllForRange(long startPos, long endPos)
            throws ServiceException;

    /**
     * Return the list of news for the specified range according to the
     * SearchCriteria. News are sorted by the amount of comments and the
     * modification date in the descending order. Range must be checked.
     * <code>startPos</code> and <code>endPos</code> must not be negative.
     * <code>endPos</code> must not be lesser than <code>startPos</code>.
     *
     * @param startPos
     *            the number of the first news inclusive; must not be negative
     * @param endPos
     *            the number of the last news inclusive; must not be lesser than
     *            <code>startPos</code>
     * @param searchCriteria
     *            the set of criteria to find news
     * @return the list of news for the specified range
     * @throws ServiceException
     *             when some errors occurs in the DAO layer; when input range is
     *             illegal
     * @since 1.0.5
     */
    List<News> getAllForRange(long startPos, long endPos,
            SearchCriteria searchCriteria) throws ServiceException;

    /**
     * Call the DAO object to get the list of news for the specified search
     * criteria.
     *
     * @param searchCriteria
     *            the set of criteria to find news
     * @return the list of news for the specified search criteria
     * @throws ServiceException
     *             when some errors occurs in the DAO layer or the
     *             {@code searchCriteria} is {@code null}
     */
    List<News> searchNews(SearchCriteria searchCriteria)
            throws ServiceException;

    /**
     * Call the DAO object to get the amount of news in the database.
     *
     * @return the amount of news in the database
     * @throws ServiceException
     *             when some errors occurs in the DAO layer
     */
    Long countAllNews() throws ServiceException;

    /**
     * Call the DAO object to get the amount of the filtered news in the
     * database.
     *
     * @param searchCriteria
     *            the set of criteria to find news
     * @return the amount of the filtered news in the database
     * @throws ServiceException
     *             when some errors occurs in the DAO layer
     */
    Long countFilteredNews(SearchCriteria searchCriteria)
            throws ServiceException;

    /**
     * Call the DAO object to add authors to the specified news. It is
     * impossible to add the nonexistent author or to add author to the
     * nonexistent news.
     *
     * @param newsId
     *            the news ID to which author should be added
     * @param authorIds
     *            the list of authors IDs which must be added to the news
     * @throws ServiceException
     *             when some errors occurs in the DAO layer or input parameters
     *             are {@code null}
     */
    void addAuthors(Long newsId, List<Long> authorIds) throws ServiceException;

    /**
     * Call the DAO object to delete author from the specified news. This method
     * must delete record from the table many-to-many for the news and authors
     * connection.
     *
     * @param newsId
     *            the news ID from which author should be delete
     * @param authorId
     *            the ID of the author which must be delete from the news
     * @throws ServiceException
     *             when some errors occurs in the DAO layer or input parameters
     *             are {@code null}
     */
    void deleteAuthorFromNews(Long newsId, Long authorId)
            throws ServiceException;

    /**
     * Call the DAO object to delete several authors from the specified news.
     * This method must delete records from the table many-to-many for the news
     * and authors connection.
     *
     * @param newsId
     *            the news ID from which author should be deleted
     * @param authorsIds
     *            the list of authors IDs which must be deleted from the news
     * @throws ServiceException
     *             when some errors occurs in the DAO layer or input parameters
     *             are {@code null}
     * @since 1.1.4
     */
    void deleteSeveralAuthorsFromNews(Long newsId, List<Long> authorsIds)
            throws ServiceException;

    /**
     * Call the DAO object to add tags to the specified news. It is impossible
     * to add the nonexistent tag or to add tag to the nonexistent news.
     *
     * @param newsId
     *            the news ID to which author should be added
     * @param tagsIds
     *            the list of tags IDs which must be added to the news
     * @throws ServiceException
     *             when some errors occurs in the DAO layer or input parameters
     *             are {@code null}
     */
    void addTags(Long newsId, List<Long> tagsIds) throws ServiceException;

    /**
     * Call the DAO object to delete tag from the specified news. This method
     * must delete record from the table many-to-many for the news and tags
     * connection.
     *
     * @param newsId
     *            the news ID from which tag should be deleted
     * @param tagId
     *            the ID of the tag which must be deleted from the news
     * @throws ServiceException
     *             when some errors occurs in the DAO layer or input parameters
     *             are {@code null}
     */
    void deleteTagFromNews(Long newsId, Long tagId) throws ServiceException;

    /**
     * Call the DAO object to delete several tags from the specified news. This
     * method must delete records from the table many-to-many for the news and
     * tags connection.
     *
     * @param newsId
     *            the news ID from which tag should be deleted
     * @param tagsIds
     *            the list of tags IDs which must be deleted from the news
     * @throws ServiceException
     *             when some errors occurs in the DAO layer or input parameters
     *             are {@code null}
     * @since 1.1.4
     */
    void deleteSeveralTagsFromNews(Long newsId, List<Long> tagsIds)
            throws ServiceException;

    /**
     * Call the DAO object to delete all tags from the specified news. This
     * method must delete records from the table many-to-many for the news and
     * tags connection.
     *
     * @param newsId
     *            the news ID from which tag should be deleted
     * @throws ServiceException
     *             when some errors occurs in the DAO layer or input parameters
     *             are {@code null}
     * @since 1.1.6
     */
    void deleteAllTagsFromNews(Long newsId) throws ServiceException;

    /**
     * Return the previous news for the specified current news ID from the
     * filtered list of news.
     *
     * @param currentNewsId
     *            the ID of the current news
     * @return the previous news for the specified current news ID
     * @throws ServiceException
     *             when some errors occurs in the DAO layer or input parameter
     *             is {@code null}
     * @since 1.0.3
     */
    News getPreviousNews(Long currentNewsId) throws ServiceException;

    /**
     * Return the previous news for the specified current news ID from the
     * filtered list of news. The list of news is filtered by search criteria.
     *
     * @param currentNewsId
     *            the ID of the current news
     * @param searchCriteria
     *            the set of criteria to find news
     * @return the previous news for the specified current news ID from the
     *         filtered list of news
     * @throws ServiceException
     *             when some errors occurs in the DAO layer or input parameter
     *             is {@code null}
     * @since 1.0.6
     */
    News getPreviousNews(Long currentNewsId, SearchCriteria searchCriteria)
            throws ServiceException;

    /**
     * Return the next news for the specified current news ID from the filtered
     * list of news.
     *
     * @param currentNewsId
     *            the ID of the current news
     * @return the next news for the specified current news ID
     * @throws ServiceException
     *             when some errors occurs in the DAO layer or input parameter
     *             is {@code null}
     * @since 1.0.3
     */
    News getNextNews(Long currentNewsId) throws ServiceException;

    /**
     * Return the next news for the specified current news ID from the filtered
     * list of news. The list of news is filtered by search criteria.
     *
     * @param currentNewsId
     *            the ID of the current news
     * @param searchCriteria
     *            the set of criteria to find news
     * @return the next news for the specified current news ID from the filtered
     *         list of news
     * @throws ServiceException
     *             when some errors occurs in the DAO layer or input parameter
     *             is {@code null}
     * @since 1.0.6
     */
    News getNextNews(Long currentNewsId, SearchCriteria searchCriteria)
            throws ServiceException;

    /**
     * Return the array of the two Long. The first is the ID of the previous
     * news and the second is the ID of the next news. The previous/next news ID
     * is searched in the filtered list of news. News list is filtered by search
     * criteria. If search criteria is null or empty this method will return the
     * IDs of the previous and next news from the non filtered list of news.
     *
     * @param currentNewsId
     *            the ID of the current news
     * @param searchCriteria
     *            the criteria for the news filtering
     * @return the array of the two IDs where the first value is previous news
     *         ID and the last value is the next news ID
     * @throws ServiceException
     *             when some errors occurs during the connection to the database
     *             or executing the query; this exception is also thrown when
     *             the {@code currentNewsId} is {@code null}
     * @since 1.0.6
     */
    Long[] getNextAndPrevNewsId(Long currentNewsId,
            SearchCriteria searchCriteria) throws ServiceException;

    /**
     * Delete several news from the database.
     *
     * @param newsIds
     *            the array of news IDs which need to be deleted
     * @throws ServiceException
     *             when some errors occurs during the connection to the database
     *             or executing the query; this exception is also thrown when
     *             the {@code newsIds} is {@code null} or contains {@code null}
     *             IDs
     * @since 1.1.2
     */
    void deleteSeveral(Long... newsIds) throws ServiceException;
}
