/**
 * Provides exception classes for the DAO layer of the application.
 *
 * @author Ilya Hrachou
 * @version 1.0
 */
package com.epam.javalab.newsmanag.exceptions;
