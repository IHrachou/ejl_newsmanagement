package com.epam.javalab.newsmanag.services;

import com.epam.javalab.newsmanag.domain.User;
import com.epam.javalab.newsmanag.exceptions.ServiceException;

/**
 * This interface is used in service layer. It extends {@code IGenericService}
 * interface and specifies additional methods for handling {@code User} domain.
 * It is used for mapping DAO logic to the service layer.
 *
 * @author Ilya Hrachou
 * @version 1.1.7
 */
public interface IUserService extends IGenericService<User, Long> {
    /**
     * Call the DAO object to get the user by login.
     *
     * @param login
     *            user login
     * @return the user instance
     * @throws ServiceException
     *             when some errors occurs in the DAO layer or the {@code login}
     *             is {@code null} or empty
     */
    User getByLogin(String login) throws ServiceException;
}
