package com.epam.javalab.newsmanag.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.epam.javalab.newsmanag.dao.AuthorDao;
import com.epam.javalab.newsmanag.domain.Author;
import com.epam.javalab.newsmanag.exceptions.DaoException;
import com.epam.javalab.newsmanag.exceptions.ServiceException;
import com.epam.javalab.newsmanag.services.IAuthorService;

/**
 * Service class for handling {@code Author}. This class provides the access to
 * the DAO layer.
 *
 * @author Ilya Hrachou
 * @version 1.1.7
 */
@Service
@Transactional(value = "transactionManager",
        rollbackFor = ServiceException.class)
public class AuthorServiceImpl implements IAuthorService {

    @Autowired
    private AuthorDao authorDao;

    /*
     * (non-Javadoc)
     * @see
     * com.epam.javalab.newsmanag.services.IGenericService#add(java.lang.Object)
     */
    @Override
    public Long add(Author author) throws ServiceException {
        try {
            return authorDao.create(author);
        } catch (DaoException ex) {
            throw new ServiceException(ex);
        }
    }

    /*
     * (non-Javadoc)
     * @see com.epam.javalab.newsmanag.services.IGenericService#getByPk(java.io.
     * Serializable)
     */
    @Override
    public Author getByPk(Long authorId) throws ServiceException {
        try {
            return authorDao.findByPk(authorId);
        } catch (DaoException ex) {
            throw new ServiceException(ex);
        }
    }

    /*
     * (non-Javadoc)
     * @see com.epam.javalab.newsmanag.services.IGenericService#getAll()
     */
    @Override
    public List<Author> getAll() throws ServiceException {
        try {
            return authorDao.findAll();
        } catch (DaoException ex) {
            throw new ServiceException(ex);
        }
    }

    /*
     * (non-Javadoc)
     * @see com.epam.javalab.newsmanag.services.IGenericService#edit(java.lang.
     * Object)
     */
    @Override
    public void edit(Author author) throws ServiceException {
        try {
            authorDao.update(author);
        } catch (DaoException ex) {
            throw new ServiceException(ex);
        }
    }

    /*
     * (non-Javadoc)
     * @see
     * com.epam.javalab.newsmanag.services.IGenericService#delete(java.lang.
     * Long)
     */
    @Override
    public void delete(Long authorId) throws ServiceException {
        try {
            authorDao.delete(authorId);
        } catch (DaoException ex) {
            throw new ServiceException(ex);
        }
    }

    /*
     * (non-Javadoc)
     * @see
     * com.epam.javalab.newsmanag.services.IAuthorService#expire(java.lang.Long)
     */
    @Override
    public void expire(Long authorId) throws ServiceException {
        try {
            authorDao.expire(authorId);
        } catch (DaoException ex) {
            throw new ServiceException(ex);
        }
    }

    /*
     * (non-Javadoc)
     * @see
     * com.epam.javalab.newsmanag.services.IAuthorService#getByNewsId(java.lang.
     * Long)
     */
    @Override
    public List<Author> getByNewsId(Long newsId) throws ServiceException {
        try {
            return authorDao.findByNewsId(newsId);
        } catch (DaoException ex) {
            throw new ServiceException(ex);
        }
    }

    /*
     * (non-Javadoc)
     * @see
     * com.epam.javalab.newsmanag.services.IAuthorService#getListByIds(java.lang
     * .Long[])
     */
    @Override
    public List<Author> getListByIds(Long[] authorIds) throws ServiceException {
        try {
            return authorDao.findListByIds(authorIds);
        } catch (DaoException ex) {
            throw new ServiceException(ex);
        }
    }

    /*
     * (non-Javadoc)
     * @see com.epam.javalab.newsmanag.services.IAuthorService#findNonExpired()
     */
    @Override
    public List<Author> getNonExpired() throws ServiceException {
        try {
            return authorDao.findNonExpired();
        } catch (DaoException ex) {
            throw new ServiceException(ex);
        }
    }
}
