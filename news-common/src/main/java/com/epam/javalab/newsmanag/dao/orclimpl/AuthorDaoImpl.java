package com.epam.javalab.newsmanag.dao.orclimpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;

import com.epam.javalab.newsmanag.dao.AuthorDao;
import com.epam.javalab.newsmanag.dao.utils.DbUtil;
import com.epam.javalab.newsmanag.domain.Author;
import com.epam.javalab.newsmanag.exceptions.DaoException;

/**
 * Implementation of the DAO interface for the {@code Author} domain.
 *
 * @author Ilya Hrachou
 * @version 1.1.5
 */
@Repository
public class AuthorDaoImpl implements AuthorDao {
    // SQL queries
    private static final String SQL_CREATE = "INSERT INTO authors "
            + "(aut_author_name) values (?)";
    private static final String SQL_FIND_BY_PK = "SELECT aut_author_id_pk, "
            + "aut_author_name, aut_expired FROM authors "
            + "WHERE aut_author_id_pk=?";
    private static final String SQL_FIND_BY_NAME = "SELECT aut_author_id_pk, "
            + "aut_author_name, aut_expired FROM authors "
            + "WHERE aut_author_name=?";
    private static final String SQL_FIND_ALL = "SELECT aut_author_id_pk, "
            + "aut_author_name, aut_expired FROM authors";
    private static final String SQL_FIND_NONEXPIRED = "SELECT "
            + "aut_author_id_pk, aut_author_name, aut_expired FROM authors "
            + "WHERE aut_expired IS NULL";
    private static final String SQL_FIND_BY_NEWSID = "SELECT aut_author_id_pk, "
            + "aut_author_name, aut_expired FROM authors "
            + "JOIN news_authors ON aut_author_id_pk=na_author_id_fk "
            + "WHERE na_news_id_fk=?";
    private static final String SQL_UPDATE = "UPDATE authors "
            + "SET aut_author_name=? WHERE aut_author_id_pk=?";
    private static final String SQL_EXPIRE = "UPDATE authors "
            + "SET aut_expired=SYSTIMESTAMP WHERE aut_author_id_pk=?";
    private static final String SQL_FIND_LIST_BY_IDS = "SELECT "
            + "aut_author_id_pk, aut_author_name, aut_expired FROM authors "
            + "WHERE aut_author_id_pk IN(%s)";
    // Database column names
    private static final String AUTHOR_ID = "aut_author_id_pk";
    private static final String AUTHOR_NAME = "aut_author_name";
    private static final String AUTHOR_EXPIRED = "aut_expired";

    @Autowired
    private DataSource dataSource;

    /*
     * (non-Javadoc)
     * @see com.epam.javalab.newsmanag.dao.GenericDao#create(java.lang.Object)
     */
    @Override
    public Long create(Author newInstance) throws DaoException {
        if (newInstance == null) {
            throw new DaoException(
                    "Unable to create null Author in the database");
        }
        Connection connection = DataSourceUtils.getConnection(dataSource);
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String[] key = { AUTHOR_ID };
        try {
            statement = connection.prepareStatement(SQL_CREATE, key);
            statement.setString(1, newInstance.getName());
            statement.executeUpdate();
            resultSet = statement.getGeneratedKeys();
            if (resultSet.next()) {
                long id = resultSet.getLong(1);
                newInstance.setId(id);
                return id;
            }
            throw new DaoException(
                    "Cannot get primary key for the inserted author");
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            DbUtil.closeAll(dataSource, connection, statement, resultSet);
        }
    }

    /*
     * (non-Javadoc)
     * @see
     * com.epam.javalab.newsmanag.dao.GenericDao#findByPk(java.io.Serializable)
     */
    @Override
    public Author findByPk(Long primaryKey) throws DaoException {
        validateId(primaryKey);
        Connection connection = DataSourceUtils.getConnection(dataSource);
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        Author author = null;
        try {
            statement = connection.prepareStatement(SQL_FIND_BY_PK);
            statement.setLong(1, primaryKey);
            resultSet = statement.executeQuery();
            List<Author> authorList = parseResultSet(resultSet);
            if (authorList.size() != 1) {
                throw new DaoException(String.format(
                        "Cannot find author by ID=%d", primaryKey));
            }
            author = authorList.get(0);
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            DbUtil.closeAll(dataSource, connection, statement, resultSet);
        }
        return author;
    }

    /*
     * (non-Javadoc)
     * @see
     * com.epam.javalab.newsmanag.dao.AuthorDao#findByName(java.lang.String)
     */
    @Override
    public Author findByName(String name) throws DaoException {
        if (name == null) {
            throw new DaoException("Author name cannot be null");
        }
        Connection connection = DataSourceUtils.getConnection(dataSource);
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        Author author = null;
        try {
            statement = connection.prepareStatement(SQL_FIND_BY_NAME);
            statement.setString(1, name);
            resultSet = statement.executeQuery();
            List<Author> authorList = parseResultSet(resultSet);
            if (authorList.size() != 1) {
                throw new DaoException(String.format(
                        "Cannot find author by name=%s", name));
            }
            author = authorList.get(0);
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            DbUtil.closeAll(dataSource, connection, statement, resultSet);
        }
        return author;
    }

    /*
     * (non-Javadoc)
     * @see com.epam.javalab.newsmanag.dao.GenericDao#findAll()
     */
    @Override
    public List<Author> findAll() throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        List<Author> authorList;
        try {
            statement = connection.prepareStatement(SQL_FIND_ALL);
            resultSet = statement.executeQuery();
            authorList = parseResultSet(resultSet);
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            DbUtil.closeAll(dataSource, connection, statement, resultSet);
        }
        return authorList;
    }

    /*
     * (non-Javadoc)
     * @see com.epam.javalab.newsmanag.dao.GenericDao#findAll()
     */
    @Override
    public List<Author> findNonExpired() throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        List<Author> authorList;
        try {
            statement = connection.prepareStatement(SQL_FIND_NONEXPIRED);
            resultSet = statement.executeQuery();
            authorList = parseResultSet(resultSet);
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            DbUtil.closeAll(dataSource, connection, statement, resultSet);
        }
        return authorList;
    }

    /*
     * (non-Javadoc)
     * @see com.epam.javalab.newsmanag.dao.GenericDao#update(java.lang.Object)
     */
    @Override
    public void update(Author newInstance) throws DaoException {
        if (newInstance == null) {
            throw new DaoException(
                    "Null instance cannot be recorded to the database");
        }
        Connection connection = DataSourceUtils.getConnection(dataSource);
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(SQL_UPDATE);
            statement.setString(1, newInstance.getName());
            statement.setLong(2, newInstance.getId());
            int count = statement.executeUpdate();
            if (count != 1) {
                throw new DaoException(
                        "On update modify more than 1 record or nothing");
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            DbUtil.closeAll(dataSource, connection, statement);
        }
    }

    /*
     * (non-Javadoc)
     * @see com.epam.javalab.newsmanag.dao.AuthorDao#delete(java.lang.Long)
     */
    @Override
    public void delete(Long authorId) throws DaoException {
        throw new DaoException(
                "Unsupported operation. Author cannot be deleted");
    }

    /*
     * (non-Javadoc)
     * @see com.epam.javalab.newsmanag.dao.AuthorDao#expire(java.lang.Long)
     */
    @Override
    public void expire(Long authorId) throws DaoException {
        validateId(authorId);
        Connection connection = DataSourceUtils.getConnection(dataSource);
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(SQL_EXPIRE);
            statement.setLong(1, authorId);
            int count = statement.executeUpdate();
            if (count == 1) {
                return;
            }
            throw new DaoException(
                    "On update modify more than 1 record or nothing");
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            DbUtil.closeAll(dataSource, connection, statement);
        }
    }

    /*
     * (non-Javadoc)
     * @see
     * com.epam.javalab.newsmanag.dao.AuthorDao#findByNewsId(java.lang.Long)
     */
    @Override
    public List<Author> findByNewsId(Long newsId) throws DaoException {
        validateId(newsId);
        Connection connection = DataSourceUtils.getConnection(dataSource);
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        List<Author> authorList;
        try {
            statement = connection.prepareStatement(SQL_FIND_BY_NEWSID);
            statement.setLong(1, newsId);
            resultSet = statement.executeQuery();
            authorList = parseResultSet(resultSet);
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            DbUtil.closeAll(dataSource, connection, statement, resultSet);
        }
        return authorList;
    }

    private List<Author> parseResultSet(ResultSet resultSet)
            throws SQLException {
        List<Author> list = new ArrayList<Author>();
        Author author;
        while (resultSet.next()) {
            author = createAuthor(resultSet);
            list.add(author);
        }
        return list;
    }

    private Author createAuthor(ResultSet resultSet) throws SQLException {
        Author author = new Author();
        author.setId(resultSet.getLong(AUTHOR_ID));
        author.setName(resultSet.getString(AUTHOR_NAME));
        author.setExpired(resultSet.getTimestamp(AUTHOR_EXPIRED));
        return author;
    }

    private void validateId(Long id) throws DaoException {
        if (id == null) {
            throw new DaoException("Primary key cannot be null");
        }
    }

    /*
     * (non-Javadoc)
     * @see
     * com.epam.javalab.newsmanag.dao.AuthorDao#findListByIds(java.lang.Long[])
     */
    @Override
    public List<Author> findListByIds(Long[] authorIds) throws DaoException {
        if (authorIds == null) {
            throw new DaoException("Input array cannot be null");
        }
        Connection connection = DataSourceUtils.getConnection(dataSource);
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        List<Author> authorList;
        try {
            statement = connection.prepareStatement(
                    prepareQueryForFindListByIds(authorIds.length));
            for (int i = 0; i < authorIds.length; i++) {
                statement.setLong(i + 1, authorIds[i]);
            }
            resultSet = statement.executeQuery();
            authorList = parseResultSet(resultSet);
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            DbUtil.closeAll(dataSource, connection, statement, resultSet);
        }
        return authorList;
    }

    private String prepareQueryForFindListByIds(int amountOfparams) {
        StringBuilder queryParams = new StringBuilder();
        for (int i = 0; i < amountOfparams; i++) {
            queryParams.append("?,");
        }
        queryParams.deleteCharAt(queryParams.length() - 1);
        return String.format(SQL_FIND_LIST_BY_IDS, queryParams);
    }
}
