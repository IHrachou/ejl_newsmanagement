/**
 * This package contains utilities classes.
 *
 * @author Ilya Hrachou
 * @version 1.0
 */
package com.epam.javalab.newsmanag.dao.utils;
