package com.epam.javalab.newsmanag.dao;

import java.util.List;

import com.epam.javalab.newsmanag.domain.News;
import com.epam.javalab.newsmanag.domain.SearchCriteria;
import com.epam.javalab.newsmanag.exceptions.DaoException;

/**
 * This interface specifies the contract for the DAO operations with the
 * {@code News} domain. It extends the {@code GenericDao} interface. This
 * interface provides the specific methods for the DAO classes for the
 * {@code News} domain.
 *
 * @author Ilya Hrachou
 * @version 1.1.6
 */
public interface NewsDao extends GenericDao<News, Long> {
    /**
     * Return the amount of news in the database.
     *
     * @return the amount of news in the database
     * @throws DaoException
     *             when some errors occurs during the connection to the database
     *             or executing the query
     */
    long countNews() throws DaoException;

    /**
     * Return the amount of the filtered news in the database.
     *
     * @param searchCriteria
     *            the criteria for the news selection
     * @return the amount of the filtered news in the database
     * @throws DaoException
     *             when some errors occurs during the connection to the database
     *             or executing the query
     * @since 1.0.7
     */
    long countFilteredNews(SearchCriteria searchCriteria) throws DaoException;

    /**
     * This method is used to add authors to the news in the database. Authors
     * and the news IDs must be specified in method parameters.
     *
     * @param newsId
     *            the ID of the news to which author must be added
     * @param authorsIds
     *            the list of authors IDs who must be added to the news
     * @throws DaoException
     *             when some errors occurs during the connection to the database
     *             or executing the query; this exception is also thrown when
     *             either {@code newsId} or {@code authorsIds} is {@code null}
     *             or one of the authors ID is {@code null}
     */
    void addAuthors(Long newsId, List<Long> authorsIds) throws DaoException;

    /**
     * This method is used to delete a number of news from the database.
     *
     * @param newsIds
     *            the array with ids of news which need to be deleted
     * @throws DaoException
     *             when some errors occurs during the connection to the database
     *             or executing the query; this exception is also thrown when
     *             either {@code newsIds} is {@code null}
     * @since 1.1.2
     */
    void deleteNewsList(Long[] newsIds) throws DaoException;

    /**
     * This method is used to delete the author from the news in the database.
     * The author and the news IDs must be specified in method parameters. This
     * method deletes record from the many-to-many table which connected authors
     * and news. Author and news instances themselves are not deleted from the
     * database.
     *
     * @param newsId
     *            the ID of the news from which author must be deleted
     * @param authorId
     *            the ID of the author who must be deleted from the news
     * @throws DaoException
     *             when some errors occurs during the connection to the database
     *             or executing the query; this exception is also thrown when
     *             either {@code newsId} or {@code authorId} is {@code null}
     */
    void deleteAuthorFromNews(Long newsId, Long authorId) throws DaoException;

    /**
     * This method is used to delete several authors from the news in the
     * database. The authors and the news IDs must be specified in method
     * parameters. This method deletes records from the many-to-many table which
     * connected authors and news. Authors and news instances themselves are not
     * deleted from the database.
     *
     * @param newsId
     *            the ID of the news to which author must be added
     * @param authorsIds
     *            the list of authors IDs which must be deleted from the news
     * @throws DaoException
     *             when some errors occurs during the connection to the database
     *             or executing the query; this exception is also thrown when
     *             either {@code newsId} or {@code authorsIds} is {@code null}
     * @since 1.1.4
     */
    void deleteSeveralAuthorsFromNews(Long newsId, List<Long> authorsIds)
            throws DaoException;

    /**
     * This method is used to add tags to the news in the database. Tags and the
     * news must be specified in method parameters.
     *
     * @param newsId
     *            the ID of the news to which tag must be added
     * @param tagsIds
     *            the list of tags IDs which must be added to the news
     * @throws DaoException
     *             when some errors occurs during the connection to the database
     *             or executing the query; this exception is also thrown when
     *             either {@code newsId} or {@code tagsIds} is {@code null} or
     *             one of the tags ID is {@code null}
     */
    void addTags(Long newsId, List<Long> tagsIds) throws DaoException;

    /**
     * This method is used to delete the tag from the news in the database. The
     * tag and the news IDs must be specified in method parameters. This method
     * deletes record from the many-to-many table which connected tags and news.
     * Tag and news instances themselves are not deleted from the database.
     *
     * @param newsId
     *            the ID of the news from which tag must be deleted
     * @param tagId
     *            the ID of the tag which must be deleted from the news
     * @throws DaoException
     *             when some errors occurs during the connection to the database
     *             or executing the query; this exception is also thrown when
     *             either {@code newsId} or {@code tagId} is {@code null}
     */
    void deleteTagFromNews(Long newsId, Long tagId) throws DaoException;

    /**
     * This method is used to delete several tags from the news in the database.
     * The tags and the news IDs must be specified in method parameters. This
     * method deletes records from the many-to-many table which connected tags
     * and news. Tag and news instances themselves are not deleted from the
     * database.
     *
     * @param newsId
     *            the ID of the news to which tags must be added
     * @param tagsIds
     *            the list of tags IDs which must be deleted from the news
     * @throws DaoException
     *             when some errors occurs during the connection to the database
     *             or executing the query; this exception is also thrown when
     *             either {@code newsId} or {@code tagsIds} is {@code null}
     * @since 1.1.4
     */
    void deleteSeveralTagsFromNews(Long newsId, List<Long> tagsIds)
            throws DaoException;

    /**
     * This method is used to delete all tags from the news in the database. The
     * tags and the news IDs must be specified in method parameters. This method
     * deletes records from the many-to-many table which connected tags and
     * news. Tags and news instances themselves are not deleted from the
     * database.
     *
     * @param newsId
     *            the ID of the news to which tags must be added
     * @throws DaoException
     *             when some errors occurs during the connection to the database
     *             or executing the query; this exception is also thrown when
     *             either {@code newsId} is {@code null}
     * @since 1.1.6
     */
    void deleteAllTagsFromNews(Long newsId) throws DaoException;

    /**
     * Return the list of news which meets with criteria. The result list must
     * be sorted by the amount of comment and by the modification date.
     *
     * @param searchCriteria
     *            the criteria for the news selection
     * @return the list of news which meets with criteria
     * @throws DaoException
     *             when some errors occurs during the connection to the database
     *             or executing the query
     */
    List<News> findByCriteria(SearchCriteria searchCriteria)
            throws DaoException;

    /**
     * Return the list of news for the specified range. News are sorted by the
     * amount of comments and the modification date in the descending order.
     *
     * @param startPos
     *            the number of the first news inclusive
     * @param endPos
     *            the number of the last news inclusive
     * @return the list of news for the specified range
     * @throws DaoException
     *             when some errors occurs during the connection to the database
     *             or executing the query
     * @since 1.0.2
     */
    List<News> findForRange(long startPos, long endPos) throws DaoException;

    /**
     * Return the list of news for the specified range according to the
     * SearchCriteria. News are sorted by the amount of comments and the
     * modification date in the descending order.
     *
     * @param startPos
     *            the number of the first news inclusive
     * @param endPos
     *            the number of the last news inclusive
     * @param searchCriteria
     *            the criteria for the news selection
     * @return the list of news for the specified range
     * @throws DaoException
     *             when some errors occurs during the connection to the database
     *             or executing the query
     * @since 1.0.5
     */
    List<News> findForRange(long startPos, long endPos,
            SearchCriteria searchCriteria) throws DaoException;

    /**
     * Return the previous news for the specified ID from the database with all
     * its authors, tags and comments.
     *
     * @param currentNewsId
     *            the ID of the current news
     * @return the previous news for the specified ID from the database with all
     *         its authors, tags and comments
     * @throws DaoException
     *             when some errors occurs during the connection to the database
     *             or executing the query; this exception is also thrown when
     *             the {@code currentNewsId} is {@code null}
     * @since 1.0.3
     */
    News findPreviousNews(Long currentNewsId) throws DaoException;

    /**
     * Return the previous news for the specified ID from the database with all
     * its authors, tags and comments. It is used to navigate through the list
     * of news filtered by the search criteria.
     *
     * @param currentNewsId
     *            the ID of the current news
     * @param searchCriteria
     *            the criteria for the news filtering
     * @return the previous news for the specified ID from the database with all
     *         its authors, tags and comments
     * @throws DaoException
     *             when some errors occurs during the connection to the database
     *             or executing the query; this exception is also thrown when
     *             the {@code currentNewsId} is {@code null}
     * @since 1.0.6
     */
    News findPreviousNews(Long currentNewsId, SearchCriteria searchCriteria)
            throws DaoException;

    /**
     * Return the next news for the specified ID from the database with all its
     * authors, tags and comments.
     *
     * @param currentNewsId
     *            the ID of the current news
     * @return the next news for the specified ID from the database with all its
     *         authors, tags and comments
     * @throws DaoException
     *             when some errors occurs during the connection to the database
     *             or executing the query; this exception is also thrown when
     *             the {@code currentNewsId} is {@code null}
     * @since 1.0.3
     */
    News findNextNews(Long currentNewsId) throws DaoException;

    /**
     * Return the next news for the specified ID from the database with all its
     * authors, tags and comments. It is used to navigate through the list of
     * news filtered by the search criteria.
     *
     * @param currentNewsId
     *            the ID of the current news
     * @param searchCriteria
     *            the criteria for the news filtering
     * @return the next news for the specified ID from the database with all its
     *         authors, tags and comments
     * @throws DaoException
     *             when some errors occurs during the connection to the database
     *             or executing the query; this exception is also thrown when
     *             the {@code currentNewsId} is {@code null}
     * @since 1.0.6
     */
    News findNextNews(Long currentNewsId, SearchCriteria searchCriteria)
            throws DaoException;

    /**
     * Return the array of the two Long. The first is the ID of the previous
     * news and the second is the ID of the next news. The previous/next news ID
     * is searched in the filtered list of news. News list is filtered by search
     * criteria. If search criteria is null or empty this method will return the
     * IDs of the previous and next news from the non filtered list of news.
     *
     * @param currentNewsId
     *            the ID of the current news
     * @param searchCriteria
     *            the criteria for the news filtering
     * @return the array of the two IDs where the first value is previous news
     *         ID and the last value is the next news ID
     * @throws DaoException
     *             when some errors occurs during the connection to the database
     *             or executing the query; this exception is also thrown when
     *             the {@code currentNewsId} is {@code null}
     * @since 1.0.6
     */
    Long[] findPrevAndNextNewsId(Long currentNewsId,
            SearchCriteria searchCriteria) throws DaoException;
}
