package com.epam.javalab.newsmanag.domain;

import java.io.Serializable;

/**
 * This class represent Role domain from the database.
 *
 * @author Ilya Hrachou
 * @version 1.0
 */
public class Role implements Serializable {
    private static final long serialVersionUID = 2778599371386154229L;
    private Long id;
    private String name;

    /**
     * Default constructor for the {@code Role}.
     */
    public Role() {
        super();
    }

    /**
     * Return the role ID.
     *
     * @return the role ID
     */
    public Long getId() {
        return id;
    }

    /**
     * Set the role ID.
     *
     * @param roleId
     *            the role ID
     */
    public void setId(Long roleId) {
        this.id = roleId;
    }

    /**
     * Return the role name.
     *
     * @return the role name
     */
    public String getName() {
        return name;
    }

    /**
     * Set the role name.
     *
     * @param name
     *            the role name
     */
    public void setName(String name) {
        this.name = name;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = (prime * result) + ((id == null) ? 0 : id.hashCode());
        result = (prime * result) + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof Role)) {
            return false;
        }
        Role other = (Role) obj;
        if (id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (!id.equals(other.id)) {
            return false;
        }
        if (name == null) {
            if (other.name != null) {
                return false;
            }
        } else if (!name.equals(other.name)) {
            return false;
        }
        return true;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return String.format("Role [id=%d, name=%s]", id, name);
    }
}
