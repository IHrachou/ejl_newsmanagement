package com.epam.javalab.newsmanag.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.epam.javalab.newsmanag.dao.CommentDao;
import com.epam.javalab.newsmanag.domain.Comment;
import com.epam.javalab.newsmanag.exceptions.DaoException;
import com.epam.javalab.newsmanag.exceptions.ServiceException;
import com.epam.javalab.newsmanag.services.ICommentService;

/**
 * Service class for handling {@code Comment}. This class provides the access to
 * the DAO layer.
 *
 * @author Ilya Hrachou
 * @version 1.1.7
 */
@Service
@Transactional(value = "transactionManager",
        rollbackFor = ServiceException.class)
public class CommentServiceImpl implements ICommentService {

    @Autowired
    private CommentDao commentDao;

    /*
     * (non-Javadoc)
     * @see
     * com.epam.javalab.newsmanag.services.IGenericService#add(java.lang.Object)
     */
    @Override
    public Long add(Comment comment) throws ServiceException {
        try {
            return commentDao.create(comment);
        } catch (DaoException ex) {
            throw new ServiceException(ex);
        }
    }

    /*
     * (non-Javadoc)
     * @see com.epam.javalab.newsmanag.services.IGenericService#getByPk(java.io.
     * Serializable)
     */
    @Override
    public Comment getByPk(Long commentId) throws ServiceException {
        try {
            return commentDao.findByPk(commentId);
        } catch (DaoException ex) {
            throw new ServiceException(ex);
        }
    }

    /*
     * (non-Javadoc)
     * @see com.epam.javalab.newsmanag.services.IGenericService#getAll()
     */
    @Override
    public List<Comment> getAll() throws ServiceException {
        try {
            return commentDao.findAll();
        } catch (DaoException ex) {
            throw new ServiceException(ex);
        }
    }

    /*
     * (non-Javadoc)
     * @see com.epam.javalab.newsmanag.services.IGenericService#edit(java.lang.
     * Object)
     */
    @Override
    public void edit(Comment comment) throws ServiceException {
        try {
            commentDao.update(comment);
        } catch (DaoException ex) {
            throw new ServiceException(ex);
        }
    }

    /*
     * (non-Javadoc)
     * @see
     * com.epam.javalab.newsmanag.services.IGenericService#delete(java.lang.
     * Long)
     */
    @Override
    public void delete(Long commentId) throws ServiceException {
        try {
            commentDao.delete(commentId);
        } catch (DaoException ex) {
            throw new ServiceException(ex);
        }
    }

    /*
     * (non-Javadoc)
     * @see
     * com.epam.javalab.newsmanag.services.ICommentService#getByNewsId(java.lang
     * .Long)
     */
    @Override
    public List<Comment> getByNewsId(Long newsId) throws ServiceException {
        try {
            return commentDao.findByNewsId(newsId);
        } catch (DaoException ex) {
            throw new ServiceException(ex);
        }
    }
}
