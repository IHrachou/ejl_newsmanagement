package com.epam.javalab.newsmanag.dao;

import java.util.List;

import com.epam.javalab.newsmanag.domain.Comment;
import com.epam.javalab.newsmanag.exceptions.DaoException;

/**
 * This interface specifies the contract for the DAO operations with the
 * {@code Comment} domain. It extends the {@code GenericDao} interface. This
 * interface provides the specific methods for the DAO classes for the
 * {@code Comment} domain.
 *
 * @author Ilya Hrachou
 * @version 1.0
 */
public interface CommentDao extends GenericDao<Comment, Long> {
    /**
     * Return the {@code List} of all comments for the specified news from the
     * database.
     *
     * @param newsId
     *            the news ID which list of the comments must be returned
     * @return the {@code List} of all comments for the specified news
     * @throws DaoException
     *             when some errors occurs during the connection to the database
     *             or executing the query; this exception is also thrown when
     *             the {@code newsId} is {@code null}
     */
    List<Comment> findByNewsId(Long newsId) throws DaoException;
}
