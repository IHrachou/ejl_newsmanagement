package com.epam.javalab.newsmanag.dao.orclimpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;

import com.epam.javalab.newsmanag.dao.CommentDao;
import com.epam.javalab.newsmanag.dao.utils.DbUtil;
import com.epam.javalab.newsmanag.domain.Comment;
import com.epam.javalab.newsmanag.exceptions.DaoException;

/**
 * Implementation of the DAO interface for the {@code Comment} domain.
 *
 * @author Ilya Hrachou
 * @version 1.0
 */
@Repository()
public class CommentDaoImpl implements CommentDao {
    // SQL queries
    private static final String SQL_CREATE = "INSERT INTO comments "
            + "(com_news_id_fk, com_comment_text, com_creation_date) "
            + "values (?, ?, SYSTIMESTAMP)";
    private static final String SQL_FIND_BY_PK = "SELECT com_comment_id_pk, "
            + "com_news_id_fk, com_comment_text, com_creation_date "
            + "FROM comments WHERE com_comment_id_pk=?";
    private static final String SQL_FIND_ALL = "SELECT com_comment_id_pk, "
            + "com_news_id_fk, com_comment_text, com_creation_date "
            + "FROM comments";
    private static final String SQL_FIND_BY_NEWSID = "SELECT com_comment_id_pk,"
            + " com_news_id_fk, com_comment_text, com_creation_date "
            + "FROM comments WHERE com_news_id_fk=?";
    private static final String SQL_UPDATE = "UPDATE comments "
            + "SET com_news_id_fk=?, com_comment_text=?, com_creation_date=? "
            + "WHERE com_comment_id_pk=?";
    private static final String SQL_DELETE = "DELETE FROM comments "
            + "WHERE com_comment_id_pk=?";
    // Database column names
    private static final String COMMENT_ID = "com_comment_id_pk";
    private static final String COMMENT_NEWS_ID = "com_news_id_fk";
    private static final String COMMENT_TEXT = "com_comment_text";
    private static final String COMMENT_CREATION_DATE = "com_creation_date";

    @Autowired
    private DataSource dataSource;

    /*
     * (non-Javadoc)
     * @see com.epam.javalab.newsmanag.dao.GenericDao#create(java.lang.Object)
     */
    @Override
    public Long create(Comment newInstance) throws DaoException {
        if (newInstance == null) {
            throw new DaoException(
                    "Unable to create null Comment in the database");
        }
        Connection connection = DataSourceUtils.getConnection(dataSource);
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String[] key = { COMMENT_ID };
        try {
            statement = connection.prepareStatement(SQL_CREATE, key);
            statement.setLong(1, newInstance.getNewsId());
            statement.setString(2, newInstance.getText());
            statement.executeUpdate();
            resultSet = statement.getGeneratedKeys();
            if (resultSet.next()) {
                long id = resultSet.getLong(1);
                newInstance.setId(id);
                return id;
            }
            throw new DaoException(
                    "Cannot get primary key for the inserted comment");
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            DbUtil.closeAll(dataSource, connection, statement, resultSet);
        }
    }

    /*
     * (non-Javadoc)
     * @see
     * com.epam.javalab.newsmanag.dao.GenericDao#findByPk(java.io.Serializable)
     */
    @Override
    public Comment findByPk(Long primaryKey) throws DaoException {
        validateId(primaryKey);
        Connection connection = DataSourceUtils.getConnection(dataSource);
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        Comment comment = null;
        try {
            statement = connection.prepareStatement(SQL_FIND_BY_PK);
            statement.setLong(1, primaryKey);
            resultSet = statement.executeQuery();
            List<Comment> commentList = parseResultSet(resultSet);
            if (commentList.size() != 1) {
                throw new DaoException(String.format(
                        "Cannot find comment by ID=%d", primaryKey));
            }
            comment = commentList.get(0);
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            DbUtil.closeAll(dataSource, connection, statement, resultSet);
        }
        return comment;
    }

    /*
     * (non-Javadoc)
     * @see com.epam.javalab.newsmanag.dao.GenericDao#findAll()
     */
    @Override
    public List<Comment> findAll() throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        List<Comment> commentList;
        try {
            statement = connection.prepareStatement(SQL_FIND_ALL);
            resultSet = statement.executeQuery();
            commentList = parseResultSet(resultSet);
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            DbUtil.closeAll(dataSource, connection, statement, resultSet);
        }
        return commentList;
    }

    /*
     * (non-Javadoc)
     * @see com.epam.javalab.newsmanag.dao.GenericDao#update(java.lang.Object)
     */
    @Override
    public void update(Comment newInstance) throws DaoException {
        if (newInstance == null) {
            throw new DaoException(
                    "Null instance cannot be recorded to the database");
        }
        Connection connection = DataSourceUtils.getConnection(dataSource);
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(SQL_UPDATE);
            statement.setLong(1, newInstance.getNewsId());
            statement.setString(2, newInstance.getText());
            statement.setTimestamp(3, newInstance.getCreationDate());
            statement.setLong(4, newInstance.getId());
            int count = statement.executeUpdate();
            if (count != 1) {
                throw new DaoException(
                        "On update modify more than 1 record or nothing");
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            DbUtil.closeAll(dataSource, connection, statement);
        }
    }

    /*
     * (non-Javadoc)
     * @see com.epam.javalab.newsmanag.dao.GenericDao#delete(java.lang.Object)
     */
    @Override
    public void delete(Long commentId) throws DaoException {
        validateId(commentId);
        Connection connection = DataSourceUtils.getConnection(dataSource);
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(SQL_DELETE);
            statement.setLong(1, commentId);
            int count = statement.executeUpdate();
            if (count != 1) {
                throw new DaoException("Deleted more than 1 record or nothing");
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            DbUtil.closeAll(dataSource, connection, statement);
        }
    }

    /*
     * (non-Javadoc)
     * @see
     * com.epam.javalab.newsmanag.dao.CommentDao#findByNewsId(java.lang.Long)
     */
    @Override
    public List<Comment> findByNewsId(Long newsId) throws DaoException {
        validateId(newsId);
        Connection connection = DataSourceUtils.getConnection(dataSource);
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        List<Comment> comments;
        try {
            statement = connection.prepareStatement(SQL_FIND_BY_NEWSID);
            statement.setLong(1, newsId);
            resultSet = statement.executeQuery();
            comments = parseResultSet(resultSet);
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            DbUtil.closeAll(dataSource, connection, statement, resultSet);
        }
        return comments;
    }

    private List<Comment> parseResultSet(ResultSet resultSet)
            throws SQLException {
        List<Comment> list = new ArrayList<Comment>();
        Comment comment;
        while (resultSet.next()) {
            comment = createComment(resultSet);
            list.add(comment);
        }
        return list;
    }

    private Comment createComment(ResultSet resultSet) throws SQLException {
        Comment comment = new Comment();
        comment.setId(resultSet.getLong(COMMENT_ID));
        comment.setNewsId(resultSet.getLong(COMMENT_NEWS_ID));
        comment.setText(resultSet.getString(COMMENT_TEXT));
        comment.setCreationDate(resultSet.getTimestamp(COMMENT_CREATION_DATE));
        return comment;
    }

    private void validateId(Long id) throws DaoException {
        if (id == null) {
            throw new DaoException("Primary key cannot be null");
        }
    }
}
