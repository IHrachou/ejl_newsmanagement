package com.epam.javalab.newsmanag.services;

import com.epam.javalab.newsmanag.domain.Role;

/**
 * This interface is used in service layer. It extends {@code IGenericService}
 * interface and specifies additional methods for handling {@code Role} domain.
 * It is used for mapping DAO logic to the service layer.
 *
 * @author Ilya Hrachou
 * @version 1.0
 */
public interface IRoleService extends IGenericService<Role, Long> {

}
