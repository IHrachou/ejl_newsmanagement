package com.epam.javalab.newsmanag.dao;

import java.util.List;

import com.epam.javalab.newsmanag.domain.Role;
import com.epam.javalab.newsmanag.exceptions.DaoException;

/**
 * This interface specifies the contract for the DAO operations with the
 * {@code Role} domain. It extends the {@code GenericDao} interface. This
 * interface provides the specific methods for the DAO classes for the
 * {@code Role} domain.
 *
 * @author Ilya Hrachou
 * @version 1.0
 */
public interface RoleDao extends GenericDao<Role, Long> {
    /**
     * Return the role from the database specified by the name.
     *
     * @param name
     *            the name of the required role
     * @return the role from the database
     * @throws DaoException
     *             when some errors occurs during the connection to the database
     *             or executing the query; this exception is also thrown when
     *             the {@code name} is {@code null}
     */
    List<Role> findByName(String name) throws DaoException;
}
