package com.epam.javalab.newsmanag.dao;

import java.util.List;

import com.epam.javalab.newsmanag.domain.Author;
import com.epam.javalab.newsmanag.exceptions.DaoException;

/**
 * This interface specifies the contract for the DAO operations with the
 * {@code Author} domain. It extends the {@code GenericDao} interface. This
 * interface provides the specific methods for the DAO classes for the
 * {@code Author} domain.
 *
 * @author Ilya Hrachou
 * @version 1.1.5
 */
public interface AuthorDao extends GenericDao<Author, Long> {
    /**
     * Return the author from the database specified by the name.
     *
     * @param name
     *            the name of the required author
     * @return the author from the database
     * @throws DaoException
     *             when some errors occurs during the connection to the database
     *             or executing the query; this exception is also thrown when
     *             the {@code name} is {@code null}
     */
    Author findByName(String name) throws DaoException;

    /**
     * This method must only throw DaoException. Author cannot be deleted from
     * the database with this application. Author an only be expired.
     *
     * @see com.epam.javalab.newsmanag.dao.GenericDao#delete(java.lang.Long)
     * @param authorId
     *            the ID of the author who must be deleted from the database
     * @throws DaoException
     *             when some errors occurs during the connection to the database
     *             or executing the query; this exception is also thrown when
     *             the {@code authorId} is {@code null}
     */
    @Override
    void delete(Long authorId) throws DaoException;

    /**
     * Set the expiration date of the specified {@code Author} to the current
     * date. If the author is expired he could not add new news. But he will be
     * visible in the list of authors for the already created news.
     *
     * @param authorId
     *            the ID of the author who must be expired
     * @throws DaoException
     *             when some errors occurs during the connection to the database
     *             or executing the query; this exception is also thrown when
     *             the {@code authorId} is {@code null}
     */
    void expire(Long authorId) throws DaoException;

    /**
     * Return the {@code List} of all authors for the specified news from the
     * database.
     *
     * @param newsId
     *            the news ID which list of authors must be returned
     * @return the {@code List} of all authors for the specified news
     * @throws DaoException
     *             when some errors occurs during the connection to the database
     *             or executing the query; this exception is also thrown when
     *             the {@code newsId} is {@code null}
     */
    List<Author> findByNewsId(Long newsId) throws DaoException;

    /**
     * Return the {@code List} of authors for the specified list of ids. If the
     * input list of authors' ids contains values that are absent in the
     * database, those values will be skipped.
     *
     * @param authorIds
     *            the array of authors' ids for search
     * @return the {@code List} of authors for the specified list of ids
     * @throws DaoException
     *             when some errors occurs during the connection to the database
     *             or executing the query; this exception is also thrown when
     *             the {@code authorIdsk} is {@code null}
     * @since 1.0.4
     */
    List<Author> findListByIds(Long[] authorIds) throws DaoException;

    /**
     * Return the {@code List} of all non expired authors from the database.
     *
     * @return the list of all non expired authors
     * @throws DaoException
     *             when some errors occurs during the connection to the database
     *             or executing the query
     * @since 1.1.3
     */
    List<Author> findNonExpired() throws DaoException;
}
