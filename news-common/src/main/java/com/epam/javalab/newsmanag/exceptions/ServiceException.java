package com.epam.javalab.newsmanag.exceptions;

/**
 * The checked exception that can appear in the Service layer. It is also used
 * to re-throw Exceptions from the DAO layers.
 *
 * @author Ilya Hrachou
 * @version 1.0
 * @see java.lang.Exception
 */
public class ServiceException extends Exception {
    private static final long serialVersionUID = -2032544849621831091L;

    /**
     * Constructs a new {@code ServiceException} with null as it's detail
     * message.
     */
    public ServiceException() {
        super();
    }

    /**
     * Construct a new {@code ServiceException} with specified detail message
     * and root cause.
     *
     * @param message
     *            the detail message. The detail message is saved for later
     *            retrieval by the {@link #getMessage()} method.
     * @param cause
     *            the cause (which is saved for later retrieval by the
     *            {@link #getCause()} method). (A &lt;tt&gt;null&lt;/tt&gt;
     *            value is permitted, and indicates that the cause is
     *            nonexistent or unknown.)
     */
    public ServiceException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * Construct a new {@code ServiceException} with specified detail message.
     * The cause is not initialized, and may subsequently be initialized by a
     * call to {@link #initCause}.
     *
     * @param message
     *            the detail message. The detail message is saved for later
     *            retrieval by the {@link #getMessage()} method.
     */
    public ServiceException(String message) {
        super(message);
    }

    /**
     * Construct a new {@code ServiceException} with the specified cause and a
     * detail message of &lt;tt&gt;(cause==null ? null :
     * cause.toString())&lt;/tt&gt; (which typically contains the class and
     * detail message of &lt;tt&gt;cause&lt;/tt&gt;).
     *
     * @param cause
     *            the cause (which is saved for later retrieval by the
     *            {@link #getCause()} method). (A &lt;tt&gt;null&lt;/tt&gt;
     *            value is permitted, and indicates that the cause is
     *            nonexistent or unknown.)
     */
    public ServiceException(Throwable cause) {
        super(cause);
    }
}
