package com.epam.javalab.newsmanag.dao;

import java.io.Serializable;
import java.util.List;

import com.epam.javalab.newsmanag.exceptions.DaoException;

/**
 * This interface is the generic DAO interface that specify the contract to the
 * C.R.U.D. operations in the database. It is parameterized with the domain type
 * and primary key type.
 *
 * @author Ilya Hrachou
 * @version 1.0
 * @param <T>
 *            the type of the domain object
 * @param <PK>
 *            the type of the primary key
 */
public interface GenericDao<T, PK extends Serializable> {
    /**
     * Insert the {@code newInstance} object to the database. When object
     * inserted this method must get the value of the primary key for the
     * inserted record and set it to the input object.
     *
     * @param instance
     *            the domain instance that must be inserted to the database
     * @return the primary key of the inserted record
     * @throws DaoException
     *             when some errors occurs during the connection to the database
     *             or executing the query; this exception is also thrown when
     *             the {@code instance} is {@code null}
     */
    PK create(T instance) throws DaoException;

    /**
     * Return the object from the database specified by the primary key.
     *
     * @param primaryKey
     *            the primary key of the required object
     * @return the object from the database
     * @throws DaoException
     *             when some errors occurs during the connection to the database
     *             or executing the query; this exception is also thrown when
     *             the {@code primaryKey} is {@code null}
     */
    T findByPk(PK primaryKey) throws DaoException;

    /**
     * Return the {@code List} of all objects from the database.
     *
     * @return the object from the database
     * @throws DaoException
     *             when some errors occurs during the connection to the database
     *             or executing the query
     */
    List<T> findAll() throws DaoException;

    /**
     * Update the object in the database.
     *
     * @param instance
     *            the object that must be updated
     * @throws DaoException
     *             when some errors occurs during the connection to the database
     *             or executing the query; this exception is also thrown when
     *             the {@code instance} is {@code null}
     */
    void update(T instance) throws DaoException;

    /**
     * Delete the object from the database.
     *
     * @param primaryKey
     *            the primary key of the object which must be deleted from the
     *            database
     * @throws DaoException
     *             when some errors occurs during the connection to the database
     *             or executing the query; this exception is also thrown when
     *             the {@code primaryKey} is {@code null}
     */
    void delete(Long primaryKey) throws DaoException;
}
