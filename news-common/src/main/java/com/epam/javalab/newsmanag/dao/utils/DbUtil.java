package com.epam.javalab.newsmanag.dao.utils;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.DataSourceUtils;

import com.epam.javalab.newsmanag.exceptions.DaoException;

/**
 * Utility class that is used to close the {@code ResltSet} and the
 * {@code Statement} and to release the connection to the connection pool. This
 * class provides quite closing (without throwing exceptions).
 *
 * @author Ilya Hrachou
 * @version 1.1.2
 */
public final class DbUtil {
    private DbUtil() {
    }

    /**
     * Close JDBC resources and release the connection to the connection pool.
     *
     * @param dataSource
     *            {@code DataSource} instance for connections
     * @param connection
     *            the {@code Connection} to the database that must be released
     * @param statement
     *            the {@code Statement} which must be closed
     * @param resultSet
     *            the {@code ResultSet} which must be closed
     */
    public static void closeAll(DataSource dataSource, Connection connection,
            Statement statement, ResultSet resultSet) {
        closeResultSet(resultSet);
        closeAll(dataSource, connection, statement);
    }

    /**
     * Close JDBC resources and release the connection to the connection pool.
     *
     * @param dataSource
     *            {@code DataSource} instance for connections
     * @param connection
     *            the {@code Connection} to the database that must be released
     * @param statement
     *            the {@code Statement} which must be closed
     */
    public static void closeAll(DataSource dataSource, Connection connection,
            Statement statement) {
        closeStatement(statement);
        releaseConnection(dataSource, connection);
    }

    /**
     * Close JDBC ResultSet if it is not {@code null}.
     *
     * @param resultSet
     *            the {@code ResultSet} which must be closed
     * @since 1.1.2
     */
    public static void closeResultSet(ResultSet resultSet) {
        try {
            if (resultSet != null) {
                resultSet.close();
            }
        } catch (SQLException e) {
            // Nothing to do
        }
    }

    /**
     * Close several JDBC ResultSets if they are not {@code null}.
     *
     * @param resultSets
     *            the array of {@code ResultSet}s which must be closed
     * @since 1.1.2
     */
    public static void closeResultSet(ResultSet... resultSets) {
        for (ResultSet resultSet : resultSets) {
            closeResultSet(resultSet);
        }
    }

    /**
     * Close JDBC Statement if it is not {@code null}.
     *
     * @param statement
     *            the {@code Statement} which must be closed
     * @since 1.1.2
     */
    public static void closeStatement(Statement statement) {
        try {
            if (statement != null) {
                statement.close();
            }
        } catch (SQLException e) {
            // Nothing to do
        }

    }

    /**
     * Close several JDBC Statements if they are not {@code null}.
     *
     * @param statements
     *            the array of {@code Statement}s which must be closed
     * @since 1.1.2
     */
    public static void closeStatement(Statement... statements) {
        for (Statement statement : statements) {
            closeStatement(statement);
        }
    }

    /**
     * Release the connection to the connection pool.
     *
     * @param dataSource
     *            {@code DataSource} instance for connections
     * @param connection
     *            the {@code Connection} to the database that must be released
     * @since 1.1.2
     */
    public static void releaseConnection(DataSource dataSource,
            Connection connection) {
        try {
            DataSourceUtils.doReleaseConnection(connection, dataSource);
        } catch (SQLException e) {
            // Nothing to do
        }
    }

    /**
     * Rollback the connection and throws the DaoException if some errors occur.
     *
     * @param connection
     *            the {@code Connection} to the database that must be rolled
     *            back
     * @throws DaoException
     *             when some errors occur
     * @since 1.1.2
     */
    public static void connectionRollback(Connection connection)
            throws DaoException {
        try {
            connection.rollback();
        } catch (SQLException ex) {
            throw new DaoException(ex);
        }
    }
}
