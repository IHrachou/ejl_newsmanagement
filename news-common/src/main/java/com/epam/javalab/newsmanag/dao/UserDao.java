package com.epam.javalab.newsmanag.dao;

import com.epam.javalab.newsmanag.domain.User;
import com.epam.javalab.newsmanag.exceptions.DaoException;

/**
 * This interface specifies the contract for the DAO operations with the
 * {@code User} domain. It extends the {@code GenericDao} interface. This
 * interface provides the specific methods for the DAO classes for the
 * {@code User} domain.
 *
 * @author Ilya Hrachou
 * @version 1.0
 */
public interface UserDao extends GenericDao<User, Long> {
    /**
     * Return the user from the database specified by the login.
     *
     * @param login
     *            the login of the required user
     * @return the user from the database
     * @throws DaoException
     *             when some errors occurs during the connection to the database
     *             or executing the query; this exception is also thrown when
     *             the {@code login} is {@code null}
     */
    User findByLogin(String login) throws DaoException;
}
