package com.epam.javalab.newsmanag.domain;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;

/**
 * This class represent News domain from the database.
 *
 * @author Ilya Hrachou
 * @version 1.0
 */
public class News implements Serializable {
    private static final long serialVersionUID = 2493252210943323522L;
    private Long id;
    private String title;
    private String shortText;
    private String fullText;
    private Timestamp creationDate;
    private Date modificationDate;

    /**
     * Default constructor for the {@code News}.
     */
    public News() {
        super();
    }

    /**
     * Return the news id.
     *
     * @return the news id
     */
    public Long getId() {
        return id;
    }

    /**
     * Set the news id.
     *
     * @param id
     *            the news id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Return the news title.
     *
     * @return the news title
     */
    public String getTitle() {
        return title;
    }

    /**
     * Set the news title.
     *
     * @param title
     *            the news title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Return the news short text.
     *
     * @return the news short text
     */
    public String getShortText() {
        return shortText;
    }

    /**
     * Set the news short text.
     *
     * @param shortText
     *            the news short text
     */
    public void setShortText(String shortText) {
        this.shortText = shortText;
    }

    /**
     * Return the news full text.
     *
     * @return the news full text
     */
    public String getFullText() {
        return fullText;
    }

    /**
     * Set the news full text.
     *
     * @param fullText
     *            the news full text
     */
    public void setFullText(String fullText) {
        this.fullText = fullText;
    }

    /**
     * Return the news creation date.
     *
     * @return the news creation date
     */
    public Timestamp getCreationDate() {
        return creationDate;
    }

    /**
     * Set the news creation date.
     *
     * @param creationDate
     *            the news creation date
     */
    public void setCreationDate(Timestamp creationDate) {
        this.creationDate = creationDate;
    }

    /**
     * Return the news modification date.
     *
     * @return the news modification date
     */
    public Date getModificationDate() {
        return modificationDate;
    }

    /**
     * Set the news modification date.
     *
     * @param modificationDate
     *            the news modification date
     */
    public void setModificationDate(Date modificationDate) {
        this.modificationDate = modificationDate;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = (prime * result) + ((creationDate == null) ? 0
                : creationDate.hashCode());
        result = (prime * result) + ((fullText == null) ? 0
                : fullText.hashCode());
        result = (prime * result) + ((id == null) ? 0 : id.hashCode());
        result = (prime * result) + ((modificationDate == null) ? 0
                : modificationDate.hashCode());
        result = (prime * result) + ((shortText == null) ? 0
                : shortText.hashCode());
        result = (prime * result) + ((title == null) ? 0 : title.hashCode());
        return result;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof News)) {
            return false;
        }
        News other = (News) obj;
        if (id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (!id.equals(other.id)) {
            return false;
        }
        if (title == null) {
            if (other.title != null) {
                return false;
            }
        } else if (!title.equals(other.title)) {
            return false;
        }
        if (shortText == null) {
            if (other.shortText != null) {
                return false;
            }
        } else if (!shortText.equals(other.shortText)) {
            return false;
        }
        if (fullText == null) {
            if (other.fullText != null) {
                return false;
            }
        } else if (!fullText.equals(other.fullText)) {
            return false;
        }

        if (creationDate == null) {
            if (other.creationDate != null) {
                return false;
            }
        } else if (!creationDate.equals(other.creationDate)) {
            return false;
        }
        if (modificationDate == null) {
            if (other.modificationDate != null) {
                return false;
            }
        } else if (!modificationDate.toString().equals(other.modificationDate
                .toString())) {
            return false;
        }
        return true;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        String format = "News [id=%d, title=%s, shortText=%s, fullText=%s, "
                + "creationDate=%s, modificationDate=%s]";
        return String.format(format, id, title, shortText, fullText,
                creationDate, modificationDate);
    }
}
