package com.epam.javalab.newsmanag.domain;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * This class is used for searching required news by the authors and tags. It
 * contains sets of author IDs and tag IDs which must present in news.
 *
 * @author Ilya Hrachou
 * @version 1.0
 */
public class SearchCriteria implements Serializable {
    private static final long serialVersionUID = 8690989053332307772L;

    private Set<Long> authorIds = new HashSet<>();
    private Set<Long> tagIds = new HashSet<>();

    /**
     * Create new search criteria with empty lists of tags and authors.
     */
    public SearchCriteria() {
        super();
    }

    /**
     * Return the set of author IDs for this {@code SearchCriteria}.
     *
     * @return the set of author IDs for this {@code SearchCriteria}
     */
    public Set<Long> getAuthorIds() {
        return authorIds;
    }

    /**
     * Set the set of author IDs for this {@code SearchCriteria}.
     *
     * @param authorIds
     *            the set of author IDs for this {@code SearchCriteria}
     */
    public void setAuthorIds(Set<Long> authorIds) {
        if (authorIds != null) {
            this.authorIds = authorIds;
        }
    }

    /**
     * Return the set of tag IDs for this {@code SearchCriteria}.
     *
     * @return the set of tag IDs for this {@code SearchCriteria}
     */
    public Set<Long> getTagIds() {
        return tagIds;
    }

    /**
     * Set the set of tag IDs for this {@code SearchCriteria}.
     *
     * @param tagIds
     *            the set of tag IDs for this {@code SearchCriteria}
     */
    public void setTagIds(Set<Long> tagIds) {
        if (tagIds != null) {
            this.tagIds = tagIds;
        }
    }

    /**
     * Add the author ID to this {@code SearchCriteria}.
     *
     * @param authorId
     *            the author ID to be added to this set
     * @return true if this set did not already contain the specified element
     */
    public boolean addAuthorId(Long authorId) {
        if (authorId != null) {
            return authorIds.add(authorId);
        }
        return false;
    }

    /**
     * Remove the author ID from this {@code SearchCriteria}.
     *
     * @param authorId
     *            the author ID to be removed from this set, if present
     * @return true if this set contained the specified element
     */
    public boolean removeAuthorId(Long authorId) {
        if (authorId != null) {
            return authorIds.remove(authorId);
        }
        return false;
    }

    /**
     * Add the tag ID to this {@code SearchCriteria}.
     *
     * @param tagId
     *            the tag ID instance to be added to this set
     * @return true if this set did not already contain the specified element
     */
    public boolean addTagId(Long tagId) {
        if (tagId != null) {
            return tagIds.add(tagId);
        }
        return false;
    }

    /**
     * Remove the tag ID from this {@code SearchCriteria}.
     *
     * @param tagId
     *            the tag ID to be removed from this set, if present
     * @return true if this set contained the specified element
     */
    public boolean removeTagId(Long tagId) {
        if (tagId != null) {
            return tagIds.remove(tagId);
        }
        return false;
    }

    /**
     * Check if this SearchCriteria contains any tags or authors.
     *
     * @return {@code true} if this SearchCriteria does not contain any tags or
     *         authors
     */
    public boolean isEmpty() {
        return authorIds.isEmpty() && tagIds.isEmpty();
    }
}
