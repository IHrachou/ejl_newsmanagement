package com.epam.javalab.newsmanag.domain;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * This class represent Author domain from the database.
 *
 * @author Ilya Hrachou
 * @version 1.0
 */
public class Author implements Serializable {
    private static final long serialVersionUID = 7395107333694023580L;
    private Long id;
    private String name;
    private Timestamp expired;

    /**
     * Default constructor for the {@code Author}.
     */
    public Author() {
        super();
    }

    /**
     * Return the author's id.
     *
     * @return the author's id
     */
    public Long getId() {
        return id;
    }

    /**
     * Set the author's id.
     *
     * @param id
     *            the author's id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Return the author's name.
     *
     * @return the author's name
     */
    public String getName() {
        return name;
    }

    /**
     * Set the author's name.
     *
     * @param name
     *            the author's name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Return the date of expiration.
     *
     * @return the date of expiration
     */
    public Timestamp getExpired() {
        return expired;
    }

    /**
     * Set the date of expiration.
     *
     * @param expired
     *            the date of expiration
     */
    public void setExpired(Timestamp expired) {
        this.expired = expired;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = (prime * result) + ((expired == null) ? 0
                : expired.hashCode());
        result = (prime * result) + ((id == null) ? 0 : id.hashCode());
        result = (prime * result) + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof Author)) {
            return false;
        }
        Author other = (Author) obj;
        if (expired == null) {
            if (other.expired != null) {
                return false;
            }
        } else if (!expired.equals(other.expired)) {
            return false;
        }
        if (id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (!id.equals(other.id)) {
            return false;
        }
        if (name == null) {
            if (other.name != null) {
                return false;
            }
        } else if (!name.equals(other.name)) {
            return false;
        }
        return true;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return String.format("Author [id=%d, name=%s, expired=%s]", id, name,
                expired);
    }
}
