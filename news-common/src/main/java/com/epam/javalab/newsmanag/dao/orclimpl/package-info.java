/**
 * This package contains implementation of the DAO layer for the Oracle
 * database.
 *
 * @author Ilya Hrachou
 * @version 1.0
 */
package com.epam.javalab.newsmanag.dao.orclimpl;
