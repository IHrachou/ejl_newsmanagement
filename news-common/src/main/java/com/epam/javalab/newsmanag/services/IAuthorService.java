package com.epam.javalab.newsmanag.services;

import java.util.List;

import com.epam.javalab.newsmanag.domain.Author;
import com.epam.javalab.newsmanag.exceptions.ServiceException;

/**
 * This interface is used in service layer. It extends {@code IGenericService}
 * interface and specifies additional methods for handling {@code Author}
 * domain. It is used for mapping DAO logic to the service layer.
 *
 * @author Ilya Hrachou
 * @version 1.1.5
 */
public interface IAuthorService extends IGenericService<Author, Long> {
    /**
     * Call the DAO object to expire author. If author is expired he could not
     * add new news. But he is visible in the existing news.
     *
     * @param authorId
     *            the ID of the author who must be expired
     * @throws ServiceException
     *             when some errors occurs in the DAO layer or the
     *             {@code authorId} is {@code null}
     */
    void expire(Long authorId) throws ServiceException;

    /**
     * Call the DAO object to get the list of authors for the specified news.
     *
     * @param newsId
     *            the news ID which list of authors must be returned
     * @return the list of authors for the specified news
     * @throws ServiceException
     *             when some errors occurs in the DAO layer or the
     *             {@code newsId} is {@code null}
     */
    List<Author> getByNewsId(Long newsId) throws ServiceException;

    /**
     * Call the DAO object to get the list of authors for the specified set of
     * ids. Unavailable ids will be skipped.
     *
     * @param authorIds
     *            the list of ids
     * @return the list of authors for the specified set of ids
     * @throws ServiceException
     *             when some errors occurs in the DAO layer or the
     *             {@code authorIds} is {@code null}
     * @since 1.0.4
     */
    List<Author> getListByIds(Long[] authorIds) throws ServiceException;

    /**
     * Call the DAO object to get the list of all non expired authors.
     *
     * @return the list of all non expired authors
     * @throws ServiceException
     *             when some errors occurs in the DAO layer
     * @since 1.1.3
     */
    List<Author> getNonExpired() throws ServiceException;
}
