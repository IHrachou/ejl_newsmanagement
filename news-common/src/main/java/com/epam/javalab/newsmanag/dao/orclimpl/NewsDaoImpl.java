package com.epam.javalab.newsmanag.dao.orclimpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;

import com.epam.javalab.newsmanag.dao.NewsDao;
import com.epam.javalab.newsmanag.dao.utils.DbUtil;
import com.epam.javalab.newsmanag.domain.News;
import com.epam.javalab.newsmanag.domain.SearchCriteria;
import com.epam.javalab.newsmanag.exceptions.DaoException;

/**
 * Implementation of the DAO interface for the {@code News} domain.
 *
 * @author Ilya Hrachou
 * @version 1.1.6
 */
@Repository
public class NewsDaoImpl implements NewsDao {
    // SQL queries
    private static final String SQL_CREATE = "INSERT INTO news (ne_title, "
            + "ne_short_text, ne_full_text, ne_creation_date, "
            + "ne_modification_date) "
            + "values (?, ?, ?, SYSTIMESTAMP, TRUNC(SYSDATE,'DD'))";
    private static final String SQL_FIND_BY_PK = "SELECT ne_news_id_pk, "
            + "ne_title, ne_short_text, ne_full_text, ne_creation_date, "
            + "ne_modification_date FROM news WHERE ne_news_id_pk=%s";
    private static final String SQL_FIND_ALL_SORTED = "SELECT ne_news_id_pk, "
            + "ne_title, ne_short_text, ne_full_text, ne_creation_date, "
            + "ne_modification_date, COUNT(com_comment_id_pk) "
            + "as comment_amount "
            + "FROM news LEFT JOIN comments ON ne_news_id_pk = com_news_id_fk "
            + "GROUP BY ne_news_id_pk, ne_title, ne_short_text, ne_full_text, "
            + "ne_creation_date, ne_modification_date "
            + "ORDER BY COUNT(com_comment_id_pk) DESC, "
            + "ne_modification_date DESC";
    private static final String SQL_UPDATE = "UPDATE news SET ne_title=?, "
            + "ne_short_text=?, ne_full_text=?, "
            + "ne_modification_date=TRUNC(SYSDATE,'DD') WHERE ne_news_id_pk=?";
    private static final String SQL_DELETE = "DELETE FROM news "
            + "WHERE ne_news_id_pk=?";
    private static final String SQL_COUNT = "SELECT COUNT (*) FROM news";
    private static final String SQL_COUNT_FILTERED = "SELECT COUNT (*) "
            + "FROM (%s)";
    private static final String SQL_ADD_AUTHOR = "INSERT INTO news_authors "
            + "(na_news_id_fk, na_author_id_fk) values (?, ?)";
    private static final String SQL_DELETE_AUTHOR = "DELETE FROM news_authors "
            + "WHERE na_news_id_fk=? and na_author_id_fk=?";
    private static final String SQL_DELETE_ALL_AUTHORS = "DELETE "
            + "FROM news_authors WHERE na_news_id_fk=?";
    private static final String SQL_ADD_TAG = "INSERT INTO news_tags "
            + "(nt_news_id_fk, nt_tag_id_fk) values (?, ?)";
    private static final String SQL_DELETE_TAG = "DELETE FROM news_tags "
            + "WHERE nt_news_id_fk=? and nt_tag_id_fk=?";
    private static final String SQL_DELETE_ALL_TAGS = "DELETE FROM news_tags "
            + "WHERE nt_news_id_fk=?";
    private static final String SQL_DELETE_ALL_COMMENTS = "DELETE "
            + "FROM comments WHERE com_news_id_fk=?";
    private static final String SQL_FIND_FOR_RANGE = "SELECT ne_news_id_pk, "
            + "ne_title, ne_short_text, ne_full_text, ne_creation_date, "
            + "ne_modification_date FROM (SELECT a.*, ROWNUM rnum FROM "
            + "(%s) a WHERE ROWNUM<=?) WHERE rnum>=?";
    private static final String SQL_FIND_PREV_ID = "(SELECT prev_news "
            + "FROM (SELECT ne_news_id_pk, LAG(ne_news_id_pk, 1) "
            + "OVER(ORDER BY comment_amount DESC, ne_modification_date DESC) "
            + "AS prev_news FROM (%s)) WHERE ne_news_id_pk=?)";
    private static final String SQL_FIND_NEXT_ID = "(SELECT next_news "
            + "FROM (SELECT ne_news_id_pk, LEAD(ne_news_id_pk, 1) "
            + "OVER(ORDER BY comment_amount DESC, ne_modification_date DESC) "
            + "AS next_news FROM (%s)) WHERE ne_news_id_pk=?)";
    private static final String SQL_FIND_NEIGHBOR_ID = "(SELECT prev_news, "
            + "next_news FROM (SELECT ne_news_id_pk, LAG(ne_news_id_pk, 1) "
            + "OVER(ORDER BY comment_amount DESC, ne_modification_date DESC) "
            + "AS prev_news, LEAD(ne_news_id_pk, 1) "
            + "OVER(ORDER BY comment_amount DESC, ne_modification_date DESC) "
            + "AS next_news FROM (%s)) WHERE ne_news_id_pk=?)";
    private static final String SQL_FIND_BY_CRITERIA = "SELECT ne_news_id_pk, "
            + "ne_title, ne_short_text, ne_full_text, ne_creation_date, "
            + "ne_modification_date FROM news%s "
            + "LEFT JOIN comments ON (ne_news_id_pk = com_news_id_fk) WHERE%s "
            + "GROUP BY ne_news_id_pk, ne_title, ne_short_text, ne_full_text, "
            + "ne_creation_date, ne_modification_date "
            + "ORDER BY COUNT(com_comment_id_pk) DESC, "
            + "ne_modification_date DESC";
    // SQL parts for creating complex SQL query for searching by SearchCriteria
    private static final String SQL_CRITERIA_AUTHOR_JOIN = " JOIN news_authors "
            + "ON (ne_news_id_pk = na_news_id_fk)";
    private static final String SQL_CRITERIA_AUTHOR_CONDITION = " na_news_id_fk"
            + " IN(SELECT na_news_id_fk FROM news_authors WHERE na_author_id_fk"
            + " IN(%s) GROUP BY na_news_id_fk HAVING COUNT(*)=%d)";
    private static final String SQL_CRITERIA_TAG_JOIN = " JOIN news_tags "
            + "ON (ne_news_id_pk = nt_news_id_fk)";
    private static final String SQL_CRITERIA_TAG_CONDITION = " nt_news_id_fk "
            + "IN(SELECT nt_news_id_fk FROM news_tags WHERE nt_tag_id_fk "
            + "IN(%s) GROUP BY nt_news_id_fk HAVING COUNT(*)=%d)";
    // Database column names
    private static final String NEWS_ID = "ne_news_id_pk";
    private static final String NEWS_TITLE = "ne_title";
    private static final String NEWS_SHORT_TEXT = "ne_short_text";
    private static final String NEWS_FULL_TEXT = "ne_full_text";
    private static final String NEWS_CREATION_DATE = "ne_creation_date";
    private static final String NEWS_MODIFICATION_DATE = "ne_modification_date";

    @Autowired
    private DataSource dataSource;

    /*
     * (non-Javadoc)
     * @see com.epam.javalab.newsmanag.dao.GenericDao#create(java.lang.Object)
     */
    @Override
    public Long create(News newInstance) throws DaoException {
        if (newInstance == null) {
            throw new DaoException(
                    "Unable to create null News in the database");
        }
        Connection connection = DataSourceUtils.getConnection(dataSource);
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String[] key = { NEWS_ID };
        try {
            statement = connection.prepareStatement(SQL_CREATE, key);
            statement.setString(1, newInstance.getTitle());
            statement.setString(2, newInstance.getShortText());
            statement.setString(3, newInstance.getFullText());
            statement.executeUpdate();
            resultSet = statement.getGeneratedKeys();
            if (resultSet.next()) {
                long id = resultSet.getLong(1);
                newInstance.setId(id);
                return id;
            }
            throw new DaoException(
                    "Cannot get primary key for the inserted comment");
        } catch (SQLException ex) {
            throw new DaoException(ex);
        } finally {
            DbUtil.closeAll(dataSource, connection, statement, resultSet);
        }
    }

    /*
     * (non-Javadoc)
     * @see
     * com.epam.javalab.newsmanag.dao.GenericDao#findByPk(java.io.Serializable)
     */
    @Override
    public News findByPk(Long primaryKey) throws DaoException {
        return getNews(primaryKey, NewsFindType.CURRENT, null);
    }

    /**
     * Return the {@code List} of all {@code News} from the database sorted by
     * the amount of comments and modification date.
     *
     * @see com.epam.javalab.newsmanag.dao.GenericDao#findAll()
     * @return the {@code List} of all {@code News}
     */
    @Override
    public List<News> findAll() throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        List<News> newsList;
        try {
            statement = connection.prepareStatement(SQL_FIND_ALL_SORTED);
            resultSet = statement.executeQuery();
            newsList = parseResultSet(resultSet);
        } catch (SQLException ex) {
            throw new DaoException(ex);
        } finally {
            DbUtil.closeAll(dataSource, connection, statement, resultSet);
        }
        return newsList;
    }

    /*
     * (non-Javadoc)
     * @see com.epam.javalab.newsmanag.dao.NewsDao#findForRange(long, long)
     */
    @Override
    public List<News> findForRange(long startPos, long endPos)
            throws DaoException {
        return findForRange(startPos, endPos, new SearchCriteria());
    }

    /*
     * (non-Javadoc)
     * @see com.epam.javalab.newsmanag.dao.NewsDao#findForRange(long, long,
     * com.epam.javalab.newsmanag.domain.SearchCriteria)
     */
    @Override
    public List<News> findForRange(long startPos, long endPos,
            SearchCriteria searchCriteria) throws DaoException {
        String sqlQuerySearch = prepareSqlQueryForSearchByCriteria(
                searchCriteria);
        String sqlQuery = String.format(SQL_FIND_FOR_RANGE, sqlQuerySearch);
        Connection connection = DataSourceUtils.getConnection(dataSource);
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        List<News> newsList;
        try {
            statement = connection.prepareStatement(sqlQuery);
            statement.setLong(1, endPos);
            statement.setLong(2, startPos);
            resultSet = statement.executeQuery();
            newsList = parseResultSet(resultSet);
        } catch (SQLException ex) {
            throw new DaoException(ex);
        } finally {
            DbUtil.closeAll(dataSource, connection, statement, resultSet);
        }
        return newsList;
    }

    /*
     * (non-Javadoc)
     * @see com.epam.javalab.newsmanag.dao.GenericDao#update(java.lang.Object)
     */
    @Override
    public void update(News newInstance) throws DaoException {
        if (newInstance == null) {
            throw new DaoException(
                    "Null instance cannot be recorded to the database");
        }
        Connection connection = DataSourceUtils.getConnection(dataSource);
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(SQL_UPDATE);
            statement.setString(1, newInstance.getTitle());
            statement.setString(2, newInstance.getShortText());
            statement.setString(3, newInstance.getFullText());
            statement.setLong(4, newInstance.getId());
            int count = statement.executeUpdate();
            if (count != 1) {
                throw new DaoException(
                        "On update modify more than 1 record or nothing");
            }
        } catch (SQLException ex) {
            throw new DaoException(ex);
        } finally {
            DbUtil.closeAll(dataSource, connection, statement);
        }
    }

    /*
     * (non-Javadoc)
     * @see com.epam.javalab.newsmanag.dao.GenericDao#delete(java.lang.Long)
     */
    @Override
    public void delete(Long newsId) throws DaoException {
        validateId(newsId);
        Connection connection = DataSourceUtils.getConnection(dataSource);
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(SQL_DELETE);
            statement.setLong(1, newsId);
            int count = statement.executeUpdate();
            if (count != 1) {
                throw new DaoException("Deleted more than 1 record or nothing");
            }
        } catch (SQLException ex) {
            throw new DaoException(ex);
        } finally {
            DbUtil.closeAll(dataSource, connection, statement);
        }
    }

    /*
     * (non-Javadoc)
     * @see com.epam.javalab.newsmanag.dao.NewsDao#countNews()
     */
    @Override
    public long countNews() throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            statement = connection.prepareStatement(SQL_COUNT);
            resultSet = statement.executeQuery();
            resultSet.next();
            return resultSet.getLong(1);
        } catch (SQLException ex) {
            throw new DaoException(ex);
        } finally {
            DbUtil.closeAll(dataSource, connection, statement, resultSet);
        }
    }

    /*
     * (non-Javadoc)
     * @see
     * com.epam.javalab.newsmanag.dao.NewsDao#countFilteredNews(com.epam.javalab
     * .newsmanag.domain.SearchCriteria)
     */
    @Override
    public long countFilteredNews(SearchCriteria searchCriteria)
            throws DaoException {
        if ((searchCriteria == null) || (searchCriteria.isEmpty())) {
            return countNews();
        }
        Connection connection = DataSourceUtils.getConnection(dataSource);
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            String query = String.format(SQL_COUNT_FILTERED,
                    prepareSqlQueryForSearchByCriteria(searchCriteria));
            statement = connection.prepareStatement(query);
            resultSet = statement.executeQuery();
            resultSet.next();
            return resultSet.getLong(1);
        } catch (SQLException ex) {
            throw new DaoException(ex);
        } finally {
            DbUtil.closeAll(dataSource, connection, statement, resultSet);
        }
    }

    /*
     * (non-Javadoc)
     * @see com.epam.javalab.newsmanag.dao.NewsDao#addAuthor(java.lang.Long,
     * java.lang.Long)
     */
    @Override
    public void addAuthors(Long newsId, List<Long> authorsIds)
            throws DaoException {
        validateId(newsId);
        validateId(authorsIds);
        Connection connection = DataSourceUtils.getConnection(dataSource);
        PreparedStatement statement = null;
        try {
            connection.setAutoCommit(false);
            statement = connection.prepareStatement(SQL_ADD_AUTHOR);
            statement.setLong(1, newsId);
            for (Long authorId : authorsIds) {
                statement.setLong(2, authorId);
                statement.addBatch();
            }
            int[] count = statement.executeBatch();
            DbUtil.closeStatement(statement);
            for (int i : count) {
                if ((i != 1) && (i != PreparedStatement.SUCCESS_NO_INFO)) {
                    throw new DaoException("Failed to add authors to the news");
                }
            }
            connection.commit();
        } catch (SQLException ex) {
            DbUtil.connectionRollback(connection);
            throw new DaoException(ex);
        } finally {
            DbUtil.closeAll(dataSource, connection, statement);
        }
    }

    /*
     * (non-Javadoc)
     * @see
     * com.epam.javalab.newsmanag.dao.NewsDao#deleteAuthorFromNews(java.lang.
     * Long, java.lang.Long)
     */
    @Override
    public void deleteAuthorFromNews(Long newsId, Long authorId)
            throws DaoException {
        validateId(newsId, authorId);
        Connection connection = DataSourceUtils.getConnection(dataSource);
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(SQL_DELETE_AUTHOR);
            statement.setLong(1, newsId);
            statement.setLong(2, authorId);
            int count = statement.executeUpdate();
            if (count != 1) {
                throw new DaoException("Failed to delete author from the news");
            }
        } catch (SQLException ex) {
            throw new DaoException(ex);
        } finally {
            DbUtil.closeAll(dataSource, connection, statement);
        }
    }

    /*
     * (non-Javadoc)
     * @see
     * com.epam.javalab.newsmanag.dao.NewsDao#deleteSeveralAuthorsFromNews(java.
     * lang.Long, java.util.List)
     */
    @Override
    public void deleteSeveralAuthorsFromNews(Long newsId, List<Long> authorsIds)
            throws DaoException {
        validateId(newsId);
        validateId(authorsIds);
        Connection connection = DataSourceUtils.getConnection(dataSource);
        PreparedStatement statement = null;
        try {
            connection.setAutoCommit(false);
            statement = connection.prepareStatement(SQL_DELETE_AUTHOR);
            statement.setLong(1, newsId);
            for (Long authorId : authorsIds) {
                statement.setLong(2, authorId);
                statement.addBatch();
            }
            int[] count = statement.executeBatch();
            DbUtil.closeStatement(statement);
            for (int i : count) {
                if ((i != 1) && (i != PreparedStatement.SUCCESS_NO_INFO)) {
                    throw new DaoException(
                            "Failed to delete authors from the news");
                }
            }
            connection.commit();
        } catch (SQLException ex) {
            DbUtil.connectionRollback(connection);
            throw new DaoException(ex);
        } finally {
            DbUtil.closeAll(dataSource, connection, statement);
        }
    }

    /*
     * (non-Javadoc)
     * @see com.epam.javalab.newsmanag.dao.NewsDao#addTag(java.lang.Long,
     * java.lang.Long)
     */
    @Override
    public void addTags(Long newsId, List<Long> tagsIds) throws DaoException {
        validateId(newsId);
        validateId(tagsIds);
        Connection connection = DataSourceUtils.getConnection(dataSource);
        PreparedStatement statement = null;
        try {
            connection.setAutoCommit(false);
            statement = connection.prepareStatement(SQL_ADD_TAG);
            statement.setLong(1, newsId);
            for (Long tagId : tagsIds) {
                statement.setLong(2, tagId);
                statement.addBatch();
            }
            int[] count = statement.executeBatch();
            DbUtil.closeStatement(statement);
            for (int i : count) {
                if ((i != 1) && (i != PreparedStatement.SUCCESS_NO_INFO)) {
                    throw new DaoException("Failed to add tags to the news");
                }
            }
            connection.commit();
        } catch (SQLException ex) {
            DbUtil.connectionRollback(connection);
            throw new DaoException(ex);
        } finally {
            DbUtil.closeAll(dataSource, connection, statement);
        }
    }

    /*
     * (non-Javadoc)
     * @see
     * com.epam.javalab.newsmanag.dao.NewsDao#deleteTagFromNews(java.lang.Long,
     * java.lang.Long)
     */
    @Override
    public void deleteTagFromNews(Long newsId, Long tagId) throws DaoException {
        validateId(newsId, tagId);
        Connection connection = DataSourceUtils.getConnection(dataSource);
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(SQL_DELETE_TAG);
            statement.setLong(1, newsId);
            statement.setLong(2, tagId);
            int count = statement.executeUpdate();
            if (count != 1) {
                throw new DaoException("Failed to delete tag from the news");
            }
        } catch (SQLException ex) {
            throw new DaoException(ex);
        } finally {
            DbUtil.closeAll(dataSource, connection, statement);
        }
    }

    /*
     * (non-Javadoc)
     * @see
     * com.epam.javalab.newsmanag.dao.NewsDao#deleteSeveralTagsFromNews(java.
     * lang.Long, java.util.List)
     */
    @Override
    public void deleteSeveralTagsFromNews(Long newsId, List<Long> tagsIds)
            throws DaoException {
        validateId(newsId);
        validateId(tagsIds);
        Connection connection = DataSourceUtils.getConnection(dataSource);
        PreparedStatement statement = null;
        try {
            connection.setAutoCommit(false);
            statement = connection.prepareStatement(SQL_DELETE_TAG);
            statement.setLong(1, newsId);
            for (Long tagId : tagsIds) {
                statement.setLong(2, tagId);
                statement.addBatch();
            }
            int[] count = statement.executeBatch();
            DbUtil.closeStatement(statement);
            for (int i : count) {
                if ((i != 1) && (i != PreparedStatement.SUCCESS_NO_INFO)) {
                    throw new DaoException(
                            "Failed to delete tags from the news");
                }
            }
            connection.commit();
        } catch (SQLException ex) {
            DbUtil.connectionRollback(connection);
            throw new DaoException(ex);
        } finally {
            DbUtil.closeAll(dataSource, connection, statement);
        }

    }

    /*
     * (non-Javadoc)
     * @see
     * com.epam.javalab.newsmanag.dao.NewsDao#deleteAllTagsFromNews(java.lang.
     * Long)
     */
    @Override
    public void deleteAllTagsFromNews(Long newsId) throws DaoException {
        validateId(newsId);
        Connection connection = DataSourceUtils.getConnection(dataSource);
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(SQL_DELETE_ALL_TAGS);
            statement.setLong(1, newsId);
            statement.executeUpdate();
        } catch (SQLException ex) {
            DbUtil.connectionRollback(connection);
            throw new DaoException(ex);
        } finally {
            DbUtil.closeAll(dataSource, connection, statement);
        }

    }

    /*
     * (non-Javadoc)
     * @see
     * com.epam.javalab.newsmanag.dao.NewsDao#findByCriteria(com.epam.javalab.
     * newsmanag.domain.SearchCriteria)
     */
    @Override
    public List<News> findByCriteria(SearchCriteria searchCriteria)
            throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        List<News> newsList = null;
        try {
            String sqlQuery = prepareSqlQueryForSearchByCriteria(
                    searchCriteria);
            statement = connection.prepareStatement(sqlQuery);
            resultSet = statement.executeQuery();
            newsList = parseResultSet(resultSet);
        } catch (SQLException ex) {
            throw new DaoException(ex);
        } finally {
            DbUtil.closeAll(dataSource, connection, statement, resultSet);
        }
        return newsList;
    }

    private List<News> parseResultSet(ResultSet resultSet) throws SQLException {
        List<News> list = new ArrayList<News>();
        News news;
        while (resultSet.next()) {
            news = createNews(resultSet);
            list.add(news);
        }
        return list;
    }

    private News createNews(ResultSet resultSet) throws SQLException {
        News news = new News();
        news.setId(resultSet.getLong(NEWS_ID));
        news.setTitle(resultSet.getString(NEWS_TITLE));
        news.setShortText(resultSet.getString(NEWS_SHORT_TEXT));
        news.setFullText(resultSet.getString(NEWS_FULL_TEXT));
        news.setCreationDate(resultSet.getTimestamp(NEWS_CREATION_DATE));
        news.setModificationDate(resultSet.getDate(NEWS_MODIFICATION_DATE));
        return news;
    }

    private void validateId(Long... id) throws DaoException {
        for (Long longId : id) {
            if (longId == null) {
                throw new DaoException("Primary key cannot be null");
            }
        }
    }

    private void validateId(List<Long> ids) throws DaoException {
        if (ids == null) {
            throw new DaoException("Primary key cannot be null");
        }
        for (Long longId : ids) {
            if (longId == null) {
                throw new DaoException("Primary key cannot be null");
            }
        }
    }

    private String prepareSqlQueryForSearchByCriteria(
            SearchCriteria searchCriteria) {
        if ((searchCriteria == null) || (searchCriteria.isEmpty())) {
            return SQL_FIND_ALL_SORTED;
        }
        Set<Long> authorIds = searchCriteria.getAuthorIds();
        StringBuilder queryCondition = new StringBuilder();
        StringBuilder queryJoin = new StringBuilder();
        if (!authorIds.isEmpty()) {
            queryJoin.append(SQL_CRITERIA_AUTHOR_JOIN);
            queryCondition.append(String.format(SQL_CRITERIA_AUTHOR_CONDITION,
                    prepareIdList(authorIds), authorIds.size()));
        }
        Set<Long> tagIds = searchCriteria.getTagIds();
        if (!tagIds.isEmpty()) {
            queryJoin.append(SQL_CRITERIA_TAG_JOIN);
            if (queryCondition.length() != 0) {
                queryCondition.append(" AND");
            }
            queryCondition.append(String.format(SQL_CRITERIA_TAG_CONDITION,
                    prepareIdList(tagIds), tagIds.size()));
        }
        return String.format(SQL_FIND_BY_CRITERIA, queryJoin, queryCondition);
    }

    private String prepareIdList(Set<Long> set) {
        StringBuilder sb = new StringBuilder();
        Iterator<Long> it = set.iterator();
        for (;;) {
            Long id = it.next();
            sb.append(id);
            if (!it.hasNext()) {
                return sb.toString();
            }
            sb.append(',');
        }
    }

    /*
     * (non-Javadoc)
     * @see
     * com.epam.javalab.newsmanag.dao.NewsDao#findPreviousNews(java.lang.Long)
     */
    @Override
    public News findPreviousNews(Long currentNewsId) throws DaoException {
        return getNews(currentNewsId, NewsFindType.PREVIOUS, null);
    }

    /*
     * (non-Javadoc)
     * @see
     * com.epam.javalab.newsmanag.dao.NewsDao#findPreviousNews(java.lang.Long,
     * com.epam.javalab.newsmanag.domain.SearchCriteria)
     */
    @Override
    public News findPreviousNews(Long currentNewsId,
            SearchCriteria searchCriteria) throws DaoException {
        return getNews(currentNewsId, NewsFindType.PREVIOUS, searchCriteria);
    }

    /*
     * (non-Javadoc)
     * @see com.epam.javalab.newsmanag.dao.NewsDao#findNextNews(java.lang.Long)
     */
    @Override
    public News findNextNews(Long currentNewsId) throws DaoException {
        return getNews(currentNewsId, NewsFindType.NEXT, null);
    }

    /*
     * (non-Javadoc)
     * @see com.epam.javalab.newsmanag.dao.NewsDao#findNextNews(java.lang.Long,
     * com.epam.javalab.newsmanag.domain.SearchCriteria)
     */
    @Override
    public News findNextNews(Long currentNewsId, SearchCriteria searchCriteria)
            throws DaoException {
        return getNews(currentNewsId, NewsFindType.NEXT, searchCriteria);
    }

    private enum NewsFindType {
        CURRENT, PREVIOUS, NEXT;
    }

    private News getNews(Long newsId, NewsFindType type,
            SearchCriteria searchCriteria) throws DaoException {
        validateId(newsId);
        Connection connection = DataSourceUtils.getConnection(dataSource);
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            String query = getQueryForSingleNewsFind(type, searchCriteria);
            statement = connection.prepareStatement(query);
            statement.setLong(1, newsId);
            resultSet = statement.executeQuery();
            return parseResultSetSingleNews(resultSet);
        } catch (SQLException ex) {
            throw new DaoException(ex);
        } finally {
            DbUtil.closeAll(dataSource, connection, statement, resultSet);
        }
    }

    private News parseResultSetSingleNews(ResultSet resultSet)
            throws SQLException {
        if (!resultSet.next()) {
            return null;
        }
        News news = createNews(resultSet);
        if (resultSet.next()) {
            throw new DaoException("Cannot find news by ID");
        }
        return news;
    }

    // Create string SQL query for finding single news
    private String getQueryForSingleNewsFind(NewsFindType type,
            SearchCriteria searchCriteria) {
        String temp;
        switch (type) {
        case CURRENT:
            temp = "?";
            break;
        case PREVIOUS:
            temp = String.format(SQL_FIND_PREV_ID,
                    prepareSqlQueryForSearchByCriteria(searchCriteria));
            break;
        case NEXT:
            temp = String.format(SQL_FIND_NEXT_ID,
                    prepareSqlQueryForSearchByCriteria(searchCriteria));
            break;
        default:
            throw new EnumConstantNotPresentException(type.getClass(), type
                    .name());
        }
        return String.format(SQL_FIND_BY_PK, temp);
    }

    /*
     * (non-Javadoc)
     * @see
     * com.epam.javalab.newsmanag.dao.NewsDao#findPrevAndNextNewsId(java.lang.
     * Long, com.epam.javalab.newsmanag.domain.SearchCriteria)
     */
    @Override
    public Long[] findPrevAndNextNewsId(Long currentNewsId,
            SearchCriteria searchCriteria) throws DaoException {
        validateId(currentNewsId);
        Connection connection = DataSourceUtils.getConnection(dataSource);
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            String query = String.format(SQL_FIND_NEIGHBOR_ID,
                    prepareSqlQueryForSearchByCriteria(searchCriteria));
            statement = connection.prepareStatement(query);
            statement.setLong(1, currentNewsId);
            resultSet = statement.executeQuery();
            return parseResultSetPrevAndNextNewsId(resultSet);
        } catch (SQLException ex) {
            throw new DaoException(ex);
        } finally {
            DbUtil.closeAll(dataSource, connection, statement, resultSet);
        }
    }

    private Long[] parseResultSetPrevAndNextNewsId(ResultSet resultSet)
            throws SQLException {
        Long[] result = new Long[2];
        if (resultSet.next()) {
            long val = resultSet.getLong(1);
            if (!resultSet.wasNull()) {
                result[0] = val;
            }
            val = resultSet.getLong(2);
            if (!resultSet.wasNull()) {
                result[1] = val;
            }
            return result;
        }
        return null;
    }

    /*
     * (non-Javadoc)
     * @see
     * com.epam.javalab.newsmanag.dao.NewsDao#deleteNewsList(java.lang.Long[])
     */
    @Override
    public void deleteNewsList(Long[] newsIds) throws DaoException {
        if (newsIds == null) {
            return;
        }
        validateId(newsIds);
        Connection connection = DataSourceUtils.getConnection(dataSource);
        PreparedStatement delAuthorsStatement = null;
        PreparedStatement delTagsStatement = null;
        PreparedStatement delCommentsStatement = null;
        PreparedStatement delNewsStatement = null;
        try {
            connection.setAutoCommit(false);
            delAuthorsStatement = connection.prepareStatement(
                    SQL_DELETE_ALL_AUTHORS);
            delTagsStatement = connection.prepareStatement(SQL_DELETE_ALL_TAGS);
            delCommentsStatement = connection.prepareStatement(
                    SQL_DELETE_ALL_COMMENTS);
            delNewsStatement = connection.prepareStatement(SQL_DELETE);
            for (Long newsId : newsIds) {
                delAuthorsStatement.setLong(1, newsId);
                delAuthorsStatement.addBatch();
                delTagsStatement.setLong(1, newsId);
                delTagsStatement.addBatch();
                delCommentsStatement.setLong(1, newsId);
                delCommentsStatement.addBatch();
                delNewsStatement.setLong(1, newsId);
                delNewsStatement.addBatch();
            }
            delAuthorsStatement.executeBatch();
            delTagsStatement.executeBatch();
            delCommentsStatement.executeBatch();
            delNewsStatement.executeBatch();
            connection.commit();
        } catch (SQLException ex) {
            DbUtil.connectionRollback(connection);
            throw new DaoException(ex);
        } finally {
            DbUtil.closeStatement(delAuthorsStatement, delTagsStatement,
                    delCommentsStatement, delNewsStatement);
            DbUtil.releaseConnection(dataSource, connection);
        }
    }
}
