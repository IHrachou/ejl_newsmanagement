package com.epam.javalab.newsmanag.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.epam.javalab.newsmanag.dao.TagDao;
import com.epam.javalab.newsmanag.domain.Tag;
import com.epam.javalab.newsmanag.exceptions.DaoException;
import com.epam.javalab.newsmanag.exceptions.ServiceException;
import com.epam.javalab.newsmanag.services.ITagService;

/**
 * Service class for handling {@code Tag}. This class provides the access to the
 * DAO layer.
 *
 * @author Ilya Hrachou
 * @version 1.1.7
 */
@Service
@Transactional(value = "transactionManager",
        rollbackFor = ServiceException.class)
public class TagServiceImpl implements ITagService {

    @Autowired
    private TagDao tagDao;

    /*
     * (non-Javadoc)
     * @see
     * com.epam.javalab.newsmanag.services.IGenericService#add(java.lang.Object)
     */
    @Override
    public Long add(Tag tag) throws ServiceException {
        try {
            return tagDao.create(tag);
        } catch (DaoException ex) {
            throw new ServiceException(ex);
        }
    }

    /*
     * (non-Javadoc)
     * @see com.epam.javalab.newsmanag.services.IGenericService#getByPk(java.io.
     * Serializable)
     */
    @Override
    public Tag getByPk(Long tagId) throws ServiceException {
        try {
            return tagDao.findByPk(tagId);
        } catch (DaoException ex) {
            throw new ServiceException(ex);
        }
    }

    /*
     * (non-Javadoc)
     * @see com.epam.javalab.newsmanag.services.IGenericService#getAll()
     */
    @Override
    public List<Tag> getAll() throws ServiceException {
        try {
            return tagDao.findAll();
        } catch (DaoException ex) {
            throw new ServiceException(ex);
        }
    }

    /*
     * (non-Javadoc)
     * @see com.epam.javalab.newsmanag.services.IGenericService#edit(java.lang.
     * Object)
     */
    @Override
    public void edit(Tag tag) throws ServiceException {
        try {
            tagDao.update(tag);
        } catch (DaoException ex) {
            throw new ServiceException(ex);
        }
    }

    /*
     * (non-Javadoc)
     * @see
     * com.epam.javalab.newsmanag.services.IGenericService#delete(java.lang.
     * Long)
     */
    @Override
    public void delete(Long tagId) throws ServiceException {
        try {
            tagDao.delete(tagId);
        } catch (DaoException ex) {
            throw new ServiceException(ex);
        }
    }

    /*
     * (non-Javadoc)
     * @see
     * com.epam.javalab.newsmanag.services.ITagService#getByNewsId(java.lang.
     * Long)
     */
    @Override
    public List<Tag> getByNewsId(Long newsId) throws ServiceException {
        try {
            return tagDao.findByNewsId(newsId);
        } catch (DaoException ex) {
            throw new ServiceException(ex);
        }
    }

    /*
     * (non-Javadoc)
     * @see
     * com.epam.javalab.newsmanag.services.ITagService#getListByIds(java.lang.
     * Long[])
     */
    @Override
    public List<Tag> getListByIds(Long[] tagIds) throws ServiceException {
        try {
            return tagDao.findListByIds(tagIds);
        } catch (DaoException ex) {
            throw new ServiceException(ex);
        }
    }
}
