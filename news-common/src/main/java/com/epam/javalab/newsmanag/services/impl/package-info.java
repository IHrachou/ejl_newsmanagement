/**
 * This package contains implementation of the Service layer.
 *
 * @author Ilya Hrachou
 * @version 1.0
 */
package com.epam.javalab.newsmanag.services.impl;
