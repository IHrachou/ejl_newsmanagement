package com.epam.javalab.newsmanag.services;

import java.io.Serializable;
import java.util.List;

import com.epam.javalab.newsmanag.exceptions.ServiceException;

/**
 * This interface is used in service layer. It specifies methods for C.R.U.D.
 * operations. It is used for mapping DAO logic to the service layer.
 *
 * @author Ilya Hrachou
 * @version 1.0
 * @param <T>
 *            the type of the domain object of the current service
 * @param <PK>
 *            the type of the primary key
 */
public interface IGenericService<T, PK extends Serializable> {
    /**
     * Call the DAO object to add new domain object instance to the database.
     *
     * @param instance
     *            the domain instance that must be inserted to the database
     * @return the primary key of the inserted record
     * @throws ServiceException
     *             when some errors occurs in the DAO layer or the
     *             {@code instance} is {@code null}
     */
    PK add(T instance) throws ServiceException;

    /**
     * Call the DAO object to get the object instance specified by the primary
     * key from the database .
     *
     * @param primaryKey
     *            the primary key of the required object
     * @return the object from the database
     * @throws ServiceException
     *             when some errors occurs in the DAO layer or the
     *             {@code primaryKey} is {@code null}
     */
    T getByPk(PK primaryKey) throws ServiceException;

    /**
     * Call the DAO object to get the {@code List} of all objects from the
     * database.
     *
     * @return the object from the database
     * @throws ServiceException
     *             when some errors occurs in the DAO layer
     */
    List<T> getAll() throws ServiceException;

    /**
     * Call the DAO object to update the object instance in the database.
     *
     * @param newInstance
     *            the object that must be updated
     * @throws ServiceException
     *             when some errors occurs in the DAO layer or the
     *             {@code newInstance} is {@code null}
     */
    void edit(T newInstance) throws ServiceException;

    /**
     * Call the DAO object to delete the object instance from the database.
     *
     * @param id
     *            the id of the object which must be deleted from the database
     * @throws ServiceException
     *             when some errors occurs in the DAO layer or the {@code id} is
     *             {@code null}
     */
    void delete(Long id) throws ServiceException;
}
