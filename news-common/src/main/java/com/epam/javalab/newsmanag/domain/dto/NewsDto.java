package com.epam.javalab.newsmanag.domain.dto;

import java.util.ArrayList;
import java.util.List;

import com.epam.javalab.newsmanag.domain.Author;
import com.epam.javalab.newsmanag.domain.Comment;
import com.epam.javalab.newsmanag.domain.News;
import com.epam.javalab.newsmanag.domain.Tag;

/**
 * Data Transfer Object (DTO) for the news domain.
 *
 * @author Ilya Hrachou
 * @version 1.1.6
 */
public class NewsDto {
    private News news = new News();
    private List<Author> authors;
    private List<Tag> tags;
    private List<Comment> comments;

    /**
     * Default constructor inherited from Object.
     */
    public NewsDto() {
        super();
    }

    /**
     * Return the {@code News} instance for this DTO.
     *
     * @return the {@code News} instance
     */
    public News getNews() {
        return news;
    }

    /**
     * Set the {@code News} instance to this DTO.
     *
     * @param news
     *            the {@code News} instance
     */
    public void setNews(News news) {
        this.news = news;
    }

    /**
     * Return the list of {@code Author} instances for this DTO.
     *
     * @return the list of {@code Author} instances
     */
    public List<Author> getAuthors() {
        return authors;
    }

    /**
     * Return the list of authors IDs for this DTO.
     *
     * @return the list of authors IDs
     * @since 1.1.2
     */
    public List<Long> getAuthorsIds() {
        List<Long> authorsIds = new ArrayList<>();
        for (Author author : authors) {
            authorsIds.add(author.getId());
        }
        return authorsIds;
    }

    /**
     * Set the list of {@code Author} instances to this DTO.
     *
     * @param authors
     *            the list of {@code Author} instances
     */
    public void setAuthors(List<Author> authors) {
        this.authors = authors;
    }

    /**
     * Return the list of {@code Tag} instances for this DTO.
     *
     * @return the list of {@code Tag} instances
     */
    public List<Tag> getTags() {
        return tags;
    }

    /**
     * Return the list of tags IDs for this DTO or null if there are no tags.
     *
     * @return the list of tags IDs
     * @since 1.1.2
     */
    public List<Long> getTagsIds() {
        List<Long> tagsIds = new ArrayList<>();
        if (tags == null) {
            return null;
        }
        for (Tag tag : tags) {
            tagsIds.add(tag.getId());
        }
        return tagsIds;
    }

    /**
     * Set the list of {@code Tag} instances to this DTO.
     *
     * @param tags
     *            the list of {@code Tag} instances
     */
    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    /**
     * Return the list of {@code Comments} instances for this DTO.
     *
     * @return the list of {@code Comments} instances
     */
    public List<Comment> getComments() {
        return comments;
    }

    /**
     * Set the list of {@code Comments} instances to this DTO.
     *
     * @param comments
     *            the list of {@code Comments} instances
     */
    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = (prime * result) + ((authors == null) ? 0
                : authors.hashCode());
        result = (prime * result) + ((comments == null) ? 0
                : comments.hashCode());
        result = (prime * result) + ((news == null) ? 0 : news.hashCode());
        result = (prime * result) + ((tags == null) ? 0 : tags.hashCode());
        return result;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof NewsDto)) {
            return false;
        }
        NewsDto other = (NewsDto) obj;
        if (authors == null) {
            if (other.authors != null) {
                return false;
            }
        } else if (!authors.equals(other.authors)) {
            return false;
        }
        if (comments == null) {
            if (other.comments != null) {
                return false;
            }
        } else if (!comments.equals(other.comments)) {
            return false;
        }
        if (news == null) {
            if (other.news != null) {
                return false;
            }
        } else if (!news.equals(other.news)) {
            return false;
        }
        if (tags == null) {
            if (other.tags != null) {
                return false;
            }
        } else if (!tags.equals(other.tags)) {
            return false;
        }
        return true;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("NewsDto [newsID=");
        builder.append(news.getId());
        builder.append(", authors=");
        for (Author author : authors) {
            builder.append(author);
            builder.append(", ");
        }
        builder.append(", tagsID=(");
        for (Tag tag : tags) {
            builder.append(tag);
            builder.append(", ");
        }
        builder.append("), comments=(");
        for (Comment comment : comments) {
            builder.append(comment);
            builder.append(", ");
        }
        builder.append(")]");
        return builder.toString();
    }
}
