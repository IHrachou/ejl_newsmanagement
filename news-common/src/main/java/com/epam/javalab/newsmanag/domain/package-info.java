/**
 * This package contains domain bean classes that is used in business logic.
 *
 * @author Ilya Hrachou
 * @version 1.0
 */
package com.epam.javalab.newsmanag.domain;
