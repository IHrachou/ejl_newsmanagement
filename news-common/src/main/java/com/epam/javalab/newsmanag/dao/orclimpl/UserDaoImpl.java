package com.epam.javalab.newsmanag.dao.orclimpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;

import com.epam.javalab.newsmanag.dao.UserDao;
import com.epam.javalab.newsmanag.dao.utils.DbUtil;
import com.epam.javalab.newsmanag.domain.User;
import com.epam.javalab.newsmanag.exceptions.DaoException;

/**
 * Implementation of the DAO interface for the {@code User} domain.
 *
 * @author Ilya Hrachou
 * @version 1.1.7
 */
@Repository()
public class UserDaoImpl implements UserDao {
    // SQL queries
    private static final String SQL_CREATE = "INSERT INTO users "
            + "(usr_user_name, usr_login, usr_password, usr_role_id_fk) "
            + "values (?, ?, ?, ?)";
    private static final String SQL_FIND_BY_PK = "SELECT usr_user_id_pk, "
            + "usr_user_name, usr_login, usr_password, usr_role_id_fk "
            + "FROM users WHERE usr_user_id_pk=?";
    private static final String SQL_FIND_BY_LOGIN = "SELECT usr_user_id_pk, "
            + "usr_user_name, usr_login, usr_password, usr_role_id_fk "
            + "FROM users WHERE usr_login=?";
    private static final String SQL_FIND_ALL = "SELECT usr_user_id_pk, "
            + "usr_user_name, usr_login, usr_password, usr_role_id_fk "
            + "FROM users";
    private static final String SQL_UPDATE = "UPDATE users "
            + "SET usr_user_name=?, usr_login=?, usr_password=?, "
            + "usr_role_id_fk=? WHERE usr_user_id_pk=?";
    private static final String SQL_DELETE = "DELETE FROM users "
            + "WHERE usr_user_id_pk=?";
    // Database column names
    private static final String USER_ID = "usr_user_id_pk";
    private static final String USER_NAME = "usr_user_name";
    private static final String USER_LOGIN = "usr_login";
    private static final String USER_PASSWORD = "usr_password";
    private static final String USER_ROLE_ID = "usr_role_id_fk";

    @Autowired
    private DataSource dataSource;

    /*
     * (non-Javadoc)
     * @see com.epam.javalab.newsmanag.dao.GenericDao#create(java.lang.Object)
     */
    @Override
    public Long create(User newInstance) throws DaoException {
        if (newInstance == null) {
            throw new DaoException(
                    "Unable to create null User in the database");
        }
        Connection connection = DataSourceUtils.getConnection(dataSource);
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String[] key = { USER_ID };
        try {
            statement = connection.prepareStatement(SQL_CREATE, key);
            statement.setString(1, newInstance.getName());
            statement.setString(2, newInstance.getLogin());
            statement.setString(3, newInstance.getPassword());
            statement.setLong(4, newInstance.getRoleId());
            statement.executeUpdate();
            resultSet = statement.getGeneratedKeys();
            if (resultSet.next()) {
                long id = resultSet.getLong(1);
                newInstance.setId(id);
                return id;
            }
            throw new DaoException(
                    "Cannot get primary key for the inserted user");
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            DbUtil.closeAll(dataSource, connection, statement, resultSet);
        }
    }

    /*
     * (non-Javadoc)
     * @see
     * com.epam.javalab.newsmanag.dao.GenericDao#findByPk(java.io.Serializable)
     */
    @Override
    public User findByPk(Long primaryKey) throws DaoException {
        validateId(primaryKey);
        Connection connection = DataSourceUtils.getConnection(dataSource);
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        User user = null;
        try {
            statement = connection.prepareStatement(SQL_FIND_BY_PK);
            statement.setLong(1, primaryKey);
            resultSet = statement.executeQuery();
            List<User> userList = parseResultSet(resultSet);
            if (userList.size() != 1) {
                throw new DaoException(String.format(
                        "Cannot find user by ID=%d", primaryKey));
            }
            user = userList.get(0);
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            DbUtil.closeAll(dataSource, connection, statement, resultSet);
        }
        return user;
    }

    /*
     * (non-Javadoc)
     * @see com.epam.javalab.newsmanag.dao.TagDao#findByName(java.lang.String)
     */
    @Override
    public User findByLogin(String login) throws DaoException {
        if ((login == null) || (login.isEmpty())) {
            throw new DaoException("User login cannot be null or empty");
        }
        Connection connection = DataSourceUtils.getConnection(dataSource);
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        User user = null;
        try {
            statement = connection.prepareStatement(SQL_FIND_BY_LOGIN);
            statement.setString(1, login);
            resultSet = statement.executeQuery();
            List<User> userList = parseResultSet(resultSet);
            if (userList.size() != 1) {
                throw new DaoException(String.format(
                        "Cannot find user by login=%s", login));
            }
            user = userList.get(0);
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            DbUtil.closeAll(dataSource, connection, statement, resultSet);
        }
        return user;
    }

    /*
     * (non-Javadoc)
     * @see com.epam.javalab.newsmanag.dao.GenericDao#findAll()
     */
    @Override
    public List<User> findAll() throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        List<User> userList;
        try {
            statement = connection.prepareStatement(SQL_FIND_ALL);
            resultSet = statement.executeQuery();
            userList = parseResultSet(resultSet);
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            DbUtil.closeAll(dataSource, connection, statement, resultSet);
        }
        return userList;
    }

    /*
     * (non-Javadoc)
     * @see com.epam.javalab.newsmanag.dao.GenericDao#update(java.lang.Object)
     */
    @Override
    public void update(User newInstance) throws DaoException {
        if (newInstance == null) {
            throw new DaoException(
                    "Null instance cannot be recorded to the database");
        }
        Connection connection = DataSourceUtils.getConnection(dataSource);
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(SQL_UPDATE);
            statement.setString(1, newInstance.getName());
            statement.setString(2, newInstance.getLogin());
            statement.setString(3, newInstance.getPassword());
            statement.setLong(4, newInstance.getRoleId());
            statement.setLong(5, newInstance.getId());
            int count = statement.executeUpdate();
            if (count != 1) {
                throw new DaoException(
                        "On update modify more than 1 record or nothing");
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            DbUtil.closeAll(dataSource, connection, statement);
        }
    }

    /*
     * (non-Javadoc)
     * @see com.epam.javalab.newsmanag.dao.GenericDao#delete(java.lang.Object)
     */
    @Override
    public void delete(Long userId) throws DaoException {
        validateId(userId);
        Connection connection = DataSourceUtils.getConnection(dataSource);
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(SQL_DELETE);
            statement.setLong(1, userId);
            int count = statement.executeUpdate();
            if (count != 1) {
                throw new DaoException("Deleted more than 1 record or nothing");
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            DbUtil.closeAll(dataSource, connection, statement);
        }
    }

    private List<User> parseResultSet(ResultSet resultSet) throws SQLException {
        List<User> list = new ArrayList<User>();
        User user;
        while (resultSet.next()) {
            user = createUser(resultSet);
            list.add(user);
        }
        return list;
    }

    private User createUser(ResultSet resultSet) throws SQLException {
        User user = new User();
        user.setId(resultSet.getLong(USER_ID));
        user.setName(resultSet.getString(USER_NAME));
        user.setLogin(resultSet.getString(USER_LOGIN));
        user.setPassword(resultSet.getString(USER_PASSWORD));
        user.setRoleId(resultSet.getLong(USER_ROLE_ID));
        return user;
    }

    private void validateId(Long id) throws DaoException {
        if (id == null) {
            throw new DaoException("Primary key cannot be null");
        }
    }
}
