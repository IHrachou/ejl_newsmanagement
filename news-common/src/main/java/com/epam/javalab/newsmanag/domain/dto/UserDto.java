package com.epam.javalab.newsmanag.domain.dto;

import com.epam.javalab.newsmanag.domain.Role;
import com.epam.javalab.newsmanag.domain.User;

/**
 * Data Transfer Object (DTO) for the user domain.
 *
 * @author Ilya Hrachou
 * @version 1.0
 */
public class UserDto {
    private User user;
    private Role role;

    /**
     * Default constructor inherited from Object.
     */
    public UserDto() {
        super();
    }

    /**
     * Return the {@code User} instance for this DTO.
     *
     * @return the {@code User} instance
     */
    public User getUser() {
        return user;
    }

    /**
     * Set the {@code User} instance to this DTO.
     *
     * @param user
     *            the {@code User} instance
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     * Return the list of {@code Role} instances for this DTO.
     *
     * @return the list of {@code Role} instances
     */
    public Role getRole() {
        return role;
    }

    /**
     * Set the list of {@code Role} instances to this DTO.
     *
     * @param role
     *            the list of {@code Role} instances
     */
    public void setRole(Role role) {
        this.role = role;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = (prime * result) + ((role == null) ? 0 : role.hashCode());
        result = (prime * result) + ((user == null) ? 0 : user.hashCode());
        return result;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof UserDto)) {
            return false;
        }
        UserDto other = (UserDto) obj;
        if (role == null) {
            if (other.role != null) {
                return false;
            }
        } else if (!role.equals(other.role)) {
            return false;
        }
        if (user == null) {
            if (other.user != null) {
                return false;
            }
        } else if (!user.equals(other.user)) {
            return false;
        }
        return true;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return String.format("UserDto [userID=%d, roleID=%d]", user.getId(),
                role.getId());
    }
}
