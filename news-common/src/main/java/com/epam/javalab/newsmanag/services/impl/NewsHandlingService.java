package com.epam.javalab.newsmanag.services.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.epam.javalab.newsmanag.domain.Author;
import com.epam.javalab.newsmanag.domain.Comment;
import com.epam.javalab.newsmanag.domain.News;
import com.epam.javalab.newsmanag.domain.SearchCriteria;
import com.epam.javalab.newsmanag.domain.Tag;
import com.epam.javalab.newsmanag.domain.dto.NewsDto;
import com.epam.javalab.newsmanag.exceptions.ServiceException;
import com.epam.javalab.newsmanag.services.IAuthorService;
import com.epam.javalab.newsmanag.services.ICommentService;
import com.epam.javalab.newsmanag.services.INewsHandlingService;
import com.epam.javalab.newsmanag.services.INewsService;
import com.epam.javalab.newsmanag.services.ITagService;

/**
 * This class provides service methods for manipulating news with its
 * constrains. For example, this class has methods to add new news with tags and
 * authors or to delete news with all constrains and its comments. This class
 * methods use the simple service classes from current package to process theirs
 * operations.
 *
 * @author Ilya Hrachou
 * @version 1.1.7
 */
@Service
@Transactional(value = "transactionManager",
        rollbackFor = ServiceException.class)
public class NewsHandlingService implements INewsHandlingService {

    @Autowired
    private INewsService newsService;
    @Autowired
    private IAuthorService authorService;
    @Autowired
    private ICommentService commentService;
    @Autowired
    private ITagService tagService;

    /*
     * (non-Javadoc)
     * @see
     * com.epam.javalab.newsmanag.services.INewsHandlingService#addNews(com.epam
     * .javalab.newsmanag.domain.dto.NewsDto)
     */
    @Override
    @Transactional(value = "transactionManager",
            rollbackFor = ServiceException.class)
    public Long addNews(NewsDto newsDto) throws ServiceException {
        fullCheckNewsDto(newsDto);
        News news = newsDto.getNews();
        try {
            Long newsId = newsService.add(news);
            newsService.addAuthors(newsId, newsDto.getAuthorsIds());
            List<Long> tagsIds = newsDto.getTagsIds();
            if (tagsIds != null) {
                newsService.addTags(newsId, newsDto.getTagsIds());
            }
            return newsId;
        } catch (ServiceException ex) {
            throw ex;
        }
    }

    /*
     * (non-Javadoc)
     * @see
     * com.epam.javalab.newsmanag.services.INewsHandlingService#editNews(com.
     * epam.javalab.newsmanag.domain.dto.NewsDto)
     */
    @Override
    public void editNews(NewsDto newsDto) throws ServiceException {
        fullCheckNewsDto(newsDto);
        try {
            newsService.edit(newsDto.getNews());
            Long newsId = newsDto.getNews().getId();
            List<Long> newAuthorsIds = newsDto.getAuthorsIds();
            editNewsAuthors(newsId, authorService.getByNewsId(newsId),
                    newAuthorsIds);
            List<Long> newTagsIds = newsDto.getTagsIds();
            if ((newTagsIds == null) || (newTagsIds.isEmpty())) {
                newsService.deleteAllTagsFromNews(newsId);
            } else {
                editNewsTags(newsId, tagService.getByNewsId(newsId),
                        newTagsIds);
            }
        } catch (ServiceException ex) {
            throw ex;
        }
    }

    private void editNewsAuthors(Long newsId, List<Author> oldAuthors,
            List<Long> newAuthorsIds) throws ServiceException {
        List<Long> oldAuthorsIds = new ArrayList<>();
        List<Long> deleteIdsList = new ArrayList<>();
        for (Author author : oldAuthors) {
            Long oldAuthorId = author.getId();
            if (!newAuthorsIds.contains(oldAuthorId)) {
                deleteIdsList.add(oldAuthorId);
            } else {
                oldAuthorsIds.add(oldAuthorId);
            }
        }
        if (!deleteIdsList.isEmpty()) {
            newsService.deleteSeveralAuthorsFromNews(newsId, deleteIdsList);
        }
        newAuthorsIds.removeAll(oldAuthorsIds);
        if (!newAuthorsIds.isEmpty()) {
            newsService.addAuthors(newsId, newAuthorsIds);
        }
    }

    private void editNewsTags(Long newsId, List<Tag> oldTags,
            List<Long> newTagsIds) throws ServiceException {
        List<Long> oldTagsIds = new ArrayList<>();
        List<Long> deleteIdsList = new ArrayList<>();
        for (Tag oldTag : oldTags) {
            Long oldTagId = oldTag.getId();
            if (!newTagsIds.contains(oldTagId)) {
                deleteIdsList.add(oldTagId);
            } else {
                oldTagsIds.add(oldTagId);
            }
        }
        if (!deleteIdsList.isEmpty()) {
            newsService.deleteSeveralTagsFromNews(newsId, deleteIdsList);
        }
        newTagsIds.removeAll(oldTagsIds);
        if (!newTagsIds.isEmpty()) {
            newsService.addTags(newsId, newTagsIds);
        }
    }

    /*
     * (non-Javadoc)
     * @see
     * com.epam.javalab.newsmanag.services.INewsHandlingService#deleteNews(com.
     * epam.javalab.newsmanag.domain.dto.NewsDto)
     */
    @Override
    @Transactional(value = "transactionManager",
            rollbackFor = ServiceException.class)
    public void deleteNews(NewsDto newsDto) throws ServiceException {
        checkNewsDto(newsDto);
        News news = newsDto.getNews();
        Long newsId = news.getId();
        try {
            for (Author author : authorService.getByNewsId(newsId)) {
                newsService.deleteAuthorFromNews(newsId, author.getId());
            }
            for (Tag tag : tagService.getByNewsId(newsId)) {
                newsService.deleteTagFromNews(newsId, tag.getId());
            }
            for (Comment comment : commentService.getByNewsId(newsId)) {
                commentService.delete(comment.getId());
            }
            newsService.delete(newsId);
        } catch (ServiceException ex) {
            throw ex;
        }
    }

    private void checkNewsDto(NewsDto newsDto) throws ServiceException {
        if (newsDto == null) {
            throw new ServiceException("News DTO object is null");
        }
        if (newsDto.getNews() == null) {
            throw new ServiceException("News DTO object doesn't contain news");
        }
    }

    private void fullCheckNewsDto(NewsDto newsDto) throws ServiceException {
        checkNewsDto(newsDto);
        if (newsDto.getAuthors() == null) {
            throw new ServiceException(
                    "News DTO object doesn't contain authors");
        }
    }

    /*
     * (non-Javadoc)
     * @see
     * com.epam.javalab.newsmanag.services.INewsHandlingService#showNews(java.
     * lang.Long)
     */
    @Override
    public NewsDto showNews(Long newsId) throws ServiceException {
        if (newsId == null) {
            throw new ServiceException("Illegal news id");
        }
        try {
            News news = newsService.getByPk(newsId);
            return createNewsDto(news);
        } catch (ServiceException ex) {
            throw ex;
        }
    }

    /*
     * (non-Javadoc)
     * @see com.epam.javalab.newsmanag.services.INewsHandlingService#
     * showNewsListForRange(long, long)
     */
    @Override
    public List<NewsDto> showNewsListForRange(long startPos, long endPos)
            throws ServiceException {
        try {
            List<News> newsList = newsService.getAllForRange(startPos, endPos);
            List<NewsDto> newsDtoList = new ArrayList<>(newsList.size());
            for (News news : newsList) {
                NewsDto newsDto = createNewsDto(news);
                newsDtoList.add(newsDto);
            }
            return newsDtoList;
        } catch (ServiceException ex) {
            throw ex;
        }
    }

    /*
     * (non-Javadoc)
     * @see com.epam.javalab.newsmanag.services.INewsHandlingService#
     * showNewsListForRange(long, long)
     */
    @Override
    public List<NewsDto> showNewsListForRange(long startPos, long endPos,
            SearchCriteria searchCriteria) throws ServiceException {
        try {
            List<News> newsList = newsService.getAllForRange(startPos, endPos,
                    searchCriteria);
            List<NewsDto> newsDtoList = new ArrayList<>(newsList.size());
            for (News news : newsList) {
                NewsDto newsDto = createNewsDto(news);
                newsDtoList.add(newsDto);
            }
            return newsDtoList;
        } catch (ServiceException ex) {
            throw ex;
        }
    }

    private NewsDto createNewsDto(News news) throws ServiceException {
        if (news == null) {
            return null;
        }
        NewsDto newsDto = new NewsDto();
        newsDto.setNews(news);
        Long newsId = news.getId();
        newsDto.setAuthors(authorService.getByNewsId(newsId));
        newsDto.setComments(commentService.getByNewsId(newsId));
        newsDto.setTags(tagService.getByNewsId(newsId));
        return newsDto;
    }

    /*
     * (non-Javadoc)
     * @see
     * com.epam.javalab.newsmanag.services.INewsHandlingService#showPreviousNews
     * (java.lang.Long)
     */
    @Override
    public NewsDto showPreviousNews(Long currentNewsId)
            throws ServiceException {
        if (currentNewsId == null) {
            throw new ServiceException("Illegal news id");
        }
        try {
            News news = newsService.getPreviousNews(currentNewsId);
            return createNewsDto(news);
        } catch (ServiceException ex) {
            throw ex;
        }
    }

    /*
     * (non-Javadoc)
     * @see
     * com.epam.javalab.newsmanag.services.INewsHandlingService#showPreviousNews
     * (java.lang.Long, com.epam.javalab.newsmanag.domain.SearchCriteria)
     */
    @Override
    public NewsDto showPreviousNews(Long currentNewsId,
            SearchCriteria searchCriteria) throws ServiceException {
        if (currentNewsId == null) {
            throw new ServiceException("Illegal news id");
        }
        try {
            News news = newsService.getPreviousNews(currentNewsId,
                    searchCriteria);
            return createNewsDto(news);
        } catch (ServiceException ex) {
            throw ex;
        }
    }

    /*
     * (non-Javadoc)
     * @see
     * com.epam.javalab.newsmanag.services.INewsHandlingService#showNextNews(
     * java.lang.Long)
     */
    @Override
    public NewsDto showNextNews(Long currentNewsId) throws ServiceException {
        if (currentNewsId == null) {
            throw new ServiceException("Illegal news id");
        }
        try {
            News news = newsService.getNextNews(currentNewsId);
            return createNewsDto(news);
        } catch (ServiceException ex) {
            throw ex;
        }
    }

    /*
     * (non-Javadoc)
     * @see
     * com.epam.javalab.newsmanag.services.INewsHandlingService#showNextNews(
     * java.lang.Long, com.epam.javalab.newsmanag.domain.SearchCriteria)
     */
    @Override
    public NewsDto showNextNews(Long currentNewsId,
            SearchCriteria searchCriteria) throws ServiceException {
        if (currentNewsId == null) {
            throw new ServiceException("Illegal news id");
        }
        try {
            News news = newsService.getNextNews(currentNewsId, searchCriteria);
            return createNewsDto(news);
        } catch (ServiceException ex) {
            throw ex;
        }
    }
}
