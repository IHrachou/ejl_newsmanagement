package com.epam.javalab.newsmanag.domain;

import java.io.Serializable;

/**
 * This class represent Tag domain from the database.
 *
 * @author Ilya Hrachou
 * @version 1.0
 */
public class Tag implements Serializable {
    private static final long serialVersionUID = 4539146116214787073L;
    private Long id;
    private String name;

    /**
     * Default constructor for the {@code Tag}.
     */
    public Tag() {
        super();
    }

    /**
     * Return the tag id.
     *
     * @return the tag id
     */
    public Long getId() {
        return id;
    }

    /**
     * Set the tag id.
     *
     * @param id
     *            the tag id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Return the tag name.
     *
     * @return the tag name
     */
    public String getName() {
        return name;
    }

    /**
     * Set the tag name.
     *
     * @param name
     *            the tag name
     */
    public void setName(String name) {
        this.name = name;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = (prime * result) + ((id == null) ? 0 : id.hashCode());
        result = (prime * result) + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof Tag)) {
            return false;
        }
        Tag other = (Tag) obj;
        if (id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (!id.equals(other.id)) {
            return false;
        }
        if (name == null) {
            if (other.name != null) {
                return false;
            }
        } else if (!name.equals(other.name)) {
            return false;
        }
        return true;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return String.format("Tag [id=%d, name=%s]", id, name);
    }
}
