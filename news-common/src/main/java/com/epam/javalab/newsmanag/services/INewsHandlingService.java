package com.epam.javalab.newsmanag.services;

import java.util.List;

import com.epam.javalab.newsmanag.domain.SearchCriteria;
import com.epam.javalab.newsmanag.domain.dto.NewsDto;
import com.epam.javalab.newsmanag.exceptions.ServiceException;

/**
 * This interface is used in service layer. It specifies methods for handling
 * news DTO domain. It is used for mapping simple service logic to the common
 * news handling.
 *
 * @author Ilya Hrachou
 * @version 1.1.4
 */
public interface INewsHandlingService {

    /**
     * This method add new news with its authors and tags to the data storage
     * and return its ID. Tags and authors must already present in the data
     * storage.
     *
     * @param newsDto
     *            data Transfer Object (DTO) for the news domain
     * @return the ID of the added news
     * @throws ServiceException
     *             when some errors occurs or the {@code newsDto} is
     *             {@code null} or incorrect (e.g. its fields empty or
     *             {@code null})
     */
    Long addNews(NewsDto newsDto) throws ServiceException;

    /**
     * This method edit news with its authors and tags to the data storage and
     * return its ID. Tags and authors must already present in the data storage.
     *
     * @param newsDto
     *            data Transfer Object (DTO) for the news domain
     * @throws ServiceException
     *             when some errors occurs or the {@code newsDto} is
     *             {@code null} or incorrect (e.g. its fields empty or
     *             {@code null})
     * @since 1.1.4
     */
    void editNews(NewsDto newsDto) throws ServiceException;

    /**
     * Delete news with its dependencies from the data storage. This method
     * delete all comments for the specified news and all dependencies with
     * authors and tags.
     *
     * @param newsDto
     *            data Transfer Object (DTO) for the news domain
     * @throws ServiceException
     *             when some errors occurs or the {@code newsDto} is
     *             {@code null} or {@code newsDto} does not contains
     *             {@code News} instance
     */
    void deleteNews(NewsDto newsDto) throws ServiceException;

    /**
     * Return the news and all authors, tags and comments, related with it.
     *
     * @param newsId
     *            the id of news which must be returned
     * @return the news and all authors, tags and comments, related with it
     * @throws ServiceException
     *             when some errors occurs or the {@code newsId} is {@code null}
     */
    NewsDto showNews(Long newsId) throws ServiceException;

    /**
     * Return the list of news with their authors, tags and comments for the
     * specified range. It is used to show news list with pagination.
     * <p>
     * For example, if we need to get news for the 3rd page and each page
     * contains 10 news:
     * <code>List&lt;NewsDto&gt; listNews = showNewsListForRange(21, 30)</code>
     *
     * @param startPos
     *            the number of the first news inclusive
     * @param endPos
     *            the number of the last news inclusive
     * @return the list of news with their authors, tags and comments for the
     *         specified range
     * @throws ServiceException
     *             when some errors occurs
     */
    List<NewsDto> showNewsListForRange(long startPos, long endPos)
            throws ServiceException;

    /**
     * Return the list of news with their authors, tags and comments for the
     * specified range with SearchCriteria. It is used to show news list with
     * pagination.
     * <p>
     * For example, if we need to get news for the 3rd page and each page
     * contains 10 news:
     * <p>
     * <code>
     * List&lt;NewsDto&gt; listNews =
     *     showNewsListForRange(21, 30, searchCriteria)
     * </code>
     *
     * @param startPos
     *            the number of the first news inclusive
     * @param endPos
     *            the number of the last news inclusive
     * @param searchCriteria
     *            the set of criteria to find news
     * @return the list of news with their authors, tags and comments for the
     *         specified range
     * @throws ServiceException
     *             when some errors occurs
     * @since 1.0.5
     */
    List<NewsDto> showNewsListForRange(long startPos, long endPos,
            SearchCriteria searchCriteria) throws ServiceException;

    /**
     * Return the previous news for the specified current news ID with all of
     * its authors, tags and comments.
     *
     * @param currentNewsId
     *            the ID of the current news
     * @return the previous news for the specified ID with all of its authors,
     *         tags and comments
     * @throws ServiceException
     *             when some errors occurs or the {@code newsId} is {@code null}
     * @since 1.0.3
     */
    NewsDto showPreviousNews(Long currentNewsId) throws ServiceException;

    /**
     * Return the previous news for the specified current news ID with all of
     * its authors, tags and comments. This method returns the full news from
     * the filtered list of news. The list is filtered by search criteria.
     *
     * @param currentNewsId
     *            the ID of the current news
     * @param searchCriteria
     *            the set of criteria to find news
     * @return the previous news for the specified ID with all of its authors,
     *         tags and comments
     * @throws ServiceException
     *             when some errors occurs or the {@code newsId} is {@code null}
     * @since 1.0.6
     */
    NewsDto showPreviousNews(Long currentNewsId, SearchCriteria searchCriteria)
            throws ServiceException;

    /**
     * Return the next news for the specified current news ID with all of its
     * authors, tags and comments.
     *
     * @param currentNewsId
     *            the ID of the current news
     * @return the next news for the specified ID with all of its authors, tags
     *         and comments
     * @throws ServiceException
     *             when some errors occurs or the {@code newsId} is {@code null}
     * @since 1.0.3
     */
    NewsDto showNextNews(Long currentNewsId) throws ServiceException;

    /**
     * Return the next news for the specified current news ID with all of its
     * authors, tags and comments. This method returns the full news from the
     * filtered list of news. The list is filtered by search criteria.
     *
     * @param currentNewsId
     *            the ID of the current news
     * @param searchCriteria
     *            the set of criteria to find news
     * @return the next news for the specified ID with all of its authors, tags
     *         and comments
     * @throws ServiceException
     *             when some errors occurs or the {@code newsId} is {@code null}
     * @since 1.0.6
     */
    NewsDto showNextNews(Long currentNewsId, SearchCriteria searchCriteria)
            throws ServiceException;
}
