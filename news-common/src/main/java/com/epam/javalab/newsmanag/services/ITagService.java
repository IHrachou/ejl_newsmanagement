package com.epam.javalab.newsmanag.services;

import java.util.List;

import com.epam.javalab.newsmanag.domain.Tag;
import com.epam.javalab.newsmanag.exceptions.ServiceException;

/**
 * This interface is used in service layer. It extends {@code IGenericService}
 * interface and specifies additional methods for handling {@code Tag} domain.
 * It is used for mapping DAO logic to the service layer.
 *
 * @author Ilya Hrachou
 * @version 1.0
 */
public interface ITagService extends IGenericService<Tag, Long> {
    /**
     * Call the DAO object to get the list of tags for the specified news.
     *
     * @param newsId
     *            the news ID which list of tags must be returned
     * @return the list of tags for the specified news
     * @throws ServiceException
     *             when some errors occurs in the DAO layer or the
     *             {@code newsId} is {@code null}
     */
    List<Tag> getByNewsId(Long newsId) throws ServiceException;

    /**
     * Call the DAO object to get the list of tags for the specified set of ids.
     * Unavailable ids will be skipped.
     *
     * @param tagIds
     *            the list of ids
     * @return the list of tags for the specified set of ids
     * @throws ServiceException
     *             when some errors occurs in the DAO layer or the
     *             {@code tagIds} is {@code null}
     * @since 1.0.4
     */
    List<Tag> getListByIds(Long[] tagIds) throws ServiceException;
}
