package com.epam.javalab.newsmanag.exceptions;

import java.sql.SQLException;

/**
 * The checked exception that can appear while getting access to the data source
 * or handling information in it.
 *
 * @author Ilya Hrachou
 * @version 1.0
 * @see java.lang.Exception
 */
public class DaoException extends SQLException {
    private static final long serialVersionUID = -3018364731573531906L;

    /**
     * Constructs a new {@code DaoException} with null as it's detail message.
     */
    public DaoException() {
        super();
    }

    /**
     * Construct a new {@code DaoException} with specified detail message and
     * root cause.
     *
     * @param message
     *            the detail message. The detail message is saved for later
     *            retrieval by the {@link #getMessage()} method.
     * @param cause
     *            the cause (which is saved for later retrieval by the
     *            {@link #getCause()} method). (A &lt;tt&gt;null&lt;/tt&gt;
     *            value is permitted, and indicates that the cause is
     *            nonexistent or unknown.)
     */
    public DaoException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * Construct a new {@code DaoException} with specified detail message. The
     * cause is not initialized, and may subsequently be initialized by a call
     * to {@link #initCause}.
     *
     * @param message
     *            the detail message. The detail message is saved for later
     *            retrieval by the {@link #getMessage()} method.
     */
    public DaoException(String message) {
        super(message);
    }

    /**
     * Construct a new {@code DaoException} with the specified cause and a
     * detail message of &lt;tt&gt;(cause==null ? null :
     * cause.toString())&lt;/tt&gt; (which typically contains the class and
     * detail message of &lt;tt&gt;cause&lt;/tt&gt;).
     *
     * @param cause
     *            the cause (which is saved for later retrieval by the
     *            {@link #getCause()} method). (A &lt;tt&gt;null&lt;/tt&gt;
     *            value is permitted, and indicates that the cause is
     *            nonexistent or unknown.)
     */
    public DaoException(Throwable cause) {
        super(cause);
    }
}
