/**
 * Provides classes and interfaces which are used for all operations (including
 * C.R.U.D. operations) with the data storage. All those classes and interfaces
 * represent the DAO layer of the application.
 *
 * @author Ilya Hrachou
 * @version 1.0
 */
package com.epam.javalab.newsmanag.dao;
