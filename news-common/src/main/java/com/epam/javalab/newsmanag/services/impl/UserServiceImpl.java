package com.epam.javalab.newsmanag.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.epam.javalab.newsmanag.dao.UserDao;
import com.epam.javalab.newsmanag.domain.User;
import com.epam.javalab.newsmanag.exceptions.DaoException;
import com.epam.javalab.newsmanag.exceptions.ServiceException;
import com.epam.javalab.newsmanag.services.IUserService;

/**
 * Service class for handling {@code User}. This class provides the access to
 * the DAO layer.
 *
 * @author Ilya Hrachou
 * @version 1.1.7
 */
@Service
@Transactional(value = "transactionManager",
        rollbackFor = ServiceException.class)
public class UserServiceImpl implements IUserService {

    @Autowired
    private UserDao userDao;

    /*
     * (non-Javadoc)
     * @see
     * com.epam.javalab.newsmanag.services.IGenericService#add(java.lang.Object)
     */
    @Override
    public Long add(User user) throws ServiceException {
        try {
            return userDao.create(user);
        } catch (DaoException ex) {
            throw new ServiceException(ex);
        }
    }

    /*
     * (non-Javadoc)
     * @see com.epam.javalab.newsmanag.services.IGenericService#getByPk(java.io.
     * Serializable)
     */
    @Override
    public User getByPk(Long userId) throws ServiceException {
        try {
            return userDao.findByPk(userId);
        } catch (DaoException ex) {
            throw new ServiceException(ex);
        }
    }

    /*
     * (non-Javadoc)
     * @see
     * com.epam.javalab.newsmanag.services.IUserService#getByLogin(java.lang.
     * String)
     */
    @Override
    public User getByLogin(String login) throws ServiceException {
        try {
            return userDao.findByLogin(login);
        } catch (DaoException ex) {
            throw new ServiceException(ex);
        }
    }

    /*
     * (non-Javadoc)
     * @see com.epam.javalab.newsmanag.services.IGenericService#getAll()
     */
    @Override
    public List<User> getAll() throws ServiceException {
        try {
            return userDao.findAll();
        } catch (DaoException ex) {
            throw new ServiceException(ex);
        }
    }

    /*
     * (non-Javadoc)
     * @see com.epam.javalab.newsmanag.services.IGenericService#edit(java.lang.
     * Object)
     */
    @Override
    public void edit(User user) throws ServiceException {
        try {
            userDao.update(user);
        } catch (DaoException ex) {
            throw new ServiceException(ex);
        }
    }

    /*
     * (non-Javadoc)
     * @see
     * com.epam.javalab.newsmanag.services.IGenericService#delete(java.lang.
     * Long)
     */
    @Override
    public void delete(Long userId) throws ServiceException {
        try {
            userDao.delete(userId);
        } catch (DaoException ex) {
            throw new ServiceException(ex);
        }
    }
}
