package com.epam.javalab.newsmanag.dao.orclimpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;

import com.epam.javalab.newsmanag.dao.TagDao;
import com.epam.javalab.newsmanag.dao.utils.DbUtil;
import com.epam.javalab.newsmanag.domain.Tag;
import com.epam.javalab.newsmanag.exceptions.DaoException;

/**
 * Implementation of the DAO interface for the {@code Tag} domain.
 *
 * @author Ilya Hrachou
 * @version 1.0
 */
@Repository()
public class TagDaoImpl implements TagDao {
    // SQL queries
    private static final String SQL_CREATE = "INSERT INTO tags (tg_tag_name) "
            + "values (?)";
    private static final String SQL_FIND_BY_PK = "SELECT tg_tag_id_pk, "
            + "tg_tag_name FROM tags WHERE tg_tag_id_pk=?";
    private static final String SQL_FIND_BY_NAME = "SELECT tg_tag_id_pk, "
            + "tg_tag_name FROM tags WHERE tg_tag_name=?";
    private static final String SQL_FIND_ALL = "SELECT tg_tag_id_pk, "
            + "tg_tag_name FROM tags";
    private static final String SQL_FIND_BY_NEWSID = "SELECT tg_tag_id_pk, "
            + "tg_tag_name FROM tags "
            + "JOIN news_tags ON tg_tag_id_pk=nt_tag_id_fk "
            + "WHERE nt_news_id_fk=?";
    private static final String SQL_UPDATE = "UPDATE tags SET tg_tag_name=? "
            + "WHERE tg_tag_id_pk=?";
    private static final String SQL_DELETE = "DELETE FROM tags "
            + "WHERE tg_tag_id_pk=?";
    private static final String SQL_FIND_LIST_BY_IDS = "SELECT tg_tag_id_pk, "
            + "tg_tag_name FROM tags WHERE tg_tag_id_pk IN(%s)";
    // Database column names
    private static final String TAG_ID = "tg_tag_id_pk";
    private static final String TAG_NAME = "tg_tag_name";

    @Autowired
    private DataSource dataSource;

    /*
     * (non-Javadoc)
     * @see com.epam.javalab.newsmanag.dao.GenericDao#create(java.lang.Object)
     */
    @Override
    public Long create(Tag newInstance) throws DaoException {
        if (newInstance == null) {
            throw new DaoException("Unable to create null Tag in the database");
        }
        Connection connection = DataSourceUtils.getConnection(dataSource);
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String[] key = { TAG_ID };
        try {
            statement = connection.prepareStatement(SQL_CREATE, key);
            statement.setString(1, newInstance.getName());
            statement.executeUpdate();
            resultSet = statement.getGeneratedKeys();
            if (resultSet.next()) {
                long id = resultSet.getLong(1);
                newInstance.setId(id);
                return id;
            }
            throw new DaoException(
                    "Cannot get primary key for the inserted tag");
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            DbUtil.closeAll(dataSource, connection, statement, resultSet);
        }
    }

    /*
     * (non-Javadoc)
     * @see
     * com.epam.javalab.newsmanag.dao.GenericDao#findByPk(java.io.Serializable)
     */
    @Override
    public Tag findByPk(Long primaryKey) throws DaoException {
        validateId(primaryKey);
        Connection connection = DataSourceUtils.getConnection(dataSource);
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        Tag tag = null;
        try {
            statement = connection.prepareStatement(SQL_FIND_BY_PK);
            statement.setLong(1, primaryKey);
            resultSet = statement.executeQuery();
            List<Tag> tagList = parseResultSet(resultSet);
            if (tagList.size() != 1) {
                throw new DaoException(String.format("Cannot find tag by ID=%d",
                        primaryKey));
            }
            tag = tagList.get(0);
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            DbUtil.closeAll(dataSource, connection, statement, resultSet);
        }
        return tag;
    }

    /*
     * (non-Javadoc)
     * @see com.epam.javalab.newsmanag.dao.TagDao#findByName(java.lang.String)
     */
    @Override
    public Tag findByName(String name) throws DaoException {
        if (name == null) {
            throw new DaoException("Tag name cannot be null");
        }
        Connection connection = DataSourceUtils.getConnection(dataSource);
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        Tag tag = null;
        try {
            statement = connection.prepareStatement(SQL_FIND_BY_NAME);
            statement.setString(1, name);
            resultSet = statement.executeQuery();
            List<Tag> tagList = parseResultSet(resultSet);
            if (tagList.size() != 1) {
                throw new DaoException(String.format(
                        "Cannot find tag by name=%s", name));
            }
            tag = tagList.get(0);
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            DbUtil.closeAll(dataSource, connection, statement, resultSet);
        }
        return tag;
    }

    /*
     * (non-Javadoc)
     * @see com.epam.javalab.newsmanag.dao.GenericDao#findAll()
     */
    @Override
    public List<Tag> findAll() throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        List<Tag> tagList;
        try {
            statement = connection.prepareStatement(SQL_FIND_ALL);
            resultSet = statement.executeQuery();
            tagList = parseResultSet(resultSet);
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            DbUtil.closeAll(dataSource, connection, statement, resultSet);
        }
        return tagList;
    }

    /*
     * (non-Javadoc)
     * @see com.epam.javalab.newsmanag.dao.GenericDao#update(java.lang.Object)
     */
    @Override
    public void update(Tag newInstance) throws DaoException {
        if (newInstance == null) {
            throw new DaoException(
                    "Null instance cannot be recorded to the database");
        }
        Connection connection = DataSourceUtils.getConnection(dataSource);
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(SQL_UPDATE);
            statement.setString(1, newInstance.getName());
            statement.setLong(2, newInstance.getId());
            int count = statement.executeUpdate();
            if (count != 1) {
                throw new DaoException(
                        "On update modify more than 1 record or nothing");
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            DbUtil.closeAll(dataSource, connection, statement);
        }
    }

    /*
     * (non-Javadoc)
     * @see com.epam.javalab.newsmanag.dao.GenericDao#delete(java.lang.Long)
     */
    @Override
    public void delete(Long tagId) throws DaoException {
        validateId(tagId);
        Connection connection = DataSourceUtils.getConnection(dataSource);
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(SQL_DELETE);
            statement.setLong(1, tagId);
            int count = statement.executeUpdate();
            if (count != 1) {
                throw new DaoException("Deleted more than 1 record or nothing");
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            DbUtil.closeAll(dataSource, connection, statement);
        }
    }

    /*
     * (non-Javadoc)
     * @see com.epam.javalab.newsmanag.dao.TagDao#findByNewsId(java.lang.Long)
     */
    @Override
    public List<Tag> findByNewsId(Long newsId) throws DaoException {
        validateId(newsId);
        Connection connection = DataSourceUtils.getConnection(dataSource);
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        List<Tag> tags;
        try {
            statement = connection.prepareStatement(SQL_FIND_BY_NEWSID);
            statement.setLong(1, newsId);
            resultSet = statement.executeQuery();
            tags = parseResultSet(resultSet);
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            DbUtil.closeAll(dataSource, connection, statement, resultSet);
        }
        return tags;
    }

    private List<Tag> parseResultSet(ResultSet resultSet) throws SQLException {
        List<Tag> list = new ArrayList<Tag>();
        Tag tag;
        while (resultSet.next()) {
            tag = createTag(resultSet);
            list.add(tag);
        }
        return list;
    }

    private Tag createTag(ResultSet resultSet) throws SQLException {
        Tag tag = new Tag();
        tag.setId(resultSet.getLong(TAG_ID));
        tag.setName(resultSet.getString(TAG_NAME));
        return tag;
    }

    private void validateId(Long id) throws DaoException {
        if (id == null) {
            throw new DaoException("Primary key cannot be null");
        }
    }

    /*
     * (non-Javadoc)
     * @see
     * com.epam.javalab.newsmanag.dao.TagDao#findListByIds(java.lang.Long[])
     */
    @Override
    public List<Tag> findListByIds(Long[] tagIds) throws DaoException {
        if (tagIds == null) {
            throw new DaoException("Input array cannot be null");
        }
        Connection connection = DataSourceUtils.getConnection(dataSource);
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        List<Tag> tagList;
        try {
            statement = connection.prepareStatement(
                    prepareQueryForFindListByIds(tagIds.length));
            for (int i = 0; i < tagIds.length; i++) {
                statement.setLong(i + 1, tagIds[i]);
            }
            resultSet = statement.executeQuery();
            tagList = parseResultSet(resultSet);
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            DbUtil.closeAll(dataSource, connection, statement, resultSet);
        }
        return tagList;
    }

    private String prepareQueryForFindListByIds(int amountOfparams) {
        StringBuilder queryParams = new StringBuilder();
        for (int i = 0; i < amountOfparams; i++) {
            queryParams.append("?,");
        }
        queryParams.deleteCharAt(queryParams.length() - 1);
        return String.format(SQL_FIND_LIST_BY_IDS, queryParams);
    }
}
