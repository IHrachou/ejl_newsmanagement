/**
 * Provides classes and interfaces which are used for all operations (including
 * C.R.U.D. operations) with the DAO layer. All those classes and interfaces
 * represent the service layer of the application.
 *
 * @author Ilya Hrachou
 * @version 1.0
 */
package com.epam.javalab.newsmanag.services;
