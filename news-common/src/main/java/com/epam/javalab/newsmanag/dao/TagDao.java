package com.epam.javalab.newsmanag.dao;

import java.util.List;

import com.epam.javalab.newsmanag.domain.Tag;
import com.epam.javalab.newsmanag.exceptions.DaoException;

/**
 * This interface specifies the contract for the DAO operations with the
 * {@code Tag} domain. It extends the {@code GenericDao} interface. This
 * interface provides the specific methods for the DAO classes for the
 * {@code Tag} domain.
 *
 * @author Ilya Hrachou
 * @version 1.0
 */
public interface TagDao extends GenericDao<Tag, Long> {
    /**
     * Return the tag from the database specified by the name.
     *
     * @param name
     *            the name of the required tag
     * @return the tag from the database
     * @throws DaoException
     *             when some errors occurs during the connection to the database
     *             or executing the query; this exception is also thrown when
     *             the {@code name} is {@code null}
     */
    Tag findByName(String name) throws DaoException;

    /**
     * Return the {@code List} of all tags for the specified news from the
     * database.
     *
     * @param newsId
     *            the news ID which list of tags must be returned
     * @return the {@code List} of all tags for the specified news
     * @throws DaoException
     *             when some errors occurs during the connection to the database
     *             or executing the query; this exception is also thrown when
     *             the {@code newsId} is {@code null}
     */
    List<Tag> findByNewsId(Long newsId) throws DaoException;

    /**
     * Return the {@code List} of tags for the specified list of ids. If the
     * input list of tags ids contains values that are absent in the database,
     * those values will be skipped.
     *
     * @param tagIds
     *            the array of tags ids for search
     * @return the {@code List} of tags for the specified list of ids
     * @throws DaoException
     *             when some errors occurs during the connection to the database
     *             or executing the query; this exception is also thrown when
     *             the {@code tagIds} is {@code null}
     * @since 1.0.4
     */
    List<Tag> findListByIds(Long[] tagIds) throws DaoException;
}
