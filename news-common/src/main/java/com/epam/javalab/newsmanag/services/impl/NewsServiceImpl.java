package com.epam.javalab.newsmanag.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.epam.javalab.newsmanag.dao.NewsDao;
import com.epam.javalab.newsmanag.domain.News;
import com.epam.javalab.newsmanag.domain.SearchCriteria;
import com.epam.javalab.newsmanag.exceptions.DaoException;
import com.epam.javalab.newsmanag.exceptions.ServiceException;
import com.epam.javalab.newsmanag.services.INewsService;

/**
 * Service class for handling {@code News}. This class provides the access to
 * the DAO layer.
 *
 * @author Ilya Hrachou
 * @version 1.1.7
 */
@Service
@Transactional(value = "transactionManager",
        rollbackFor = ServiceException.class)
public class NewsServiceImpl implements INewsService {

    @Autowired
    private NewsDao newsDao;

    /*
     * (non-Javadoc)
     * @see
     * com.epam.javalab.newsmanag.services.IGenericService#add(java.lang.Object)
     */
    @Override
    public Long add(News news) throws ServiceException {
        try {
            return newsDao.create(news);
        } catch (DaoException ex) {
            throw new ServiceException(ex);
        }
    }

    /*
     * (non-Javadoc)
     * @see com.epam.javalab.newsmanag.services.IGenericService#getByPk(java.io.
     * Serializable)
     */
    @Override
    public News getByPk(Long newsId) throws ServiceException {
        try {
            return newsDao.findByPk(newsId);
        } catch (DaoException ex) {
            throw new ServiceException(ex);
        }
    }

    /*
     * (non-Javadoc)
     * @see com.epam.javalab.newsmanag.services.IGenericService#getAll()
     */
    @Override
    public List<News> getAll() throws ServiceException {
        try {
            return newsDao.findAll();
        } catch (DaoException ex) {
            throw new ServiceException(ex);
        }
    }

    /*
     * (non-Javadoc)
     * @see
     * com.epam.javalab.newsmanag.services.INewsService#getAllForRange(long,
     * long)
     */
    @Override
    public List<News> getAllForRange(long startPos, long endPos)
            throws ServiceException {
        checkRange(startPos, endPos);
        try {
            return newsDao.findForRange(startPos, endPos);
        } catch (DaoException ex) {
            throw new ServiceException(ex);
        }
    }

    /*
     * (non-Javadoc)
     * @see
     * com.epam.javalab.newsmanag.services.INewsService#getAllForRange(long,
     * long)
     */
    @Override
    public List<News> getAllForRange(long startPos, long endPos,
            SearchCriteria searchCriteria) throws ServiceException {
        checkRange(startPos, endPos);
        try {
            return newsDao.findForRange(startPos, endPos, searchCriteria);
        } catch (DaoException ex) {
            throw new ServiceException(ex);
        }
    }

    private void checkRange(long start, long end) throws ServiceException {
        if ((start < 0) || (end < 0) || (start > end)) {
            throw new ServiceException("Illegal range");
        }
    }

    /*
     * (non-Javadoc)
     * @see com.epam.javalab.newsmanag.services.IGenericService#edit(java.lang.
     * Object)
     */
    @Override
    public void edit(News news) throws ServiceException {
        try {
            newsDao.update(news);
        } catch (DaoException ex) {
            throw new ServiceException(ex);
        }
    }

    /*
     * (non-Javadoc)
     * @see
     * com.epam.javalab.newsmanag.services.IGenericService#delete(java.lang.
     * Long)
     */
    @Override
    public void delete(Long newsId) throws ServiceException {
        try {
            newsDao.delete(newsId);
        } catch (DaoException ex) {
            throw new ServiceException(ex);
        }
    }

    /*
     * (non-Javadoc)
     * @see
     * com.epam.javalab.newsmanag.services.IGenericService#delete(java.lang.
     * Long)
     */
    @Override
    public void deleteSeveral(Long... newsIds) throws ServiceException {
        try {
            newsDao.deleteNewsList(newsIds);
        } catch (DaoException ex) {
            throw new ServiceException(ex);
        }
    }

    /*
     * (non-Javadoc)
     * @see
     * com.epam.javalab.newsmanag.services.INewsService#searchNews(com.epam.
     * javalab.newsmanag.domain.SearchCriteria)
     */
    @Override
    public List<News> searchNews(SearchCriteria searchCriteria)
            throws ServiceException {
        try {
            return newsDao.findByCriteria(searchCriteria);
        } catch (DaoException ex) {
            throw new ServiceException(ex);
        }
    }

    /*
     * (non-Javadoc)
     * @see com.epam.javalab.newsmanag.services.INewsService#countAllNews()
     */
    @Override
    public Long countAllNews() throws ServiceException {
        try {
            return newsDao.countNews();
        } catch (DaoException ex) {
            throw new ServiceException(ex);
        }
    }

    /*
     * (non-Javadoc)
     * @see
     * com.epam.javalab.newsmanag.services.INewsService#countFilteredNews(com.
     * epam.javalab.newsmanag.domain.SearchCriteria)
     */
    @Override
    public Long countFilteredNews(SearchCriteria searchCriteria)
            throws ServiceException {
        try {
            return newsDao.countFilteredNews(searchCriteria);
        } catch (DaoException ex) {
            throw new ServiceException(ex);
        }
    }

    /*
     * (non-Javadoc)
     * @see
     * com.epam.javalab.newsmanag.services.INewsService#addAuthor(java.lang.
     * Long, java.lang.Long)
     */
    @Override
    public void addAuthors(Long newsId, List<Long> authorIds)
            throws ServiceException {
        try {
            newsDao.addAuthors(newsId, authorIds);
        } catch (DaoException ex) {
            throw new ServiceException(ex);
        }
    }

    /*
     * (non-Javadoc)
     * @see
     * com.epam.javalab.newsmanag.services.INewsService#deleteAuthorFromNews(
     * java.lang.Long, java.lang.Long)
     */
    @Override
    public void deleteAuthorFromNews(Long newsId, Long authorId)
            throws ServiceException {
        try {
            newsDao.deleteAuthorFromNews(newsId, authorId);
        } catch (DaoException ex) {
            throw new ServiceException(ex);
        }
    }

    /*
     * (non-Javadoc)
     * @see com.epam.javalab.newsmanag.services.INewsService#
     * deleteSeveralAuthorsFromNews(java.lang.Long, java.util.List)
     */
    @Override
    public void deleteSeveralAuthorsFromNews(Long newsId, List<Long> authorsIds)
            throws ServiceException {
        try {
            newsDao.deleteSeveralAuthorsFromNews(newsId, authorsIds);
        } catch (DaoException ex) {
            throw new ServiceException(ex);
        }
    }

    /*
     * (non-Javadoc)
     * @see
     * com.epam.javalab.newsmanag.services.INewsService#addTag(java.lang.Long,
     * java.lang.Long)
     */
    @Override
    public void addTags(Long newsId, List<Long> tagIds)
            throws ServiceException {
        try {
            newsDao.addTags(newsId, tagIds);
        } catch (DaoException ex) {
            throw new ServiceException(ex);
        }
    }

    /*
     * (non-Javadoc)
     * @see
     * com.epam.javalab.newsmanag.services.INewsService#deleteTagFromNews(java.
     * lang.Long, java.lang.Long)
     */
    @Override
    public void deleteTagFromNews(Long newsId, Long tagId)
            throws ServiceException {
        try {
            newsDao.deleteTagFromNews(newsId, tagId);
        } catch (DaoException ex) {
            throw new ServiceException(ex);
        }
    }

    /*
     * (non-Javadoc)
     * @see com.epam.javalab.newsmanag.services.INewsService#
     * deleteSeveralTagsFromNews(java.lang.Long, java.util.List)
     */
    @Override
    public void deleteSeveralTagsFromNews(Long newsId, List<Long> tagsIds)
            throws ServiceException {
        try {
            newsDao.deleteSeveralTagsFromNews(newsId, tagsIds);
        } catch (DaoException ex) {
            throw new ServiceException(ex);
        }
    }

    /*
     * (non-Javadoc)
     * @see
     * com.epam.javalab.newsmanag.services.INewsService#deleteAllTagsFromNews(
     * java.lang.Long)
     */
    @Override
    public void deleteAllTagsFromNews(Long newsId) throws ServiceException {
        try {
            newsDao.deleteAllTagsFromNews(newsId);
        } catch (DaoException ex) {
            throw new ServiceException(ex);
        }
    }

    /*
     * (non-Javadoc)
     * @see
     * com.epam.javalab.newsmanag.services.INewsService#getPreviousNews(java.
     * lang.Long)
     */
    @Override
    public News getPreviousNews(Long currentNewsId) throws ServiceException {
        try {
            return newsDao.findPreviousNews(currentNewsId);
        } catch (DaoException ex) {
            throw new ServiceException(ex);
        }
    }

    /*
     * (non-Javadoc)
     * @see
     * com.epam.javalab.newsmanag.services.INewsService#getPreviousNews(java.
     * lang.Long, com.epam.javalab.newsmanag.domain.SearchCriteria)
     */
    @Override
    public News getPreviousNews(Long currentNewsId,
            SearchCriteria searchCriteria) throws ServiceException {
        try {
            return newsDao.findPreviousNews(currentNewsId, searchCriteria);
        } catch (DaoException ex) {
            throw new ServiceException(ex);
        }
    }

    /*
     * (non-Javadoc)
     * @see
     * com.epam.javalab.newsmanag.services.INewsService#getNextNews(java.lang.
     * Long)
     */
    @Override
    public News getNextNews(Long currentNewsId) throws ServiceException {
        try {
            return newsDao.findNextNews(currentNewsId);
        } catch (DaoException ex) {
            throw new ServiceException(ex);
        }
    }

    /*
     * (non-Javadoc)
     * @see
     * com.epam.javalab.newsmanag.services.INewsService#getNextNews(java.lang.
     * Long, com.epam.javalab.newsmanag.domain.SearchCriteria)
     */
    @Override
    public News getNextNews(Long currentNewsId, SearchCriteria searchCriteria)
            throws ServiceException {
        try {
            return newsDao.findNextNews(currentNewsId, searchCriteria);
        } catch (DaoException ex) {
            throw new ServiceException(ex);
        }
    }

    /*
     * (non-Javadoc)
     * @see
     * com.epam.javalab.newsmanag.services.INewsService#getNextAndPrevNewsId(
     * java.lang.Long, com.epam.javalab.newsmanag.domain.SearchCriteria)
     */
    @Override
    public Long[] getNextAndPrevNewsId(Long currentNewsId,
            SearchCriteria searchCriteria) throws ServiceException {
        try {
            return newsDao.findPrevAndNextNewsId(currentNewsId, searchCriteria);
        } catch (DaoException ex) {
            throw new ServiceException(ex);
        }
    }
}
