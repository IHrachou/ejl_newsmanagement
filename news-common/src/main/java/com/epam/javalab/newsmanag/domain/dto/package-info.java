/**
 * This package contains Data Transfer Objects. This objects are used to
 * transfer data between controller and service layer.
 *
 * @author Ilya Hrachou
 * @version 1.0
 */
package com.epam.javalab.newsmanag.domain.dto;
