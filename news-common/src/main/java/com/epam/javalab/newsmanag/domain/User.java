package com.epam.javalab.newsmanag.domain;

import java.io.Serializable;

/**
 * This class represent User domain from the database.
 *
 * @author Ilya Hrachou
 * @version 1.0
 */
public class User implements Serializable {
    private static final long serialVersionUID = 4797854740016004549L;
    private Long id;
    private String name;
    private String login;
    private String password;
    private Long roleId;

    /**
     * Default constructor for the {@code User}.
     */
    public User() {
        super();
    }

    /**
     * Return the user's id.
     *
     * @return the user's id
     */
    public Long getId() {
        return id;
    }

    /**
     * Set the user's id.
     *
     * @param id
     *            the user's id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Return the user's name.
     *
     * @return the user's name
     */
    public String getName() {
        return name;
    }

    /**
     * Set the user's name.
     *
     * @param name
     *            the user's name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Return the user's login.
     *
     * @return the user's login
     */
    public String getLogin() {
        return login;
    }

    /**
     * Set the user's login.
     *
     * @param login
     *            the user's login
     */
    public void setLogin(String login) {
        this.login = login;
    }

    /**
     * Return the user's password.
     *
     * @return the user's password
     */
    public String getPassword() {
        return password;
    }

    /**
     * Set the user's password.
     *
     * @param password
     *            the user's password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Return the user's role ID.
     *
     * @return the user's role ID
     */
    public Long getRoleId() {
        return roleId;
    }

    /**
     * Set the user's role ID.
     *
     * @param roleId
     *            the user's role ID
     */
    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = (prime * result) + ((id == null) ? 0 : id.hashCode());
        result = (prime * result) + ((login == null) ? 0 : login.hashCode());
        result = (prime * result) + ((name == null) ? 0 : name.hashCode());
        result = (prime * result) + ((password == null) ? 0
                : password.hashCode());
        result = (prime * result) + ((roleId == null) ? 0 : roleId.hashCode());
        return result;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof User)) {
            return false;
        }
        User other = (User) obj;
        if (id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (!id.equals(other.id)) {
            return false;
        }
        if (login == null) {
            if (other.login != null) {
                return false;
            }
        } else if (!login.equals(other.login)) {
            return false;
        }
        if (name == null) {
            if (other.name != null) {
                return false;
            }
        } else if (!name.equals(other.name)) {
            return false;
        }
        if (password == null) {
            if (other.password != null) {
                return false;
            }
        } else if (!password.equals(other.password)) {
            return false;
        }
        if (roleId == null) {
            if (other.roleId != null) {
                return false;
            }
        } else if (!roleId.equals(other.roleId)) {
            return false;
        }
        return true;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return String.format(
                "User [id=%d, name=%s, login=%s, password=%s, roleID=%d]", id,
                name, login, password, roleId);
    }
}
