package com.epam.javalab.newsmanag.dao.orclimpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;

import com.epam.javalab.newsmanag.dao.RoleDao;
import com.epam.javalab.newsmanag.dao.utils.DbUtil;
import com.epam.javalab.newsmanag.domain.Role;
import com.epam.javalab.newsmanag.exceptions.DaoException;

/**
 * Implementation of the DAO interface for the {@code Role} domain.
 *
 * @author Ilya Hrachou
 * @version 1.0
 */
@Repository
public class RoleDaoImpl implements RoleDao {
    // SQL queries
    private static final String SQL_CREATE = "INSERT INTO roles (ro_role_name) "
            + "values (?)";
    private static final String SQL_FIND_BY_PK = "SELECT ro_role_id_pk, "
            + "ro_role_name FROM roles WHERE ro_role_id_pk=?";
    private static final String SQL_FIND_BY_NAME = "SELECT ro_role_id_pk, "
            + "ro_role_name FROM roles WHERE ro_role_name=?";
    private static final String SQL_FIND_ALL = "SELECT ro_role_id_pk, "
            + "ro_role_name FROM roles";
    private static final String SQL_UPDATE = "UPDATE roles SET ro_role_name=? "
            + "WHERE ro_role_id_pk=?";
    private static final String SQL_DELETE = "DELETE FROM roles "
            + "WHERE ro_role_id_pk=?";
    // Database column names
    private static final String ROLE_ID = "ro_role_id_pk";
    private static final String ROLE_NAME = "ro_role_name";

    @Autowired
    private DataSource dataSource;

    /*
     * (non-Javadoc)
     * @see com.epam.javalab.newsmanag.dao.GenericDao#create(java.lang.Object)
     */
    @Override
    public Long create(Role newInstance) throws DaoException {
        if (newInstance == null) {
            throw new DaoException(
                    "Unable to create null Role in the database");
        }
        Connection connection = DataSourceUtils.getConnection(dataSource);
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String[] key = { ROLE_ID };
        try {
            statement = connection.prepareStatement(SQL_CREATE, key);
            statement.setString(1, newInstance.getName());
            statement.executeUpdate();
            resultSet = statement.getGeneratedKeys();
            if (resultSet.next()) {
                long id = resultSet.getLong(1);
                newInstance.setId(id);
                return id;
            }
            throw new DaoException(
                    "Cannot get primary key for the inserted tag");
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            DbUtil.closeAll(dataSource, connection, statement, resultSet);
        }
    }

    /*
     * (non-Javadoc)
     * @see
     * com.epam.javalab.newsmanag.dao.GenericDao#findByPk(java.io.Serializable)
     */
    @Override
    public Role findByPk(Long primaryKey) throws DaoException {
        validateId(primaryKey);
        Connection connection = DataSourceUtils.getConnection(dataSource);
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        Role role = null;
        try {
            statement = connection.prepareStatement(SQL_FIND_BY_PK);
            statement.setLong(1, primaryKey);
            resultSet = statement.executeQuery();
            List<Role> roleList = parseResultSet(resultSet);
            if (roleList.size() != 1) {
                throw new DaoException(String.format(
                        "Cannot find role by ID=%d", primaryKey));
            }
            role = roleList.get(0);
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            DbUtil.closeAll(dataSource, connection, statement, resultSet);
        }
        return role;
    }

    /*
     * (non-Javadoc)
     * @see com.epam.javalab.newsmanag.dao.RoleDao#findByName(java.lang.String)
     */
    @Override
    public List<Role> findByName(String name) throws DaoException {
        if (name == null) {
            throw new DaoException("Role name cannot be null");
        }
        Connection connection = DataSourceUtils.getConnection(dataSource);
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        List<Role> roleList = null;
        try {
            statement = connection.prepareStatement(SQL_FIND_BY_NAME);
            statement.setString(1, name);
            resultSet = statement.executeQuery();
            roleList = parseResultSet(resultSet);
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            DbUtil.closeAll(dataSource, connection, statement, resultSet);
        }
        return roleList;
    }

    /*
     * (non-Javadoc)
     * @see com.epam.javalab.newsmanag.dao.GenericDao#findAll()
     */
    @Override
    public List<Role> findAll() throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        List<Role> roleList;
        try {
            statement = connection.prepareStatement(SQL_FIND_ALL);
            resultSet = statement.executeQuery();
            roleList = parseResultSet(resultSet);
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            DbUtil.closeAll(dataSource, connection, statement, resultSet);
        }
        return roleList;
    }

    /*
     * (non-Javadoc)
     * @see com.epam.javalab.newsmanag.dao.GenericDao#update(java.lang.Object)
     */
    @Override
    public void update(Role newInstance) throws DaoException {
        if (newInstance == null) {
            throw new DaoException(
                    "Null instance cannot be recorded to the database");
        }
        Connection connection = DataSourceUtils.getConnection(dataSource);
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(SQL_UPDATE);
            statement.setString(1, newInstance.getName());
            statement.setLong(2, newInstance.getId());
            int count = statement.executeUpdate();
            if (count != 1) {
                throw new DaoException(
                        "On update modify more than 1 record or nothing");
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            DbUtil.closeAll(dataSource, connection, statement);
        }
    }

    /*
     * (non-Javadoc)
     * @see com.epam.javalab.newsmanag.dao.GenericDao#delete(java.lang.Object)
     */
    @Override
    public void delete(Long roleId) throws DaoException {
        validateId(roleId);
        Connection connection = DataSourceUtils.getConnection(dataSource);
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(SQL_DELETE);
            statement.setLong(1, roleId);
            int count = statement.executeUpdate();
            if (count != 1) {
                throw new DaoException("Deleted more than 1 record or nothing");
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            DbUtil.closeAll(dataSource, connection, statement);
        }
    }

    private List<Role> parseResultSet(ResultSet resultSet) throws SQLException {
        List<Role> list = new ArrayList<Role>();
        Role role;
        while (resultSet.next()) {
            role = createRole(resultSet);
            list.add(role);
        }
        return list;
    }

    private Role createRole(ResultSet resultSet) throws SQLException {
        Role role = new Role();
        role.setId(resultSet.getLong(ROLE_ID));
        role.setName(resultSet.getString(ROLE_NAME));
        return role;
    }

    private void validateId(Long id) throws DaoException {
        if (id == null) {
            throw new DaoException("Primary key cannot be null");
        }
    }
}
