package com.epam.javalab.newsmanag.domain;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * This class represent Comment domain from the database.
 *
 * @author Ilya Hrachou
 * @version 1.0
 */
public class Comment implements Serializable {
    private static final long serialVersionUID = 7330443568221617741L;
    private Long id;
    private Long newsId;
    private String text;
    private Timestamp creationDate;

    /**
     * Default constructor for the {@code Comment}.
     */
    public Comment() {
        super();
    }

    /**
     * Return the comment id.
     *
     * @return the comment id
     */
    public Long getId() {
        return id;
    }

    /**
     * Set the comment id.
     *
     * @param id
     *            the comment id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Return the ID of the news that this comment belong to.
     *
     * @return the ID of the news that this comment belong to
     */
    public Long getNewsId() {
        return newsId;
    }

    /**
     * Set the ID of the news that this comment belong to.
     *
     * @param newsId
     *            the ID of the news that this comment belong to
     */
    public void setNewsId(Long newsId) {
        this.newsId = newsId;
    }

    /**
     * Return the comment text.
     *
     * @return the comment text
     */
    public String getText() {
        return text;
    }

    /**
     * Set the comment text.
     *
     * @param text
     *            the comment text
     */
    public void setText(String text) {
        this.text = text;
    }

    /**
     * Return the comment creation date.
     *
     * @return the comment creation date
     */
    public Timestamp getCreationDate() {
        return creationDate;
    }

    /**
     * Set the comment creation date.
     *
     * @param creationDate
     *            the comment creation date
     */
    public void setCreationDate(Timestamp creationDate) {
        this.creationDate = creationDate;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = (prime * result) + ((creationDate == null) ? 0
                : creationDate.hashCode());
        result = (prime * result) + ((id == null) ? 0 : id.hashCode());
        result = (prime * result) + ((newsId == null) ? 0 : newsId.hashCode());
        result = (prime * result) + ((text == null) ? 0 : text.hashCode());
        return result;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof Comment)) {
            return false;
        }
        Comment other = (Comment) obj;
        if (creationDate == null) {
            if (other.creationDate != null) {
                return false;
            }
        } else if (!creationDate.equals(other.creationDate)) {
            return false;
        }
        if (id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (!id.equals(other.id)) {
            return false;
        }
        if (newsId == null) {
            if (other.newsId != null) {
                return false;
            }
        } else if (!newsId.equals(other.newsId)) {
            return false;
        }
        if (text == null) {
            if (other.text != null) {
                return false;
            }
        } else if (!text.equals(other.text)) {
            return false;
        }
        return true;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return String.format(
                "Comment [id=%s, newsId=%s, text=%s, creationDate=%s]", id,
                newsId, text, creationDate);
    }
}
